import {
  READ_DETAIL_AUDIT_TRAIL,
  READ_DETAIL_AUDIT_TRAIL_REJECT,
  READ_DETAIL_AUDIT_TRAIL_FULFILLED,
  READ_LIST_AUDIT_TRAIL,
  READ_LIST_AUDIT_TRAIL_REJECT,
  READ_LIST_AUDIT_TRAIL_FULFILLED,
  READ_SEARCH_AUDIT_TRAIL,
  READ_SEARCH_AUDIT_TRAIL_REJECT,
  READ_SEARCH_AUDIT_TRAIL_FULFILLED,
} from "./actionTypes";

export const readDetailAuditTrail = (txId) => {
  return {
    type: READ_DETAIL_AUDIT_TRAIL,
    payload: txId,
  };
};

export const readDetailAuditTrailReject = (payload) => {
  return {
    type: READ_DETAIL_AUDIT_TRAIL_REJECT,
    payload: payload,
  };
};

export const readDetailAuditTrailFulfilled = (data) => {
  return {
    type: READ_DETAIL_AUDIT_TRAIL_FULFILLED,
    payload: data,
  };
};

export const readListAuditTrail = (data) => {
  return {
    type: READ_LIST_AUDIT_TRAIL,
    payload: data,
  };
};

export const readListAuditTrailReject = (payload) => {
  return {
    type: READ_LIST_AUDIT_TRAIL_REJECT,
    payload: payload,
  };
};

export const readListAuditTrailFulfilled = (data) => {
  return {
    type: READ_LIST_AUDIT_TRAIL_FULFILLED,
    payload: data,
  };
};

export const readSearchAuditTrail = (txId) => {
  return {
    type: READ_SEARCH_AUDIT_TRAIL,
    payload: txId,
  };
};

export const readSearchAuditTrailReject = (payload) => {
  return {
    type: READ_SEARCH_AUDIT_TRAIL_REJECT,
    payload: payload,
  };
};

export const readSearchAuditTrailFulfilled = (data) => {
  return {
    type: READ_SEARCH_AUDIT_TRAIL_FULFILLED,
    payload: data,
  };
};
