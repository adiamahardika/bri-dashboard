import {
  READ_DETAIL_AUDIT_TRAIL,
  READ_DETAIL_AUDIT_TRAIL_REJECT,
  READ_DETAIL_AUDIT_TRAIL_FULFILLED,
  READ_LIST_AUDIT_TRAIL,
  READ_LIST_AUDIT_TRAIL_REJECT,
  READ_LIST_AUDIT_TRAIL_FULFILLED,
  READ_SEARCH_AUDIT_TRAIL,
  READ_SEARCH_AUDIT_TRAIL_REJECT,
  READ_SEARCH_AUDIT_TRAIL_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_detail_audit_trail: null,
  list_audit_trail: null,
  message_detail_audit_trail: null,
  message_audit_trail: null,
  response_detail_code_audit_trail: null,
  response_code_audit_trail: null,
  total_pages_audit_trail: null,
  active_pages_audit_trail: null,
  loading: false,
};

const AuditTrail = (state = INIT_STATE, action) => {
  switch (action.type) {
    case READ_DETAIL_AUDIT_TRAIL:
      return {
        ...state,
        loading: true,
      };
    case READ_DETAIL_AUDIT_TRAIL_REJECT:
      return {
        ...state,
        message_detail_audit_trail: action.payload.description,
        response_detail_code_audit_trail: action.payload.responseCode,
        loading: true,
      };
    case READ_DETAIL_AUDIT_TRAIL_FULFILLED:
      return {
        ...state,
        list_detail_audit_trail: action.payload.listAuditTrail,
        message_detail_audit_trail: action.payload.description,
        response_detail_code_audit_trail: action.payload.responseCode,
        loading: false,
      };
    case READ_LIST_AUDIT_TRAIL:
      return {
        ...state,
        loading: true,
      };
    case READ_LIST_AUDIT_TRAIL_REJECT:
      return {
        ...state,
        message_audit_trail: action.payload.description,
        response_code_audit_trail: action.payload.responseCode,
        loading: true,
      };
    case READ_LIST_AUDIT_TRAIL_FULFILLED:
      return {
        ...state,
        list_audit_trail: action.payload.content,
        message_audit_trail: action.payload.description,
        response_code_audit_trail: action.payload.responseCode,
        total_pages_audit_trail: action.payload.totalPages,
        active_pages_audit_trail: action.payload.page,
        loading: false,
      };
    case READ_SEARCH_AUDIT_TRAIL:
      return {
        ...state,
        loading: true,
      };
    case READ_SEARCH_AUDIT_TRAIL_REJECT:
      return {
        ...state,
        message_audit_trail: action.payload.description,
        response_code_audit_trail: action.payload.responseCode,
        loading: true,
      };
    case READ_SEARCH_AUDIT_TRAIL_FULFILLED:
      return {
        ...state,
        list_audit_trail: action.payload.auditTrail,
        message_audit_trail: action.payload.description,
        response_code_audit_trail: action.payload.responseCode,
        loading: false,
      };
    default:
      return state;
  }
};

export default AuditTrail;
