import { all, call, fork, put, takeLatest } from "redux-saga/effects";

import {
  READ_DETAIL_AUDIT_TRAIL,
  READ_LIST_AUDIT_TRAIL,
  READ_SEARCH_AUDIT_TRAIL,
} from "./actionTypes";
import {
  readDetailAuditTrailFulfilled,
  readDetailAuditTrailReject,
  readListAuditTrailFulfilled,
  readListAuditTrailReject,
  readSearchAuditTrailFulfilled,
  readSearchAuditTrailReject,
} from "./actions";
import {
  readDetailAuditTrailMethod,
  readListAuditTrailMethod,
  readSearchAuditTrailMethod,
} from "./method";
import general_constant from "../../../helpers/general_constant.json";

function* readDetailAuditTrail({ payload: txId }) {
  const response = yield call(readDetailAuditTrailMethod, txId);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readDetailAuditTrailFulfilled(response));
  } else {
    yield put(readDetailAuditTrailReject(response));
  }
}
function* readListAuditTrail({ payload: data }) {
  const response = yield call(readListAuditTrailMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readListAuditTrailFulfilled(response));
  } else {
    yield put(readListAuditTrailReject(response));
  }
}
function* readSearchAuditTrail({ payload: txId }) {
  const response = yield call(readSearchAuditTrailMethod, txId);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readSearchAuditTrailFulfilled(response));
  } else {
    yield put(readSearchAuditTrailReject(response));
  }
}

export function* watchReadDetailAuditTrail() {
  yield takeLatest(READ_DETAIL_AUDIT_TRAIL, readDetailAuditTrail);
}
export function* watchReadListAuditTrail() {
  yield takeLatest(READ_LIST_AUDIT_TRAIL, readListAuditTrail);
}
export function* watchReadSearchAuditTrail() {
  yield takeLatest(READ_SEARCH_AUDIT_TRAIL, readSearchAuditTrail);
}

function* AuditTrailSaga() {
  yield all([
    fork(watchReadDetailAuditTrail),
    fork(watchReadListAuditTrail),
    fork(watchReadSearchAuditTrail),
  ]);
}

export default AuditTrailSaga;
