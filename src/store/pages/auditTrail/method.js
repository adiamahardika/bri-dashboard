import general_constant from "../../../helpers/general_constant.json";
require("dotenv").config();

export const readDetailAuditTrailMethod = async (txId) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/api/detail-audit-trail/${txId}`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const readListAuditTrailMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/api/get-list-audit-trail`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const readSearchAuditTrailMethod = async (txId) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/api/audit-trail/${txId}`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};
