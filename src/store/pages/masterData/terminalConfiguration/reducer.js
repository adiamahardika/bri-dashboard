import {
  READ_MASTER_TERMINAL_CONFIGURATION,
  READ_MASTER_TERMINAL_CONFIGURATION_REJECT,
  READ_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  CREATE_MASTER_TERMINAL_CONFIGURATION,
  CREATE_MASTER_TERMINAL_CONFIGURATION_REJECT,
  CREATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  UPDATE_MASTER_TERMINAL_CONFIGURATION,
  UPDATE_MASTER_TERMINAL_CONFIGURATION_REJECT,
  UPDATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  DELETE_MASTER_TERMINAL_CONFIGURATION,
  DELETE_MASTER_TERMINAL_CONFIGURATION_REJECT,
  DELETE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  READ_MS_TERMINAL_CONFIG_SECTION,
  READ_MS_TERMINAL_CONFIG_SECTION_REJECT,
  READ_MS_TERMINAL_CONFIG_SECTION_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_master_terminal_configuration: null,
  list_ms_terminal_config_section: null,
  message_master_terminal_configuration: null,
  response_master_code_terminal_configuration: null,
  loading: false,
};

const MasterTerminalConfiguration = (state = INIT_STATE, action) => {
  switch (action.type) {
    // Get Detail Terminal Configuration
    case READ_MASTER_TERMINAL_CONFIGURATION:
      return {
        ...state,
        loading: true,
      };
    case READ_MASTER_TERMINAL_CONFIGURATION_REJECT:
      return {
        ...state,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: true,
      };
    case READ_MASTER_TERMINAL_CONFIGURATION_FULFILLED:
      return {
        ...state,
        list_master_terminal_configuration: action.payload.content,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: false,
      };

    // Create Detail Terminal Configuration
    case CREATE_MASTER_TERMINAL_CONFIGURATION:
      return {
        ...state,
        loading: true,
      };
    case CREATE_MASTER_TERMINAL_CONFIGURATION_REJECT:
      return {
        ...state,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: true,
      };
    case CREATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED:
      return {
        ...state,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: false,
      };

    // Update Detail Terminal Configuration
    case UPDATE_MASTER_TERMINAL_CONFIGURATION:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_MASTER_TERMINAL_CONFIGURATION_REJECT:
      return {
        ...state,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: true,
      };
    case UPDATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED:
      return {
        ...state,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: false,
      };

    // Delete Detail Terminal Configuration
    case DELETE_MASTER_TERMINAL_CONFIGURATION:
      return {
        ...state,
        loading: true,
      };
    case DELETE_MASTER_TERMINAL_CONFIGURATION_REJECT:
      return {
        ...state,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: true,
      };
    case DELETE_MASTER_TERMINAL_CONFIGURATION_FULFILLED:
      return {
        ...state,
        list_master_terminal_configuration: action.payload.content,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: false,
      };

    // Get Ms Terminal Config Section
    case READ_MS_TERMINAL_CONFIG_SECTION:
      return {
        ...state,
        loading: true,
      };
    case READ_MS_TERMINAL_CONFIG_SECTION_REJECT:
      return {
        ...state,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: true,
      };
    case READ_MS_TERMINAL_CONFIG_SECTION_FULFILLED:
      return {
        ...state,
        list_ms_terminal_config_section: action.payload.content,
        response_master_code_terminal_configuration:
          action.payload.responseCode,
        message_master_terminal_configuration: action.payload.description,
        loading: false,
      };

    default:
      return state;
  }
};

export default MasterTerminalConfiguration;
