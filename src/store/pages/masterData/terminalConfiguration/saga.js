import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  READ_MASTER_TERMINAL_CONFIGURATION,
  CREATE_MASTER_TERMINAL_CONFIGURATION,
  UPDATE_MASTER_TERMINAL_CONFIGURATION,
  DELETE_MASTER_TERMINAL_CONFIGURATION,
  READ_MS_TERMINAL_CONFIG_SECTION,
} from "./actionTypes";
import {
  readMasterTerminalConfigurationFulfilled,
  readMasterTerminalConfigurationReject,
  createMasterTerminalConfigurationFulfilled,
  createMasterTerminalConfigurationReject,
  updateMasterTerminalConfigurationFulfilled,
  updateMasterTerminalConfigurationReject,
  deleteMasterTerminalConfigurationFulfilled,
  deleteMasterTerminalConfigurationReject,
  readMsTerminalConfigSectionFulfilled,
  readMsTerminalConfigSectionReject,
} from "./actions";
import {
  readMasterTerminalConfigurationMethod,
  createMasterTerminalConfigurationMethod,
  updateMasterTerminalConfigurationMethod,
  deleteMasterTerminalConfigurationMethod,
  readMsTerminalConfigSectionMethod,
} from "./method";
import general_constant from "../../../../helpers/general_constant.json";

function* readMasterTerminalConfiguration() {
  const response = yield call(readMasterTerminalConfigurationMethod);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readMasterTerminalConfigurationFulfilled(response));
  } else {
    yield put(readMasterTerminalConfigurationReject(response));
  }
}
function* createMasterTerminalConfiguration({ payload: data }) {
  const response = yield call(createMasterTerminalConfigurationMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(createMasterTerminalConfigurationFulfilled(response));
  } else {
    yield put(createMasterTerminalConfigurationReject(response));
  }
}
function* updateMasterTerminalConfiguration({ payload: data }) {
  const response = yield call(updateMasterTerminalConfigurationMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(updateMasterTerminalConfigurationFulfilled(response));
  } else {
    yield put(updateMasterTerminalConfigurationReject(response));
  }
}
function* deleteMasterTerminalConfiguration({ payload: id }) {
  const response = yield call(deleteMasterTerminalConfigurationMethod, id);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(deleteMasterTerminalConfigurationFulfilled(response));
  } else {
    yield put(deleteMasterTerminalConfigurationReject(response));
  }
}
function* readMsTerminalConfigSection() {
  const response = yield call(readMsTerminalConfigSectionMethod);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readMsTerminalConfigSectionFulfilled(response));
  } else {
    yield put(readMsTerminalConfigSectionReject(response));
  }
}

export function* watchReadMasterTerminalConfiguration() {
  yield takeLatest(
    READ_MASTER_TERMINAL_CONFIGURATION,
    readMasterTerminalConfiguration
  );
}
export function* watchCreateMasterTerminalConfiguration() {
  yield takeLatest(
    CREATE_MASTER_TERMINAL_CONFIGURATION,
    createMasterTerminalConfiguration
  );
}
export function* watchUpdateMasterTerminalConfiguration() {
  yield takeLatest(
    UPDATE_MASTER_TERMINAL_CONFIGURATION,
    updateMasterTerminalConfiguration
  );
}
export function* watchDeleteMasterTerminalConfiguration() {
  yield takeLatest(
    DELETE_MASTER_TERMINAL_CONFIGURATION,
    deleteMasterTerminalConfiguration
  );
}
export function* watchReadMsTerminalConfigurationSection() {
  yield takeLatest(
    READ_MS_TERMINAL_CONFIG_SECTION,
    readMsTerminalConfigSection
  );
}

function* MasterTerminalConfigurationSaga() {
  yield all([
    fork(watchReadMasterTerminalConfiguration),
    fork(watchCreateMasterTerminalConfiguration),
    fork(watchUpdateMasterTerminalConfiguration),
    fork(watchDeleteMasterTerminalConfiguration),
    fork(watchReadMsTerminalConfigurationSection),
  ]);
}

export default MasterTerminalConfigurationSaga;
