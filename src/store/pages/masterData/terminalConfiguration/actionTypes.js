export const READ_MASTER_TERMINAL_CONFIGURATION =
  "READ_MASTER_TERMINAL_CONFIGURATION";
export const READ_MASTER_TERMINAL_CONFIGURATION_REJECT =
  "READ_MASTER_TERMINAL_CONFIGURATION_REJECT";
export const READ_MASTER_TERMINAL_CONFIGURATION_FULFILLED =
  "READ_MASTER_TERMINAL_CONFIGURATION_FULFILLED";

export const CREATE_MASTER_TERMINAL_CONFIGURATION =
  "CREATE_MASTER_TERMINAL_CONFIGURATION";
export const CREATE_MASTER_TERMINAL_CONFIGURATION_REJECT =
  "CREATE_MASTER_TERMINAL_CONFIGURATION_REJECT";
export const CREATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED =
  "CREATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED";

export const UPDATE_MASTER_TERMINAL_CONFIGURATION =
  "UPDATE_MASTER_TERMINAL_CONFIGURATION";
export const UPDATE_MASTER_TERMINAL_CONFIGURATION_REJECT =
  "UPDATE_MASTER_TERMINAL_CONFIGURATION_REJECT";
export const UPDATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED =
  "UPDATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED";

export const DELETE_MASTER_TERMINAL_CONFIGURATION =
  "DELETE_MASTER_TERMINAL_CONFIGURATION";
export const DELETE_MASTER_TERMINAL_CONFIGURATION_REJECT =
  "DELETE_MASTER_TERMINAL_CONFIGURATION_REJECT";
export const DELETE_MASTER_TERMINAL_CONFIGURATION_FULFILLED =
  "DELETE_MASTER_TERMINAL_CONFIGURATION_FULFILLED";

export const READ_MS_TERMINAL_CONFIG_SECTION =
  "READ_MS_TERMINAL_CONFIG_SECTION";
export const READ_MS_TERMINAL_CONFIG_SECTION_REJECT =
  "READ_MS_TERMINAL_CONFIG_SECTION_REJECT";
export const READ_MS_TERMINAL_CONFIG_SECTION_FULFILLED =
  "READ_MS_TERMINAL_CONFIG_SECTION_FULFILLED";
