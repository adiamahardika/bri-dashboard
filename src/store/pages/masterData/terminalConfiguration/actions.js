import {
  READ_MASTER_TERMINAL_CONFIGURATION,
  READ_MASTER_TERMINAL_CONFIGURATION_REJECT,
  READ_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  CREATE_MASTER_TERMINAL_CONFIGURATION,
  CREATE_MASTER_TERMINAL_CONFIGURATION_REJECT,
  CREATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  UPDATE_MASTER_TERMINAL_CONFIGURATION,
  UPDATE_MASTER_TERMINAL_CONFIGURATION_REJECT,
  UPDATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  DELETE_MASTER_TERMINAL_CONFIGURATION,
  DELETE_MASTER_TERMINAL_CONFIGURATION_REJECT,
  DELETE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
  READ_MS_TERMINAL_CONFIG_SECTION,
  READ_MS_TERMINAL_CONFIG_SECTION_REJECT,
  READ_MS_TERMINAL_CONFIG_SECTION_FULFILLED,
} from "./actionTypes";

// Get Master Terminal Configuration
export const readMasterTerminalConfiguration = () => {
  return {
    type: READ_MASTER_TERMINAL_CONFIGURATION,
  };
};

export const readMasterTerminalConfigurationReject = (payload) => {
  return {
    type: READ_MASTER_TERMINAL_CONFIGURATION_REJECT,
    payload: payload,
  };
};

export const readMasterTerminalConfigurationFulfilled = (data) => {
  return {
    type: READ_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
    payload: data,
  };
};

// Create Master Terminal Configuration
export const createMasterTerminalConfiguration = (data) => {
  return {
    type: CREATE_MASTER_TERMINAL_CONFIGURATION,
    payload: data,
  };
};

export const createMasterTerminalConfigurationReject = (payload) => {
  return {
    type: CREATE_MASTER_TERMINAL_CONFIGURATION_REJECT,
    payload: payload,
  };
};

export const createMasterTerminalConfigurationFulfilled = (data) => {
  return {
    type: CREATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
    payload: data,
  };
};

// Update Master Terminal Configuration
export const updateMasterTerminalConfiguration = (data) => {
  return {
    type: UPDATE_MASTER_TERMINAL_CONFIGURATION,
    payload: data,
  };
};

export const updateMasterTerminalConfigurationReject = (payload) => {
  return {
    type: UPDATE_MASTER_TERMINAL_CONFIGURATION_REJECT,
    payload: payload,
  };
};

export const updateMasterTerminalConfigurationFulfilled = (data) => {
  return {
    type: UPDATE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
    payload: data,
  };
};

// Delete Master Terminal Configuration
export const deleteMasterTerminalConfiguration = (id) => {
  return {
    type: DELETE_MASTER_TERMINAL_CONFIGURATION,
    payload: id,
  };
};

export const deleteMasterTerminalConfigurationReject = (payload) => {
  return {
    type: DELETE_MASTER_TERMINAL_CONFIGURATION_REJECT,
    payload: payload,
  };
};

export const deleteMasterTerminalConfigurationFulfilled = (data) => {
  return {
    type: DELETE_MASTER_TERMINAL_CONFIGURATION_FULFILLED,
    payload: data,
  };
};

// Get Master Terminal Configuration Section
export const readMsTerminalConfigSection = () => {
  return {
    type: READ_MS_TERMINAL_CONFIG_SECTION,
  };
};

export const readMsTerminalConfigSectionReject = (payload) => {
  return {
    type: READ_MS_TERMINAL_CONFIG_SECTION_REJECT,
    payload: payload,
  };
};

export const readMsTerminalConfigSectionFulfilled = (data) => {
  return {
    type: READ_MS_TERMINAL_CONFIG_SECTION_FULFILLED,
    payload: data,
  };
};
