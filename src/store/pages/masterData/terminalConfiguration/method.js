import general_constant from "../../../../helpers/general_constant.json";
require("dotenv").config();

export const readMasterTerminalConfigurationMethod = async () => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/msterminal-config/getall`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const createMasterTerminalConfigurationMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/msterminal-config/add`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const updateMasterTerminalConfigurationMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/msterminal-config/edit`,
    {
      method: "PUT",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const deleteMasterTerminalConfigurationMethod = async (id) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/msterminal-config/delete-terminal/${id}`,
    {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  const responseGet = await fetch(
    `${process.env.REACT_APP_API}/msterminal-config/getall`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else if (
    response.json().responseCode === general_constant.success_response_code
  ) {
    return response.json();
  } else {
    return responseGet.json();
  }
};

export const readMsTerminalConfigSectionMethod = async () => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/msterminal-config/get-section`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};
