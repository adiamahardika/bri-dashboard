import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  READ_ALL_CARD,
  READ_NAMA_PRODUK,
  DELETE_CARD,
  CREATE_CARD,
  UPDATE_CARD,
} from "./actionTypes";
import {
  readAllCardFulfilled,
  readAllCardReject,
  readNamaProdukFulfilled,
  readNamaProdukReject,
  deleteCardFulfilled,
  deleteCardReject,
  createCardFulfilled,
  createCardReject,
  updateCardFulfilled,
  updateCardReject,
} from "./actions";
import {
  readAllCardMethod,
  readNamaProdukMethod,
  deleteCardMethod,
  createCardMethod,
  updateCardMethod,
} from "./method";
import general_constant from "../../../../helpers/general_constant.json";

function* readAllCard({ payload: data }) {
  const response = yield call(readAllCardMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readAllCardFulfilled(response));
  } else {
    yield put(readAllCardReject(response));
  }
}
function* readNamaProduk() {
  const response = yield call(readNamaProdukMethod);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readNamaProdukFulfilled(response));
  } else {
    yield put(readNamaProdukReject(response));
  }
}
function* deleteCard({ payload: data }) {
  const response = yield call(deleteCardMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(deleteCardFulfilled(response));
  } else {
    yield put(deleteCardReject(response));
  }
}
function* createCard({ payload: data }) {
  const response = yield call(createCardMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(createCardFulfilled(response));
  } else {
    yield put(createCardReject(response));
  }
}
function* updateCard({ payload: data }) {
  const response = yield call(updateCardMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(updateCardFulfilled(response));
  } else {
    yield put(updateCardReject(response));
  }
}

export function* watchReadAllCard() {
  yield takeLatest(READ_ALL_CARD, readAllCard);
}
export function* watchReadNamaProduk() {
  yield takeLatest(READ_NAMA_PRODUK, readNamaProduk);
}
export function* watchDeleteCard() {
  yield takeLatest(DELETE_CARD, deleteCard);
}
export function* watchAddCard() {
  yield takeLatest(CREATE_CARD, createCard);
}
export function* watchEditCard() {
  yield takeLatest(UPDATE_CARD, updateCard);
}

function* PropertiesSaga() {
  yield all([
    fork(watchReadAllCard),
    fork(watchReadNamaProduk),
    fork(watchDeleteCard),
    fork(watchAddCard),
    fork(watchEditCard),
  ]);
}

export default PropertiesSaga;
