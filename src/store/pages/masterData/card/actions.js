import {
  READ_ALL_CARD,
  READ_ALL_CARD_REJECT,
  READ_ALL_CARD_FULFILLED,
  READ_NAMA_PRODUK,
  READ_NAMA_PRODUK_REJECT,
  READ_NAMA_PRODUK_FULFILLED,
  DELETE_CARD,
  DELETE_CARD_REJECT,
  DELETE_CARD_FULFILLED,
  CREATE_CARD,
  CREATE_CARD_REJECT,
  CREATE_CARD_FULFILLED,
  UPDATE_CARD,
  UPDATE_CARD_REJECT,
  UPDATE_CARD_FULFILLED,
} from "./actionTypes";

// Get All Card
export const readAllCard = (data) => {
  return {
    type: READ_ALL_CARD,
    payload: data,
  };
};

export const readAllCardReject = (payload) => {
  return {
    type: READ_ALL_CARD_REJECT,
    payload: payload,
  };
};

export const readAllCardFulfilled = (data) => {
  return {
    type: READ_ALL_CARD_FULFILLED,
    payload: data,
  };
};

// Get Nama Produk
export const readNamaProduk = () => {
  return {
    type: READ_NAMA_PRODUK,
  };
};

export const readNamaProdukReject = (payload) => {
  return {
    type: READ_NAMA_PRODUK_REJECT,
    payload: payload,
  };
};

export const readNamaProdukFulfilled = (data) => {
  return {
    type: READ_NAMA_PRODUK_FULFILLED,
    payload: data,
  };
};

// Delete Card
export const deleteCard = (data) => {
  return {
    type: DELETE_CARD,
    payload: data,
  };
};

export const deleteCardReject = (payload) => {
  return {
    type: DELETE_CARD_REJECT,
    payload: payload,
  };
};

export const deleteCardFulfilled = (data) => {
  return {
    type: DELETE_CARD_FULFILLED,
    payload: data,
  };
};

// Create Card
export const createCard = (data) => {
  return {
    type: CREATE_CARD,
    payload: data,
  };
};

export const createCardReject = (payload) => {
  return {
    type: CREATE_CARD_REJECT,
    payload: payload,
  };
};

export const createCardFulfilled = (data) => {
  return {
    type: CREATE_CARD_FULFILLED,
    payload: data,
  };
};

// Update Card
export const updateCard = (data) => {
  return {
    type: UPDATE_CARD,
    payload: data,
  };
};

export const updateCardReject = (payload) => {
  return {
    type: UPDATE_CARD_REJECT,
    payload: payload,
  };
};

export const updateCardFulfilled = (data) => {
  return {
    type: UPDATE_CARD_FULFILLED,
    payload: data,
  };
};
