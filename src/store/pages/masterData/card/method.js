import general_constant from "../../../../helpers/general_constant.json";
require("dotenv").config();

export const readAllCardMethod = async (data) => {
  const page_no = data.page_no || 0;
  const size = data.size || 10;
  const sort_by = data.sort_by || "jenisKartu";
  const sort_type = data.sort_type || "asc";
  const response = await fetch(
    `${process.env.REACT_APP_API}/mscard/getall/${page_no}/${size}/${sort_by}/${sort_type}`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const readNamaProdukMethod = async () => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/mscard/get-nama-produk`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const deleteCardMethod = async (data) => {
  const page_no = data.page_no || 0;
  const size = data.size || 10;
  const sort_by = data.sort_by || "jenisKartu";
  const sort_type = data.sort_type || "asc";
  const response = await fetch(
    `${process.env.REACT_APP_API}/mscard/delete/${data.id}`,
    {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  const responseGet = await fetch(
    `${process.env.REACT_APP_API}/mscard/getall/${page_no}/${size}/${sort_by}/${sort_type}`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else if (
    response.json().responseCode === general_constant.success_response_code
  ) {
    return response.json();
  } else {
    return responseGet.json();
  }
};

export const createCardMethod = async (data) => {
  const response = await fetch(`${process.env.REACT_APP_API}/mscard/add`, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
    body: JSON.stringify(data),
  });
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const updateCardMethod = async (data) => {
  const response = await fetch(`${process.env.REACT_APP_API}/mscard/edit`, {
    method: "PUT",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
    body: JSON.stringify(data),
  });
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};
