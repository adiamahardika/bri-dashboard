import {
  READ_ALL_CARD,
  READ_ALL_CARD_REJECT,
  READ_ALL_CARD_FULFILLED,
  READ_NAMA_PRODUK,
  READ_NAMA_PRODUK_REJECT,
  READ_NAMA_PRODUK_FULFILLED,
  DELETE_CARD,
  DELETE_CARD_REJECT,
  DELETE_CARD_FULFILLED,
  CREATE_CARD,
  CREATE_CARD_REJECT,
  CREATE_CARD_FULFILLED,
  UPDATE_CARD,
  UPDATE_CARD_REJECT,
  UPDATE_CARD_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_all_card: null,
  list_nama_produk: null,
  message_all_card: null,
  message_nama_produk: null,
  response_code_all_card: null,
  response_code_nama_produk: null,
  active_page_card: null,
  total_pages_card: null,
  loading: false,
};

const Card = (state = INIT_STATE, action) => {
  switch (action.type) {
    // Get All Card
    case READ_ALL_CARD:
      return {
        ...state,
        loading: true,
      };
    case READ_ALL_CARD_REJECT:
      return {
        ...state,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        loading: true,
      };
    case READ_ALL_CARD_FULFILLED:
      return {
        ...state,
        list_all_card: action.payload.content,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        total_pages_card: action.payload.totalPages,
        active_page_card: action.payload.page,
        loading: false,
      };

    // Get Nama Produk
    case READ_NAMA_PRODUK:
      return {
        ...state,
        loading: true,
      };
    case READ_NAMA_PRODUK_REJECT:
      return {
        ...state,
        response_code_nama_produk: action.payload.responseCode,
        message_nama_produk: action.payload.description,
        loading: true,
      };
    case READ_NAMA_PRODUK_FULFILLED:
      return {
        ...state,
        list_nama_produk: action.payload.listNamaProduk,
        response_code_nama_produk: action.payload.responseCode,
        message_nama_produk: action.payload.description,
        loading: false,
      };

    // Delete Card
    case DELETE_CARD:
      return {
        ...state,
        loading: true,
      };
    case DELETE_CARD_REJECT:
      return {
        ...state,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        loading: true,
      };
    case DELETE_CARD_FULFILLED:
      return {
        ...state,
        list_all_card: action.payload.content,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        loading: false,
      };

    // Create Card
    case CREATE_CARD:
      return {
        ...state,
        loading: true,
      };
    case CREATE_CARD_REJECT:
      return {
        ...state,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        loading: true,
      };
    case CREATE_CARD_FULFILLED:
      return {
        ...state,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        loading: false,
      };

    // Update Card
    case UPDATE_CARD:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_CARD_REJECT:
      return {
        ...state,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        loading: true,
      };
    case UPDATE_CARD_FULFILLED:
      return {
        ...state,
        response_code_all_card: action.payload.responseCode,
        message_all_card: action.payload.description,
        loading: false,
      };
    default:
      return state;
  }
};

export default Card;
