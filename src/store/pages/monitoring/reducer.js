import {
  READ_LIST_MONITORING,
  READ_LIST_MONITORING_REJECT,
  READ_LIST_MONITORING_FULFILLED,
  READ_DETAIL_MONITORING,
  READ_DETAIL_MONITORING_REJECT,
  READ_DETAIL_MONITORING_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_detail_monitoring: null,
  list_monitoring: null,
  message_monitoring: null,
  response_code_monitoring: null,
  total_pages_monitoring: null,
  active_pages_monitoring: null,
  loading: false,
};

const Monitoring = (state = INIT_STATE, action) => {
  switch (action.type) {
    case READ_LIST_MONITORING:
      return {
        ...state,
        loading: true,
      };
    case READ_LIST_MONITORING_REJECT:
      return {
        ...state,
        message_monitoring: action.payload.description,
        response_code_monitoring: action.payload.responseCode,
        loading: true,
      };
    case READ_LIST_MONITORING_FULFILLED:
      return {
        ...state,
        list_monitoring: action.payload.content,
        message_monitoring: action.payload.description,
        response_code_monitoring: action.payload.responseCode,
        total_pages_monitoring: action.payload.totalPages,
        active_pages_monitoring: action.payload.page,
        loading: false,
      };

    case READ_DETAIL_MONITORING:
      return {
        ...state,
        loading: true,
      };
    case READ_DETAIL_MONITORING_REJECT:
      return {
        ...state,
        message_monitoring: action.payload.description,
        response_code_monitoring: action.payload.responseCode,
        loading: true,
      };
    case READ_DETAIL_MONITORING_FULFILLED:
      return {
        ...state,
        list_detail_monitoring: action.payload.content,
        message_monitoring: action.payload.description,
        response_code_monitoring: action.payload.responseCode,
        total_pages_monitoring: action.payload.totalPages,
        active_pages_monitoring: action.payload.page,
        loading: false,
      };

    default:
      return state;
  }
};

export default Monitoring;
