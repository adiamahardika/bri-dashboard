import { all, call, fork, put, takeLatest } from "redux-saga/effects";

import { READ_LIST_MONITORING, READ_DETAIL_MONITORING } from "./actionTypes";
import {
  readListMonitoringFulfilled,
  readListMonitoringReject,
  readDetailMonitoringFulfilled,
  readDetailMonitoringReject,
} from "./actions";
import { readListMonitoringMethod, readDetailMonitoringMethod } from "./method";
import general_constant from "../../../helpers/general_constant.json";

function* readListMonitoring({ payload: data }) {
  const response = yield call(readListMonitoringMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readListMonitoringFulfilled(response));
  } else {
    yield put(readListMonitoringReject(response));
  }
}
function* readDetailMonitoring({ payload: data }) {
  const response = yield call(readDetailMonitoringMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readDetailMonitoringFulfilled(response));
  } else {
    yield put(readDetailMonitoringReject(response));
  }
}

export function* watchReadListMonitoring() {
  yield takeLatest(READ_LIST_MONITORING, readListMonitoring);
}
export function* watchReadDetailMonitoring() {
  yield takeLatest(READ_DETAIL_MONITORING, readDetailMonitoring);
}

function* MonitoringSaga() {
  yield all([fork(watchReadListMonitoring), fork(watchReadDetailMonitoring)]);
}

export default MonitoringSaga;
