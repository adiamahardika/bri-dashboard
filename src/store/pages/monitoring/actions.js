import {
  READ_LIST_MONITORING,
  READ_LIST_MONITORING_REJECT,
  READ_LIST_MONITORING_FULFILLED,
  READ_DETAIL_MONITORING,
  READ_DETAIL_MONITORING_REJECT,
  READ_DETAIL_MONITORING_FULFILLED,
} from "./actionTypes";

export const readListMonitoring = (data) => {
  return {
    type: READ_LIST_MONITORING,
    payload: data,
  };
};

export const readListMonitoringReject = (payload) => {
  return {
    type: READ_LIST_MONITORING_REJECT,
    payload: payload,
  };
};

export const readListMonitoringFulfilled = (data) => {
  return {
    type: READ_LIST_MONITORING_FULFILLED,
    payload: data,
  };
};

export const readDetailMonitoring = (data) => {
  return {
    type: READ_DETAIL_MONITORING,
    payload: data,
  };
};

export const readDetailMonitoringReject = (payload) => {
  return {
    type: READ_DETAIL_MONITORING_REJECT,
    payload: payload,
  };
};

export const readDetailMonitoringFulfilled = (data) => {
  return {
    type: READ_DETAIL_MONITORING_FULFILLED,
    payload: data,
  };
};
