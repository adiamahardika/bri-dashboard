import general_constant from "../../../helpers/general_constant.json";
require("dotenv").config();

export const readListMonitoringMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/monitoring/getall`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const readDetailMonitoringMethod = async (data) => {
  const page_no = data.page_no || 0;
  const size = data.size || 10;
  const response = await fetch(
    `${process.env.REACT_APP_API}/monitoring/get-detail/${page_no}/${size}/${data.terminal_id}`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};
