import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  READ_REPORT_ALL_TRANSACTION,
  READ_REPORT_GANTI_KARTU,
  READ_REPORT_PRINT_LAST_5_TRANSACTION,
  READ_REPORT_PRINT_PASSBOOK,
  READ_REPORT_PRINT_MONTHLY_STATEMENT,
  EXPORT_REPORT,
} from "./actionTypes";
import {
  readReportAllTransactionFulfilled,
  readReportAllTransactionReject,
  readReportGantiKartuFulfilled,
  readReportGantiKartuReject,
  readReportPrintLast5TransactionFulfilled,
  readReportPrintLast5TransactionReject,
  readReportPrintPassbookFulfilled,
  readReportPrintPassbookReject,
  readReportPrintMonthlyStatementFulfilled,
  readReportPrintMonthlyStatementReject,
  exportReportFulfilled,
  exportReportReject,
} from "./actions";
import { readReportMethod } from "./method";
import general_constant from "../../../helpers/general_constant.json";

function* readReportAllTransaction({ payload: data }) {
  const response = yield call(readReportMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readReportAllTransactionFulfilled(response));
  } else {
    yield put(readReportAllTransactionReject(response));
  }
}
function* readReportGantiKartu({ payload: data }) {
  const response = yield call(readReportMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readReportGantiKartuFulfilled(response));
  } else {
    yield put(readReportGantiKartuReject(response));
  }
}
function* readReportPrintLast5Transaction({ payload: data }) {
  const response = yield call(readReportMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readReportPrintLast5TransactionFulfilled(response));
  } else {
    yield put(readReportPrintLast5TransactionReject(response));
  }
}
function* readReportPrintPassbook({ payload: data }) {
  const response = yield call(readReportMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readReportPrintPassbookFulfilled(response));
  } else {
    yield put(readReportPrintPassbookReject(response));
  }
}
function* readReportPrintMonthlyStatement({ payload: data }) {
  const response = yield call(readReportMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readReportPrintMonthlyStatementFulfilled(response));
  } else {
    yield put(readReportPrintMonthlyStatementReject(response));
  }
}
function* exportReport({ payload: data }) {
  const response = yield call(readReportMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(exportReportFulfilled(response));
  } else {
    yield put(exportReportReject(response));
  }
}

export function* watchReadReportAllTransaction() {
  yield takeLatest(READ_REPORT_ALL_TRANSACTION, readReportAllTransaction);
}
export function* watchReadReportGantiKartu() {
  yield takeLatest(READ_REPORT_GANTI_KARTU, readReportGantiKartu);
}
export function* watchReadReportPrintLast5Transaction() {
  yield takeLatest(
    READ_REPORT_PRINT_LAST_5_TRANSACTION,
    readReportPrintLast5Transaction
  );
}
export function* watchReadReportPrintPassbook() {
  yield takeLatest(READ_REPORT_PRINT_PASSBOOK, readReportPrintPassbook);
}
export function* watchReadReportPrintMonthlyStatement() {
  yield takeLatest(
    READ_REPORT_PRINT_MONTHLY_STATEMENT,
    readReportPrintMonthlyStatement
  );
}
export function* watchExportReport() {
  yield takeLatest(EXPORT_REPORT, exportReport);
}

function* ReportSaga() {
  yield all([
    fork(watchReadReportAllTransaction),
    fork(watchReadReportGantiKartu),
    fork(watchReadReportPrintLast5Transaction),
    fork(watchReadReportPrintPassbook),
    fork(watchReadReportPrintMonthlyStatement),
    fork(watchExportReport),
  ]);
}

export default ReportSaga;
