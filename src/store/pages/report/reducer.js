import {
  READ_REPORT_ALL_TRANSACTION,
  READ_REPORT_ALL_TRANSACTION_REJECT,
  READ_REPORT_ALL_TRANSACTION_FULFILLED,
  READ_REPORT_PRINT_LAST_5_TRANSACTION,
  READ_REPORT_PRINT_LAST_5_TRANSACTION_REJECT,
  READ_REPORT_PRINT_LAST_5_TRANSACTION_FULFILLED,
  READ_REPORT_PRINT_PASSBOOK,
  READ_REPORT_PRINT_PASSBOOK_REJECT,
  READ_REPORT_PRINT_PASSBOOK_FULFILLED,
  READ_REPORT_PRINT_MONTHLY_STATEMENT,
  READ_REPORT_PRINT_MONTHLY_STATEMENT_REJECT,
  READ_REPORT_PRINT_MONTHLY_STATEMENT_FULFILLED,
  READ_REPORT_GANTI_KARTU,
  READ_REPORT_GANTI_KARTU_REJECT,
  READ_REPORT_GANTI_KARTU_FULFILLED,
  EXPORT_REPORT,
  EXPORT_REPORT_REJECT,
  EXPORT_REPORT_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_report_all_transaction: [],
  list_report_ganti_kartu: [],
  list_report_print_last_5_transaction: [],
  list_report_print_passbook: [],
  list_report_print_monthly_statement: [],
  list_export_report: [],
  message_report_all_transaction: null,
  message_report_ganti_kartu: null,
  message_report_print_last_5_transaction: null,
  message_report_print_passbook: null,
  message_export_report: null,
  message_report_print_monthly_statement: null,
  response_code_report_all_transaction: null,
  response_code_report_ganti_kartu: null,
  response_code_report_print_last_5_transaction: null,
  response_code_report_print_passbook: null,
  response_code_report_print_monthly_statement: null,
  response_code_export_report: null,
  active_page_transaction: 0,
  total_pages_transaction: null,
  loading: false,
  is_done_export: false,
};

const Report = (state = INIT_STATE, action) => {
  switch (action.type) {
    // Get Report All Transaction
    case READ_REPORT_ALL_TRANSACTION:
      return {
        ...state,
        loading: true,
      };
    case READ_REPORT_ALL_TRANSACTION_REJECT:
      return {
        ...state,
        response_code_report_all_transaction: action.payload.responseCode,
        message_report_all_transaction: action.payload.description,
        loading: true,
      };
    case READ_REPORT_ALL_TRANSACTION_FULFILLED:
      return {
        ...state,
        list_report_all_transaction: action.payload.content,
        response_code_report_all_transaction: action.payload.responseCode,
        message_report_all_transaction: action.payload.description,
        active_page_transaction: action.payload.page,
        total_pages_transaction: action.payload.totalPages,
        loading: false,
      };

    // Get Report Print Last 5 Transaction
    case READ_REPORT_PRINT_LAST_5_TRANSACTION:
      return {
        ...state,
        loading: true,
      };
    case READ_REPORT_PRINT_LAST_5_TRANSACTION_REJECT:
      return {
        ...state,
        response_code_report_print_last_5_transaction:
          action.payload.responseCode,
        message_report_print_last_5_transaction: action.payload.description,
        loading: true,
      };
    case READ_REPORT_PRINT_LAST_5_TRANSACTION_FULFILLED:
      return {
        ...state,
        list_report_print_last_5_transaction: action.payload.content,
        response_code_report_print_last_5_transaction:
          action.payload.responseCode,
        message_report_print_last_5_transaction: action.payload.description,
        active_page_transaction: action.payload.page,
        total_pages_transaction: action.payload.totalPages,
        loading: false,
      };

    // Get Report Print Passbook
    case READ_REPORT_PRINT_PASSBOOK:
      return {
        ...state,
        loading: true,
      };
    case READ_REPORT_PRINT_PASSBOOK_REJECT:
      return {
        ...state,
        response_code_report_print_passbook: action.payload.responseCode,
        message_report_print_passbook: action.payload.description,
        loading: true,
      };
    case READ_REPORT_PRINT_PASSBOOK_FULFILLED:
      return {
        ...state,
        list_report_print_passbook: action.payload.content,
        response_code_report_print_passbook: action.payload.responseCode,
        message_report_print_passbook: action.payload.description,
        active_page_transaction: action.payload.page,
        total_pages_transaction: action.payload.totalPages,
        loading: false,
      };

    // Get Report Print Monthly Statement
    case READ_REPORT_PRINT_MONTHLY_STATEMENT:
      return {
        ...state,
        loading: true,
      };
    case READ_REPORT_PRINT_MONTHLY_STATEMENT_REJECT:
      return {
        ...state,
        response_code_report_print_monthly_statement:
          action.payload.responseCode,
        message_report_print_monthly_statement: action.payload.description,
        loading: true,
      };
    case READ_REPORT_PRINT_MONTHLY_STATEMENT_FULFILLED:
      return {
        ...state,
        list_report_print_monthly_statement: action.payload.content,
        response_code_report_print_monthly_statement:
          action.payload.responseCode,
        message_report_print_monthly_statement: action.payload.description,
        active_page_transaction: action.payload.page,
        total_pages_transaction: action.payload.totalPages,
        loading: false,
      };

    // Get Report Ganti Kartu
    case READ_REPORT_GANTI_KARTU:
      return {
        ...state,
        loading: true,
      };
    case READ_REPORT_GANTI_KARTU_REJECT:
      return {
        ...state,
        response_code_report_ganti_kartu: action.payload.responseCode,
        message_report_ganti_kartu: action.payload.description,
        loading: true,
      };
    case READ_REPORT_GANTI_KARTU_FULFILLED:
      return {
        ...state,
        list_report_ganti_kartu: action.payload.content,
        response_code_report_ganti_kartu: action.payload.responseCode,
        message_report_ganti_kartu: action.payload.description,
        active_page_transaction: action.payload.page,
        total_pages_transaction: action.payload.totalPages,
        loading: false,
      };

    // Export Report
    case EXPORT_REPORT:
      return {
        ...state,
        is_done_export: false,
        loading: true,
      };
    case EXPORT_REPORT_REJECT:
      return {
        ...state,
        response_code_export_report: action.payload.responseCode,
        message_export_report: action.payload.description,
        is_done_export: false,
        loading: true,
      };
    case EXPORT_REPORT_FULFILLED:
      return {
        ...state,
        list_export_report: action.payload.content,
        response_code_export_report: action.payload.responseCode,
        message_export_report: action.payload.description,
        is_done_export: true,
        loading: false,
      };
    default:
      return state;
  }
};

export default Report;
