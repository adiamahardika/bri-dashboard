import {
  READ_REPORT_ALL_TRANSACTION,
  READ_REPORT_ALL_TRANSACTION_REJECT,
  READ_REPORT_ALL_TRANSACTION_FULFILLED,
  READ_REPORT_PRINT_LAST_5_TRANSACTION,
  READ_REPORT_PRINT_LAST_5_TRANSACTION_REJECT,
  READ_REPORT_PRINT_LAST_5_TRANSACTION_FULFILLED,
  READ_REPORT_PRINT_PASSBOOK,
  READ_REPORT_PRINT_PASSBOOK_REJECT,
  READ_REPORT_PRINT_PASSBOOK_FULFILLED,
  READ_REPORT_PRINT_MONTHLY_STATEMENT,
  READ_REPORT_PRINT_MONTHLY_STATEMENT_REJECT,
  READ_REPORT_PRINT_MONTHLY_STATEMENT_FULFILLED,
  READ_REPORT_GANTI_KARTU,
  READ_REPORT_GANTI_KARTU_REJECT,
  READ_REPORT_GANTI_KARTU_FULFILLED,
  EXPORT_REPORT,
  EXPORT_REPORT_REJECT,
  EXPORT_REPORT_FULFILLED,
} from "./actionTypes";

// Get Report All Transaction
export const readReportAllTransaction = (data) => {
  return {
    type: READ_REPORT_ALL_TRANSACTION,
    payload: data,
  };
};

export const readReportAllTransactionReject = (payload) => {
  return {
    type: READ_REPORT_ALL_TRANSACTION_REJECT,
    payload: payload,
  };
};

export const readReportAllTransactionFulfilled = (data) => {
  return {
    type: READ_REPORT_ALL_TRANSACTION_FULFILLED,
    payload: data,
  };
};

// Get Report Print Last 5 Transaction
export const readReportPrintLast5Transaction = (data) => {
  return {
    type: READ_REPORT_PRINT_LAST_5_TRANSACTION,
    payload: data,
  };
};

export const readReportPrintLast5TransactionReject = (payload) => {
  return {
    type: READ_REPORT_PRINT_LAST_5_TRANSACTION_REJECT,
    payload: payload,
  };
};

export const readReportPrintLast5TransactionFulfilled = (data) => {
  return {
    type: READ_REPORT_PRINT_LAST_5_TRANSACTION_FULFILLED,
    payload: data,
  };
};

// Get Report Print Passbook
export const readReportPrintPassbook = (data) => {
  return {
    type: READ_REPORT_PRINT_PASSBOOK,
    payload: data,
  };
};

export const readReportPrintPassbookReject = (payload) => {
  return {
    type: READ_REPORT_PRINT_PASSBOOK_REJECT,
    payload: payload,
  };
};

export const readReportPrintPassbookFulfilled = (data) => {
  return {
    type: READ_REPORT_PRINT_PASSBOOK_FULFILLED,
    payload: data,
  };
};

// Get Report Print Passbook
export const readReportPrintMonthlyStatement = (data) => {
  return {
    type: READ_REPORT_PRINT_MONTHLY_STATEMENT,
    payload: data,
  };
};

export const readReportPrintMonthlyStatementReject = (payload) => {
  return {
    type: READ_REPORT_PRINT_MONTHLY_STATEMENT_REJECT,
    payload: payload,
  };
};

export const readReportPrintMonthlyStatementFulfilled = (data) => {
  return {
    type: READ_REPORT_PRINT_MONTHLY_STATEMENT_FULFILLED,
    payload: data,
  };
};

// Get Report Ganti Kartu
export const readReportGantiKartu = (data) => {
  return {
    type: READ_REPORT_GANTI_KARTU,
    payload: data,
  };
};

export const readReportGantiKartuReject = (payload) => {
  return {
    type: READ_REPORT_GANTI_KARTU_REJECT,
    payload: payload,
  };
};

export const readReportGantiKartuFulfilled = (data) => {
  return {
    type: READ_REPORT_GANTI_KARTU_FULFILLED,
    payload: data,
  };
};

//  Export Report
export const exportReport = (data) => {
  return {
    type: EXPORT_REPORT,
    payload: data,
  };
};

export const exportReportReject = (payload) => {
  return {
    type: EXPORT_REPORT_REJECT,
    payload: payload,
  };
};

export const exportReportFulfilled = (data) => {
  return {
    type: EXPORT_REPORT_FULFILLED,
    payload: data,
  };
};
