import general_constant from "../../../helpers/general_constant.json";
require("dotenv").config();

export const readReportMethod = async (data) => {
  const response = await fetch(`${process.env.REACT_APP_API}/api/get-report`, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("accessToken"),
    },
    body: JSON.stringify(data),
  });
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};
