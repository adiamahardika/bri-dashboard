import {
  READ_DASHBOARD,
  READ_DASHBOARD_REJECT,
  READ_DASHBOARD_FULFILLED,
} from "./actionTypes";

export const readDashboard = (date) => {
  return {
    type: READ_DASHBOARD,
    payload: date,
  };
};

export const readDashboardReject = (payload) => {
  return {
    type: READ_DASHBOARD_REJECT,
    payload: payload,
  };
};

export const readDashboardFulfilled = (data) => {
  return {
    type: READ_DASHBOARD_FULFILLED,
    payload: data,
  };
};