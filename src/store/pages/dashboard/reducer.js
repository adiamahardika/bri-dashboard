import {
  READ_DASHBOARD,
  READ_DASHBOARD_REJECT,
  READ_DASHBOARD_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  total_transaksi_gk: null,
  total_transaksi_5_mutasi: null,
  total_transaksi_passbook: null,
  total_transaksi_statement: null,
  message_dashboard: null,
  response_code_dashboard: null,
  loading: false,
};

const Dashboard = (state = INIT_STATE, action) => {
  switch (action.type) {
    case READ_DASHBOARD:
      return {
        ...state,
        loading: true,
      };
    case READ_DASHBOARD_REJECT:
      return {
        ...state,
        message_dashboard: action.payload.description,
        response_code_dashboard: action.payload.responseCode,
        loading: true,
      };
    case READ_DASHBOARD_FULFILLED:
      return {
        ...state,
        total_transaksi_gk: action.payload.totalTransaksiGK,
        total_transaksi_5_mutasi: action.payload.totalTransaksi5Mutasi,
        total_transaksi_passbook: action.payload.totalTransaksiPassbook,
        total_transaksi_statement: action.payload.totalTransaksiStatement,
        message_dashboard: action.payload.description,
        response_code_dashboard: action.payload.responseCode,
        loading: false,
      };
    default:
      return state;
  }
};

export default Dashboard;
