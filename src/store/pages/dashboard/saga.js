import { all, call, fork, put, takeLatest } from "redux-saga/effects";

import { READ_DASHBOARD } from "./actionTypes";
import { readDashboardFulfilled, readDashboardReject } from "./actions";
import { readDashboardMethod } from "./method";
import general_constant from "../../../helpers/general_constant.json";

function* readDashboard({ payload: date }) {
  const response = yield call(readDashboardMethod, date);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readDashboardFulfilled(response));
  } else {
    yield put(readDashboardReject(response));
  }
}

export function* watchReadDashboard() {
  yield takeLatest(READ_DASHBOARD, readDashboard);
}

function* DashboardSaga() {
  yield all([fork(watchReadDashboard)]);
}

export default DashboardSaga;
