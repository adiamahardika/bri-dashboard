import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  READ_REGIONAL,
  READ_TERMINAL_BY_REGIONAL,
  READ_KANWIL,
  READ_TERMINAL_BY_KANWIL,
  READ_KANCA,
  READ_TERMINAL_BY_KANCA,
  READ_JENIS_TRANSAKSI,
} from "./actionTypes";
import {
  readRegionalFulfilled,
  readRegionalReject,
  readTerminalByRegionalFulfilled,
  readTerminalByRegionalReject,
  readKanwilFulfilled,
  readKanwilReject,
  readTerminalByKanwilFulfilled,
  readTerminalByKanwilReject,
  readKancaFulfilled,
  readKancaReject,
  readTerminalByKancaFulfilled,
  readTerminalByKancaReject,
  readJenisTransaksiFulfilled,
  readJenisTransaksiReject,
} from "./actions";
import {
  readRegionalMethod,
  readTerminalByRegionalMethod,
  readKanwilMethod,
  readTerminalByKanwilMethod,
  readKancaMethod,
  readTerminalByKancaMethod,
  readJenisTransaksiMethod,
} from "./method";
import general_constant from "../../../helpers/general_constant.json";

function* readRegional() {
  const response = yield call(readRegionalMethod);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readRegionalFulfilled(response));
  } else {
    yield put(readRegionalReject(response));
  }
}
function* readTerminalByRegional({ payload: data }) {
  const response = yield call(readTerminalByRegionalMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readTerminalByRegionalFulfilled(response));
  } else {
    yield put(readTerminalByRegionalReject(response));
  }
}
function* readKanwil({ payload: data }) {
  const response = yield call(readKanwilMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readKanwilFulfilled(response));
  } else {
    yield put(readKanwilReject(response));
  }
}
function* readTerminalByKanwil({ payload: data }) {
  const response = yield call(readTerminalByKanwilMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readTerminalByKanwilFulfilled(response));
  } else {
    yield put(readTerminalByKanwilReject(response));
  }
}
function* readKanca({ payload: data }) {
  const response = yield call(readKancaMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readKancaFulfilled(response));
  } else {
    yield put(readKancaReject(response));
  }
}
function* readTerminalByKanca({ payload: data }) {
  const response = yield call(readTerminalByKancaMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readTerminalByKancaFulfilled(response));
  } else {
    yield put(readTerminalByKancaReject(response));
  }
}
function* readJenisTransaksi() {
  const response = yield call(readJenisTransaksiMethod);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readJenisTransaksiFulfilled(response));
  } else {
    yield put(readJenisTransaksiReject(response));
  }
}

export function* watchReadRegional() {
  yield takeLatest(READ_REGIONAL, readRegional);
}
export function* watchReadTerminalByRegional() {
  yield takeLatest(READ_TERMINAL_BY_REGIONAL, readTerminalByRegional);
}
export function* watchReadKanwil() {
  yield takeLatest(READ_KANWIL, readKanwil);
}
export function* watchReadTerminalByKanwil() {
  yield takeLatest(READ_TERMINAL_BY_KANWIL, readTerminalByKanwil);
}
export function* watchReadKanca() {
  yield takeLatest(READ_KANCA, readKanca);
}
export function* watchReadTerminalByKanca() {
  yield takeLatest(READ_TERMINAL_BY_KANCA, readTerminalByKanca);
}
export function* watchReadJenisTransaksi() {
  yield takeLatest(READ_JENIS_TRANSAKSI, readJenisTransaksi);
}

function* PropertiesSaga() {
  yield all([
    fork(watchReadRegional),
    fork(watchReadTerminalByRegional),
    fork(watchReadKanwil),
    fork(watchReadTerminalByKanwil),
    fork(watchReadKanca),
    fork(watchReadTerminalByKanca),
    fork(watchReadJenisTransaksi),
  ]);
}

export default PropertiesSaga;
