import {
  READ_REGIONAL,
  READ_REGIONAL_REJECT,
  READ_REGIONAL_FULFILLED,
  READ_TERMINAL_BY_REGIONAL,
  READ_TERMINAL_BY_REGIONAL_REJECT,
  READ_TERMINAL_BY_REGIONAL_FULFILLED,
  READ_KANWIL,
  READ_KANWIL_REJECT,
  READ_KANWIL_FULFILLED,
  READ_TERMINAL_BY_KANWIL,
  READ_TERMINAL_BY_KANWIL_REJECT,
  READ_TERMINAL_BY_KANWIL_FULFILLED,
  READ_KANCA,
  READ_KANCA_REJECT,
  READ_KANCA_FULFILLED,
  READ_TERMINAL_BY_KANCA,
  READ_TERMINAL_BY_KANCA_REJECT,
  READ_TERMINAL_BY_KANCA_FULFILLED,
  READ_JENIS_TRANSAKSI,
  READ_JENIS_TRANSAKSI_REJECT,
  READ_JENIS_TRANSAKSI_FULFILLED,
} from "./actionTypes";

// Get Regional
export const readRegional = () => {
  return {
    type: READ_REGIONAL,
  };
};

export const readRegionalReject = (payload) => {
  return {
    type: READ_REGIONAL_REJECT,
    payload: payload,
  };
};

export const readRegionalFulfilled = (data) => {
  return {
    type: READ_REGIONAL_FULFILLED,
    payload: data,
  };
};

// Get Terminal By Regional
export const readTerminalByRegional = (data) => {
  return {
    type: READ_TERMINAL_BY_REGIONAL,
    payload: data,
  };
};

export const readTerminalByRegionalReject = (payload) => {
  return {
    type: READ_TERMINAL_BY_REGIONAL_REJECT,
    payload: payload,
  };
};

export const readTerminalByRegionalFulfilled = (data) => {
  return {
    type: READ_TERMINAL_BY_REGIONAL_FULFILLED,
    payload: data,
  };
};

// Get Kanwil
export const readKanwil = (data) => {
  return {
    type: READ_KANWIL,
    payload: data,
  };
};

export const readKanwilReject = (payload) => {
  return {
    type: READ_KANWIL_REJECT,
    payload: payload,
  };
};

export const readKanwilFulfilled = (data) => {
  return {
    type: READ_KANWIL_FULFILLED,
    payload: data,
  };
};

// Get Terminal By Kanwil
export const readTerminalByKanwil = (data) => {
  return {
    type: READ_TERMINAL_BY_KANWIL,
    payload: data,
  };
};

export const readTerminalByKanwilReject = (payload) => {
  return {
    type: READ_TERMINAL_BY_KANWIL_REJECT,
    payload: payload,
  };
};

export const readTerminalByKanwilFulfilled = (data) => {
  return {
    type: READ_TERMINAL_BY_KANWIL_FULFILLED,
    payload: data,
  };
};

// Get Kanca
export const readKanca = (data) => {
  return {
    type: READ_KANCA,
    payload: data,
  };
};

export const readKancaReject = (payload) => {
  return {
    type: READ_KANCA_REJECT,
    payload: payload,
  };
};

export const readKancaFulfilled = (data) => {
  return {
    type: READ_KANCA_FULFILLED,
    payload: data,
  };
};

// Get Terminal By Kanca
export const readTerminalByKanca = (data) => {
  return {
    type: READ_TERMINAL_BY_KANCA,
    payload: data,
  };
};

export const readTerminalByKancaReject = (payload) => {
  return {
    type: READ_TERMINAL_BY_KANCA_REJECT,
    payload: payload,
  };
};

export const readTerminalByKancaFulfilled = (data) => {
  return {
    type: READ_TERMINAL_BY_KANCA_FULFILLED,
    payload: data,
  };
};

// Get Jenis Transaksi
export const readJenisTransaksi = () => {
  return {
    type: READ_JENIS_TRANSAKSI,
  };
};

export const readJenisTransaksiReject = (payload) => {
  return {
    type: READ_JENIS_TRANSAKSI_REJECT,
    payload: payload,
  };
};

export const readJenisTransaksiFulfilled = (data) => {
  return {
    type: READ_JENIS_TRANSAKSI_FULFILLED,
    payload: data,
  };
};
