import {
  READ_REGIONAL,
  READ_REGIONAL_REJECT,
  READ_REGIONAL_FULFILLED,
  READ_TERMINAL_BY_REGIONAL,
  READ_TERMINAL_BY_REGIONAL_REJECT,
  READ_TERMINAL_BY_REGIONAL_FULFILLED,
  READ_KANWIL,
  READ_KANWIL_REJECT,
  READ_KANWIL_FULFILLED,
  READ_TERMINAL_BY_KANWIL,
  READ_TERMINAL_BY_KANWIL_REJECT,
  READ_TERMINAL_BY_KANWIL_FULFILLED,
  READ_KANCA,
  READ_KANCA_REJECT,
  READ_KANCA_FULFILLED,
  READ_TERMINAL_BY_KANCA,
  READ_TERMINAL_BY_KANCA_REJECT,
  READ_TERMINAL_BY_KANCA_FULFILLED,
  READ_JENIS_TRANSAKSI,
  READ_JENIS_TRANSAKSI_REJECT,
  READ_JENIS_TRANSAKSI_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_jenis_transaksi: null,
  list_regional: [],
  list_all_regional: null,
  list_kanwil: [],
  list_all_kanwil: null,
  list_kanca: [],
  list_all_kanca: null,
  list_terminal: [],
  message_jenis_transaksi: null,
  message_kanwil: null,
  message_kanca: null,
  message_regional: null,
  message_terminal: null,
  response_code_jenis_transaksi: null,
  response_code_kanwil: null,
  response_code_kanca: null,
  response_code_regional: null,
  response_code_terminal: null,
  loading: false,
};

const Properties = (state = INIT_STATE, action) => {
  switch (action.type) {
    // Get Regional
    case READ_REGIONAL:
      return {
        ...state,
        loading: true,
        list_regional: [],
      };
    case READ_REGIONAL_REJECT:
      return {
        ...state,
        response_code_regional: action.payload.responseCode,
        message_regional: action.payload.description,
        loading: true,
      };
    case READ_REGIONAL_FULFILLED:
      if (action.payload.listRegional) {
        action.payload.listRegional.map((value) => {
          return state.list_regional.push({
            label: value.name,
            value: value.id,
          });
        });
      }
      return {
        ...state,
        list_all_regional: action.payload.listRegional,
        response_code_regional: action.payload.responseCode,
        message_regional: action.payload.description,
        loading: false,
      };

    // Get Terminal By Regional
    case READ_TERMINAL_BY_REGIONAL:
      return {
        ...state,
        loading: true,
        list_terminal: [],
      };
    case READ_TERMINAL_BY_REGIONAL_REJECT:
      return {
        ...state,
        response_code_terminal: action.payload.responseCode,
        message_terminal: action.payload.description,
        loading: true,
      };
    case READ_TERMINAL_BY_REGIONAL_FULFILLED:
      if (action.payload.listTerminalByRegional) {
        action.payload.listTerminalByRegional.map((value) => {
          return state.list_terminal.push({
            label: value.terminalId,
            value: value.terminalId,
          });
        });
      }
      return {
        ...state,
        response_code_terminal: action.payload.responseCode,
        message_terminal: action.payload.description,
        loading: false,
      };

    // Get Kanwil
    case READ_KANWIL:
      return {
        ...state,
        loading: true,
        list_kanwil: [],
      };
    case READ_KANWIL_REJECT:
      return {
        ...state,
        response_code_kanwil: action.payload.responseCode,
        message_kanwil: action.payload.description,
        loading: true,
      };
    case READ_KANWIL_FULFILLED:
      if (action.payload.listKanwil) {
        action.payload.listKanwil.map((value) => {
          return state.list_kanwil.push({
            label: value.name,
            value: value.id,
          });
        });
      }
      return {
        ...state,
        list_all_kanwil: action.payload.listKanwil,
        response_code_kanwil: action.payload.responseCode,
        message_kanwil: action.payload.description,
        loading: false,
      };

    // Get Terminal By Kanwil
    case READ_TERMINAL_BY_KANWIL:
      return {
        ...state,
        loading: true,
        list_terminal: [],
      };
    case READ_TERMINAL_BY_KANWIL_REJECT:
      return {
        ...state,
        response_code_terminal: action.payload.responseCode,
        message_terminal: action.payload.description,
        loading: true,
      };
    case READ_TERMINAL_BY_KANWIL_FULFILLED:
      if (action.payload.listTerminalByKanwil) {
        action.payload.listTerminalByKanwil.map((value) => {
          return state.list_terminal.push({
            label: value.terminalId,
            value: value.terminalId,
          });
        });
      }
      return {
        ...state,
        response_code_terminal: action.payload.responseCode,
        message_terminal: action.payload.description,
        loading: false,
      };

    // Get Kanca
    case READ_KANCA:
      return {
        ...state,
        loading: true,
        list_kanca: [],
      };
    case READ_KANCA_REJECT:
      return {
        ...state,
        response_code_kanca: action.payload.responseCode,
        message_kanca: action.payload.description,
        loading: true,
      };
    case READ_KANCA_FULFILLED:
      if (action.payload.listKanca) {
        action.payload.listKanca.map((value) => {
          return state.list_kanca.push({
            label: value.name,
            value: value.id,
          });
        });
      }
      return {
        ...state,
        list_all_kanca: action.payload.listKanca,
        response_code_kanca: action.payload.responseCode,
        message_kanca: action.payload.description,
        loading: false,
      };

    // Get Terminal By Kanca
    case READ_TERMINAL_BY_KANCA:
      return {
        ...state,
        loading: true,
        list_terminal: [],
      };
    case READ_TERMINAL_BY_KANCA_REJECT:
      return {
        ...state,
        response_code_terminal: action.payload.responseCode,
        message_terminal: action.payload.description,
        loading: true,
      };
    case READ_TERMINAL_BY_KANCA_FULFILLED:
      if (action.payload.listTerminalByKanca) {
        action.payload.listTerminalByKanca.map((value) => {
          return state.list_terminal.push({
            label: value.terminalId,
            value: value.terminalId,
          });
        });
      }
      return {
        ...state,
        response_code_terminal: action.payload.responseCode,
        message_terminal: action.payload.description,
        loading: false,
      };

    // Get Jenis Transaksi
    case READ_JENIS_TRANSAKSI:
      return {
        ...state,
        loading: true,
      };
    case READ_JENIS_TRANSAKSI_REJECT:
      return {
        ...state,
        response_code_jenis_transaksi: action.payload.responseCode,
        message_jenis_transaksi: action.payload.description,
        loading: true,
      };
    case READ_JENIS_TRANSAKSI_FULFILLED:
      return {
        ...state,
        list_jenis_transaksi: action.payload.jenisTransaksi,
        response_code_jenis_transaksi: action.payload.responseCode,
        message_jenis_transaksi: action.payload.description,
        loading: false,
      };
    default:
      return state;
  }
};

export default Properties;
