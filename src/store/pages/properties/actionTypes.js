export const READ_REGIONAL = "READ_REGIONAL";
export const READ_REGIONAL_REJECT = "READ_REGIONAL_REJECT";
export const READ_REGIONAL_FULFILLED = "READ_REGIONAL_FULFILLED";

export const READ_TERMINAL_BY_REGIONAL = "READ_TERMINAL_BY_REGIONAL";
export const READ_TERMINAL_BY_REGIONAL_REJECT =
  "READ_TERMINAL_BY_REGIONAL_REJECT";
export const READ_TERMINAL_BY_REGIONAL_FULFILLED =
  "READ_TERMINAL_BY_REGIONAL_FULFILLED";

export const READ_KANWIL = "READ_KANWIL";
export const READ_KANWIL_REJECT = "READ_KANWIL_REJECT";
export const READ_KANWIL_FULFILLED = "READ_KANWIL_FULFILLED";

export const READ_TERMINAL_BY_KANWIL = "READ_TERMINAL_BY_KANWIL";
export const READ_TERMINAL_BY_KANWIL_REJECT = "READ_TERMINAL_BY_KANWIL_REJECT";
export const READ_TERMINAL_BY_KANWIL_FULFILLED =
  "READ_TERMINAL_BY_KANWIL_FULFILLED";

export const READ_KANCA = "READ_KANCA";
export const READ_KANCA_REJECT = "READ_KANCA_REJECT";
export const READ_KANCA_FULFILLED = "READ_KANCA_FULFILLED";

export const READ_TERMINAL_BY_KANCA = "READ_TERMINAL_BY_KANCA";
export const READ_TERMINAL_BY_KANCA_REJECT = "READ_TERMINAL_BY_KANCA_REJECT";
export const READ_TERMINAL_BY_KANCA_FULFILLED =
  "READ_TERMINAL_BY_KANCA_FULFILLED";

export const READ_JENIS_TRANSAKSI = "READ_JENIS_TRANSAKSI";
export const READ_JENIS_TRANSAKSI_REJECT = "READ_JENIS_TRANSAKSI_REJECT";
export const READ_JENIS_TRANSAKSI_FULFILLED = "READ_JENIS_TRANSAKSI_FULFILLED";
