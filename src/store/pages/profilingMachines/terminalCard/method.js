import general_constant from "../../../../helpers/general_constant.json";
require("dotenv").config();

export const readDetailTerminalCardMethod = async (id) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/terminal-card/get-terminal-card/${id}`,
    {
      method: "GET",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const createTerminalCardMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/terminal-card/add-list-card-terminal`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};
