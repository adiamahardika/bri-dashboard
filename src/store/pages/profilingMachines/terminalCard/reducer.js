import {
  READ_DETAIL_TERMINAL_CARD,
  READ_DETAIL_TERMINAL_CARD_REJECT,
  READ_DETAIL_TERMINAL_CARD_FULFILLED,
  CREATE_TERMINAL_CARD,
  CREATE_TERMINAL_CARD_REJECT,
  CREATE_TERMINAL_CARD_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_terminal_card: null,
  list_detail_terminal_card: null,
  message_terminal_card: null,
  response_code_terminal_card: null,
  loading: false,
};

const TerminalCard = (state = INIT_STATE, action) => {
  switch (action.type) {
    // Get Detail Terminal Card
    case READ_DETAIL_TERMINAL_CARD:
      return {
        ...state,
        loading: true,
      };
    case READ_DETAIL_TERMINAL_CARD_REJECT:
      return {
        ...state,
        response_code_terminal_card: action.payload.responseCode,
        message_terminal_card: action.payload.description,
        loading: true,
      };
    case READ_DETAIL_TERMINAL_CARD_FULFILLED:
      return {
        ...state,
        list_detail_terminal_card: action.payload.content,
        response_code_terminal_card: action.payload.responseCode,
        message_terminal_card: action.payload.description,
        loading: false,
      };

    // Create Terminal Card
    case CREATE_TERMINAL_CARD:
      return {
        ...state,
        loading: true,
      };
    case CREATE_TERMINAL_CARD_REJECT:
      return {
        ...state,
        response_code_terminal_card: action.payload.responseCode,
        message_terminal_card: action.payload.description,
        loading: true,
      };
    case CREATE_TERMINAL_CARD_FULFILLED:
      return {
        ...state,
        response_code_terminal_card: action.payload.responseCode,
        message_terminal_card: action.payload.description,
        loading: false,
      };
    default:
      return state;
  }
};

export default TerminalCard;
