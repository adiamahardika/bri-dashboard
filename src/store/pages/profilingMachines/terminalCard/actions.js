import {
  READ_DETAIL_TERMINAL_CARD,
  READ_DETAIL_TERMINAL_CARD_REJECT,
  READ_DETAIL_TERMINAL_CARD_FULFILLED,
  CREATE_TERMINAL_CARD,
  CREATE_TERMINAL_CARD_REJECT,
  CREATE_TERMINAL_CARD_FULFILLED,
} from "./actionTypes";

// Get Detail Terminal Card
export const readDetailTerminalCard = (id) => {
  return {
    type: READ_DETAIL_TERMINAL_CARD,
    payload: id,
  };
};

export const readDetailTerminalCardReject = (payload) => {
  return {
    type: READ_DETAIL_TERMINAL_CARD_REJECT,
    payload: payload,
  };
};

export const readDetailTerminalCardFulfilled = (data) => {
  return {
    type: READ_DETAIL_TERMINAL_CARD_FULFILLED,
    payload: data,
  };
};

// Create Terminal Card
export const createTerminalCard = (data) => {
  return {
    type: CREATE_TERMINAL_CARD,
    payload: data,
  };
};

export const createTerminalCardReject = (payload) => {
  return {
    type: CREATE_TERMINAL_CARD_REJECT,
    payload: payload,
  };
};

export const createTerminalCardFulfilled = (data) => {
  return {
    type: CREATE_TERMINAL_CARD_FULFILLED,
    payload: data,
  };
};
