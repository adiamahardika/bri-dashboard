import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import { READ_DETAIL_TERMINAL_CARD, CREATE_TERMINAL_CARD } from "./actionTypes";
import {
  readDetailTerminalCardFulfilled,
  readDetailTerminalCardReject,
  createTerminalCardFulfilled,
  createTerminalCardReject,
} from "./actions";
import {
  readDetailTerminalCardMethod,
  createTerminalCardMethod,
} from "./method";
import general_constant from "../../../../helpers/general_constant.json";

function* readDetailTerminalCard({ payload: id }) {
  const response = yield call(readDetailTerminalCardMethod, id);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readDetailTerminalCardFulfilled(response));
  } else {
    yield put(readDetailTerminalCardReject(response));
  }
}

function* createTerminalCard({ payload: data }) {
  const response = yield call(createTerminalCardMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(createTerminalCardFulfilled(response));
  } else {
    yield put(createTerminalCardReject(response));
  }
}

export function* watchReadDetailTerminalCard() {
  yield takeLatest(READ_DETAIL_TERMINAL_CARD, readDetailTerminalCard);
}
export function* watchCreateTerminalCard() {
  yield takeLatest(CREATE_TERMINAL_CARD, createTerminalCard);
}

function* PropertiesSaga() {
  yield all([fork(watchReadDetailTerminalCard), fork(watchCreateTerminalCard)]);
}

export default PropertiesSaga;
