import {
  READ_DETAIL_TERMINAL_CONFIGURATION,
  READ_DETAIL_TERMINAL_CONFIGURATION_REJECT,
  READ_DETAIL_TERMINAL_CONFIGURATION_FULFILLED,
  CREATE_TERMINAL_CONFIGURATION,
  CREATE_TERMINAL_CONFIGURATION_REJECT,
  CREATE_TERMINAL_CONFIGURATION_FULFILLED,
} from "./actionTypes";

// Get Detail Terminal Configuration
export const readDetailTerminalConfiguration = (terminalId) => {
  return {
    type: READ_DETAIL_TERMINAL_CONFIGURATION,
    payload: terminalId,
  };
};

export const readDetailTerminalConfigurationReject = (payload) => {
  return {
    type: READ_DETAIL_TERMINAL_CONFIGURATION_REJECT,
    payload: payload,
  };
};

export const readDetailTerminalConfigurationFulfilled = (data) => {
  return {
    type: READ_DETAIL_TERMINAL_CONFIGURATION_FULFILLED,
    payload: data,
  };
};

// Create Terminal Configuration
export const createTerminalConfiguration = (data) => {
  return {
    type: CREATE_TERMINAL_CONFIGURATION,
    payload: data,
  };
};

export const createTerminalConfigurationReject = (payload) => {
  return {
    type: CREATE_TERMINAL_CONFIGURATION_REJECT,
    payload: payload,
  };
};

export const createTerminalConfigurationFulfilled = (data) => {
  return {
    type: CREATE_TERMINAL_CONFIGURATION_FULFILLED,
    payload: data,
  };
};
