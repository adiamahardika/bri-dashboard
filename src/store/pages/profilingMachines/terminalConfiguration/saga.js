import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  READ_DETAIL_TERMINAL_CONFIGURATION,
  CREATE_TERMINAL_CONFIGURATION,
} from "./actionTypes";
import {
  readDetailTerminalConfigurationFulfilled,
  readDetailTerminalConfigurationReject,
  createTerminalConfigurationFulfilled,
  createTerminalConfigurationReject,
} from "./actions";
import {
  readDetailTerminalConfigurationMethod,
  createTerminalConfigurationMethod,
} from "./method";
import general_constant from "../../../../helpers/general_constant.json";

function* readDetailTerminalConfiguration({ payload: terminalId }) {
  const response = yield call(
    readDetailTerminalConfigurationMethod,
    terminalId
  );
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readDetailTerminalConfigurationFulfilled(response));
  } else {
    yield put(readDetailTerminalConfigurationReject(response));
  }
}
function* createTerminalConfiguration({ payload: data }) {
  const response = yield call(createTerminalConfigurationMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(createTerminalConfigurationFulfilled(response));
  } else {
    yield put(createTerminalConfigurationReject(response));
  }
}

export function* watchReadDetailTerminalConfiguration() {
  yield takeLatest(
    READ_DETAIL_TERMINAL_CONFIGURATION,
    readDetailTerminalConfiguration
  );
}
export function* watchCreateTerminalConfiguration() {
  yield takeLatest(CREATE_TERMINAL_CONFIGURATION, createTerminalConfiguration);
}

function* TerminalConfigurationSaga() {
  yield all([
    fork(watchReadDetailTerminalConfiguration),
    fork(watchCreateTerminalConfiguration),
  ]);
}

export default TerminalConfigurationSaga;
