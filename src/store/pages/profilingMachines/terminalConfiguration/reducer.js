import {
  READ_DETAIL_TERMINAL_CONFIGURATION,
  READ_DETAIL_TERMINAL_CONFIGURATION_REJECT,
  READ_DETAIL_TERMINAL_CONFIGURATION_FULFILLED,
  CREATE_TERMINAL_CONFIGURATION,
  CREATE_TERMINAL_CONFIGURATION_REJECT,
  CREATE_TERMINAL_CONFIGURATION_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_detail_terminal_configuration: null,
  message_terminal_configuration: null,
  response_code_terminal_configuration: null,
  loading: false,
};

const TerminalConfiguration = (state = INIT_STATE, action) => {
  switch (action.type) {
    // Get Detail Terminal Configuration
    case READ_DETAIL_TERMINAL_CONFIGURATION:
      return {
        ...state,
        loading: true,
      };
    case READ_DETAIL_TERMINAL_CONFIGURATION_REJECT:
      return {
        ...state,
        response_code_terminal_configuration: action.payload.responseCode,
        message_terminal_configuration: action.payload.description,
        loading: true,
      };
    case READ_DETAIL_TERMINAL_CONFIGURATION_FULFILLED:
      return {
        ...state,
        list_detail_terminal_configuration: action.payload.content,
        response_code_terminal_configuration: action.payload.responseCode,
        message_terminal_configuration: action.payload.description,
        loading: false,
      };

    // Create Terminal Configuration
    case CREATE_TERMINAL_CONFIGURATION:
      return {
        ...state,
        loading: true,
      };
    case CREATE_TERMINAL_CONFIGURATION_REJECT:
      return {
        ...state,
        response_code_terminal_configuration: action.payload.responseCode,
        message_terminal_configuration: action.payload.description,
        loading: true,
      };
    case CREATE_TERMINAL_CONFIGURATION_FULFILLED:
      return {
        ...state,
        response_code_terminal_configuration: action.payload.responseCode,
        message_terminal_configuration: action.payload.description,
        loading: false,
      };

    default:
      return state;
  }
};

export default TerminalConfiguration;
