import general_constant from "../../../../helpers/general_constant.json";
require("dotenv").config();

export const readAllTerminalMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/terminal-profile/getall`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const createTerminalMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/terminal-profile/add-terminal`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const deleteTerminalMethod = async (data) => {
  let newData = data.data;
  const index = await newData.listTerminalId.indexOf(data.terminalId);
  await newData.listTerminalId.splice(index, 1);

  const response = await fetch(
    `${process.env.REACT_APP_API}/terminal-profile/delete-terminal/${data.terminalId}`,
    {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    }
  );
  const responseGet = await fetch(
    `${process.env.REACT_APP_API}/terminal-profile/getall`,
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(newData),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else if (
    response.json().responseCode === general_constant.success_response_code
  ) {
    return response.json();
  } else {
    return responseGet.json();
  }
};

export const updateTerminalMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/terminal-profile/edit-terminal`,
    {
      method: "PUT",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};

export const changePasswordTerminalMethod = async (data) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/terminal-profile/edit-password-terminal`,
    {
      method: "PUT",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
      body: JSON.stringify(data),
    }
  );
  if (response.status === general_constant.unauthorized_status) {
    localStorage.clear();
    window.location.assign("/login");
  } else {
    return response.json();
  }
};
