import {
  READ_ALL_TERMINAL,
  READ_ALL_TERMINAL_REJECT,
  READ_ALL_TERMINAL_FULFILLED,
  CREATE_TERMINAL,
  CREATE_TERMINAL_REJECT,
  CREATE_TERMINAL_FULFILLED,
  DELETE_TERMINAL,
  DELETE_TERMINAL_REJECT,
  DELETE_TERMINAL_FULFILLED,
  UPDATE_TERMINAL,
  UPDATE_TERMINAL_REJECT,
  UPDATE_TERMINAL_FULFILLED,
  CHANGE_PASSWORD_TERMINAL,
  CHANGE_PASSWORD_TERMINAL_REJECT,
  CHANGE_PASSWORD_TERMINAL_FULFILLED,
} from "./actionTypes";

// Get All Terminal
export const readAllTerminal = (data) => {
  return {
    type: READ_ALL_TERMINAL,
    payload: data,
  };
};

export const readAllTerminalReject = (payload) => {
  return {
    type: READ_ALL_TERMINAL_REJECT,
    payload: payload,
  };
};

export const readAllTerminalFulfilled = (data) => {
  return {
    type: READ_ALL_TERMINAL_FULFILLED,
    payload: data,
  };
};

// Create Terminal
export const createTerminal = (data) => {
  return {
    type: CREATE_TERMINAL,
    payload: data,
  };
};

export const createTerminalReject = (payload) => {
  return {
    type: CREATE_TERMINAL_REJECT,
    payload: payload,
  };
};

export const createTerminalFulfilled = (data) => {
  return {
    type: CREATE_TERMINAL_FULFILLED,
    payload: data,
  };
};

// Delete Terminal
export const deleteTerminal = (data) => {
  return {
    type: DELETE_TERMINAL,
    payload: data,
  };
};

export const deleteTerminalReject = (payload) => {
  return {
    type: DELETE_TERMINAL_REJECT,
    payload: payload,
  };
};

export const deleteTerminalFulfilled = (data) => {
  return {
    type: DELETE_TERMINAL_FULFILLED,
    payload: data,
  };
};

// Update Terminal
export const updateTerminal = (data) => {
  return {
    type: UPDATE_TERMINAL,
    payload: data,
  };
};

export const updateTerminalReject = (payload) => {
  return {
    type: UPDATE_TERMINAL_REJECT,
    payload: payload,
  };
};

export const updateTerminalFulfilled = (data) => {
  return {
    type: UPDATE_TERMINAL_FULFILLED,
    payload: data,
  };
};

// Change Password Terminal
export const changePasswordTerminal = (data) => {
  return {
    type: CHANGE_PASSWORD_TERMINAL,
    payload: data,
  };
};

export const changePasswordTerminalReject = (payload) => {
  return {
    type: CHANGE_PASSWORD_TERMINAL_REJECT,
    payload: payload,
  };
};

export const changePasswordTerminalFulfilled = (data) => {
  return {
    type: CHANGE_PASSWORD_TERMINAL_FULFILLED,
    payload: data,
  };
};
