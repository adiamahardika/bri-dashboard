import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  READ_ALL_TERMINAL,
  CREATE_TERMINAL,
  DELETE_TERMINAL,
  UPDATE_TERMINAL,
  CHANGE_PASSWORD_TERMINAL,
} from "./actionTypes";
import {
  readAllTerminalFulfilled,
  readAllTerminalReject,
  createTerminalFulfilled,
  createTerminalReject,
  deleteTerminalFulfilled,
  deleteTerminalReject,
  updateTerminalFulfilled,
  updateTerminalReject,
  changePasswordTerminalFulfilled,
  changePasswordTerminalReject,
} from "./actions";
import {
  readAllTerminalMethod,
  createTerminalMethod,
  deleteTerminalMethod,
  updateTerminalMethod,
  changePasswordTerminalMethod,
} from "./method";
import general_constant from "../../../../helpers/general_constant.json";

function* readAllTerminal({ payload: data }) {
  const response = yield call(readAllTerminalMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(readAllTerminalFulfilled(response));
  } else {
    yield put(readAllTerminalReject(response));
  }
}
function* createTerminal({ payload: data }) {
  const response = yield call(createTerminalMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(createTerminalFulfilled(response));
  } else {
    yield put(createTerminalReject(response));
  }
}
function* deleteTerminal({ payload: data }) {
  const response = yield call(deleteTerminalMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(deleteTerminalFulfilled(response));
  } else {
    yield put(deleteTerminalReject(response));
  }
}
function* updateTerminal({ payload: data }) {
  const response = yield call(updateTerminalMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(updateTerminalFulfilled(response));
  } else {
    yield put(updateTerminalReject(response));
  }
}
function* changePasswordTerminal({ payload: data }) {
  const response = yield call(changePasswordTerminalMethod, data);
  if (response.responseCode === general_constant.success_response_code) {
    yield put(changePasswordTerminalFulfilled(response));
  } else {
    yield put(changePasswordTerminalReject(response));
  }
}

export function* watchReadAllTerminal() {
  yield takeLatest(READ_ALL_TERMINAL, readAllTerminal);
}
export function* watchCreateTerminal() {
  yield takeLatest(CREATE_TERMINAL, createTerminal);
}
export function* watchDeleteTerminal() {
  yield takeLatest(DELETE_TERMINAL, deleteTerminal);
}
export function* watchUpdateTerminal() {
  yield takeLatest(UPDATE_TERMINAL, updateTerminal);
}
export function* watchChangePasswordTerminal() {
  yield takeLatest(CHANGE_PASSWORD_TERMINAL, changePasswordTerminal);
}

function* TerminalSaga() {
  yield all([
    fork(watchReadAllTerminal),
    fork(watchCreateTerminal),
    fork(watchDeleteTerminal),
    fork(watchUpdateTerminal),
    fork(watchChangePasswordTerminal),
  ]);
}

export default TerminalSaga;
