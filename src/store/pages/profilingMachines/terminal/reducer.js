import {
  READ_ALL_TERMINAL,
  READ_ALL_TERMINAL_REJECT,
  READ_ALL_TERMINAL_FULFILLED,
  CREATE_TERMINAL,
  CREATE_TERMINAL_REJECT,
  CREATE_TERMINAL_FULFILLED,
  DELETE_TERMINAL,
  DELETE_TERMINAL_REJECT,
  DELETE_TERMINAL_FULFILLED,
  UPDATE_TERMINAL,
  UPDATE_TERMINAL_REJECT,
  UPDATE_TERMINAL_FULFILLED,
  CHANGE_PASSWORD_TERMINAL,
  CHANGE_PASSWORD_TERMINAL_REJECT,
  CHANGE_PASSWORD_TERMINAL_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  list_all_terminal: null,
  message_all_terminal: null,
  response_code_all_terminal: null,
  active_page_terminal: null,
  total_pages_terminal: null,
  loading: false,
};

const Terminal = (state = INIT_STATE, action) => {
  switch (action.type) {
    // Get All Terminal
    case READ_ALL_TERMINAL:
      return {
        ...state,
        loading: true,
      };
    case READ_ALL_TERMINAL_REJECT:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: true,
      };
    case READ_ALL_TERMINAL_FULFILLED:
      return {
        ...state,
        list_all_terminal: action.payload.content,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        total_pages_terminal: action.payload.totalPages,
        active_page_terminal: action.payload.page,
        loading: false,
      };

    // Create Terminal
    case CREATE_TERMINAL:
      return {
        ...state,
        loading: true,
      };
    case CREATE_TERMINAL_REJECT:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: true,
      };
    case CREATE_TERMINAL_FULFILLED:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: false,
      };

    // Delete Terminal
    case DELETE_TERMINAL:
      return {
        ...state,
        loading: true,
      };
    case DELETE_TERMINAL_REJECT:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: true,
      };
    case DELETE_TERMINAL_FULFILLED:
      return {
        ...state,
        list_all_terminal: action.payload.content,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        total_pages_terminal: action.payload.totalPages,
        loading: false,
      };

    // Update Terminal
    case UPDATE_TERMINAL:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_TERMINAL_REJECT:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: true,
      };
    case UPDATE_TERMINAL_FULFILLED:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: false,
      };

    // Change Pass Terminal
    case CHANGE_PASSWORD_TERMINAL:
      return {
        ...state,
        loading: true,
      };
    case CHANGE_PASSWORD_TERMINAL_REJECT:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: true,
      };
    case CHANGE_PASSWORD_TERMINAL_FULFILLED:
      return {
        ...state,
        response_code_all_terminal: action.payload.responseCode,
        message_all_terminal: action.payload.description,
        loading: false,
      };
    default:
      return state;
  }
};

export default Terminal;
