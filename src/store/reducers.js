import { combineReducers } from "redux";

// Front
import Layout from "./layout/reducer";
import User from "./pages/users/reducer";
import Role from "./pages/role/reducer";
import Permission from "./pages/permission/reducer";
import Report from "./pages/report/reducer";
import AuditTrail from "./pages/auditTrail/reducer";
import Properties from "./pages/properties/reducer";
import Captcha from "./auth/captcha/reducer";
import Dashboard from "./pages/dashboard/reducer";
import Profile from "./auth/profile/reducer";
import Card from "./pages/masterData/card/reducer";
import Terminal from "./pages/profilingMachines/terminal/reducer";
import TerminalCard from "./pages/profilingMachines/terminalCard/reducer";
import TerminalConfiguration from "./pages/profilingMachines/terminalConfiguration/reducer";
import MasterTerminalConfiguration from "./pages/masterData/terminalConfiguration/reducer";
import Monitoring from "./pages/monitoring/reducer";
// Authentication
import Login from "./auth/login/reducer";
import Account from "./auth/register/reducer";
import ForgetPassword from "./auth/forgetpwd/reducer";

const rootReducer = combineReducers({
  // public
  Layout,
  Login,
  Account,
  ForgetPassword,
  Profile,
  User,
  Role,
  Permission,
  Report,
  AuditTrail,
  Properties,
  Captcha,
  Dashboard,
  Card,
  Terminal,
  TerminalCard,
  TerminalConfiguration,
  MasterTerminalConfiguration,
  Monitoring,
});

export default rootReducer;
