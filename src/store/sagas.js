import { all } from "redux-saga/effects";

//public
import AccountSaga from "./auth/register/saga";
import AuthSaga from "./auth/login/saga";
import ForgetSaga from "./auth/forgetpwd/saga";
import ProfileSaga from "./auth/profile/saga";
import LayoutSaga from "./layout/saga";
import UserSaga from "./pages/users/saga";
import RoleSaga from "./pages/role/saga";
import PermissionSaga from "./pages/permission/saga";
import ReportSaga from "./pages/report/saga";
import AuditTrailSaga from "./pages/auditTrail/saga";
import PropertiesSaga from "./pages/properties/saga";
import CaptchaSaga from "./auth/captcha/saga";
import DashboardSaga from "./pages/dashboard/saga";
import CardSaga from "./pages/masterData/card/saga";
import TerminalSaga from "./pages/profilingMachines/terminal/saga";
import TerminalCardSaga from "./pages/profilingMachines/terminalCard/saga";
import TerminalConfigurationSaga from "./pages/profilingMachines/terminalConfiguration/saga";
import MasterTerminalConfigurationSaga from "./pages/masterData/terminalConfiguration/saga";
import MonitoringSaga from "./pages/monitoring/saga";
export default function* rootSaga() {
  yield all([
    //public
    AccountSaga(),
    AuthSaga(),
    ProfileSaga(),
    ForgetSaga(),
    LayoutSaga(),
    UserSaga(),
    RoleSaga(),
    PermissionSaga(),
    ReportSaga(),
    AuditTrailSaga(),
    PropertiesSaga(),
    CaptchaSaga(),
    DashboardSaga(),
    CardSaga(),
    TerminalSaga(),
    TerminalCardSaga(),
    TerminalConfigurationSaga(),
    MasterTerminalConfigurationSaga(),
    MonitoringSaga(),
  ]);
}
