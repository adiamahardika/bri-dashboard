import React from "react";
import { Redirect } from "react-router-dom";
import routes from "../helpers/routes.json";
// Authentication related pages
import Login from "../pages/Authentication/Login";
import Logout from "../pages/Authentication/Logout";
import Register from "../pages/Authentication/Register";
import ForgetPwd from "../pages/Authentication/ForgetPassword";

// Dashboard
import Dashboard from "../pages/Dashboard/index";
import ReportGantiKartu from "../pages/Report/ReportGantiKartu/index";
import ReportAllTransaction from "../pages/Report/ReportAllTransaction/index";
import ReportPrintLast5Transaction from "../pages/Report/ReportPrintLast5Transaction/index";
import ReportPrintPassbook from "../pages/Report/ReportPrintPassbook/index";
import ReportPrintMonthlyStatement from "../pages/Report/ReportPrintMonthlyStatement/index";
import Users from "../pages/Settings/Users/index";
import AddUser from "../pages/Settings/Users/add_user";
import EditUser from "../pages/Settings/Users/edit_user";
import Role from "../pages/Settings/Role/index";
import AddRole from "../pages/Settings/Role/add_role";
import EditRole from "../pages/Settings/Role/edit_role";
import Profile from "../pages/Profile/index";
import AuditTrail from "../pages/AuditTrail/index";
import CardManagement from "../pages/MasterData/Card/index";
import AddCard from "../pages/MasterData/Card/add_card";
import EditCard from "../pages/MasterData/Card/edit_card";
import Terminal from "../pages/ProfilingMachines/Terminal/index";
import AddTerminal from "../pages/ProfilingMachines/Terminal/add_terminal";
import EditTerminal from "../pages/ProfilingMachines/Terminal/edit_terminal";
import EditTerminalCard from "../pages/ProfilingMachines/TerminalCard/edit_terminal_card";
import EditTerminalConfiguration from "../pages/ProfilingMachines/TerminalConfiguration/edit_terminal_configuration";
import MasterTerminalConfiguration from "../pages/MasterData/TerminalConfiguration/index";
import AddMasterTerminalConfiguration from "../pages/MasterData/TerminalConfiguration/add_master_terminal_config";
import EditMasterTerminalConfiguration from "../pages/MasterData/TerminalConfiguration/edit_master_terminal_config";
import Monitoring from "../pages/Monitoring/index";
import DetailMonitoring from "../pages/Monitoring/detail_monitoring";
const userRoutes = [
  { path: routes.dashboard, component: Dashboard },
  { path: routes.users, component: Users },
  { path: routes.add_user, component: AddUser },
  { path: routes.edit_user, component: EditUser },
  { path: routes.role, component: Role },
  { path: routes.add_role, component: AddRole },
  { path: routes.edit_role, component: EditRole },
  { path: routes.report_all_transaction, component: ReportAllTransaction },
  {
    path: routes.report_print_last_5_transaction,
    component: ReportPrintLast5Transaction,
  },
  {
    path: routes.report_print_passbook,
    component: ReportPrintPassbook,
  },
  {
    path: routes.report_monthly_statement,
    component: ReportPrintMonthlyStatement,
  },
  {
    path: routes.report_ganti_kartu,
    component: ReportGantiKartu,
  },
  { path: routes.profile, component: Profile },
  { path: routes.audit_trail, component: AuditTrail },
  { path: routes.card, component: CardManagement },
  { path: routes.add_card, component: AddCard },
  { path: routes.edit_card, component: EditCard },
  { path: routes.terminal, component: Terminal },
  { path: routes.add_terminal, component: AddTerminal },
  { path: routes.edit_terminal, component: EditTerminal },
  { path: routes.edit_terminal_card, component: EditTerminalCard },
  {
    path: routes.edit_terminal_configuration,
    component: EditTerminalConfiguration,
  },
  {
    path: routes.master_terminal_configuration,
    component: MasterTerminalConfiguration,
  },
  {
    path: routes.add_master_terminal_configuration,
    component: AddMasterTerminalConfiguration,
  },
  {
    path: routes.edit_master_terminal_configuration,
    component: EditMasterTerminalConfiguration,
  },
  {
    path: routes.monitoring,
    component: Monitoring,
  },
  {
    path: routes.detail_monitoring,
    component: DetailMonitoring,
  },

  // this route should be at the end of all other routes
  {
    path: "/",
    exact: true,
    component: () => <Redirect to={routes.dashboard} />,
  },
];

const authRoutes = [
  { path: routes.logout, component: Logout },
  { path: routes.login, component: Login },
  { path: "/forgot-password", component: ForgetPwd },
  { path: "/register", component: Register },
];

export { userRoutes, authRoutes };
