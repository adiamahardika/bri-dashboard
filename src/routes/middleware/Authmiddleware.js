import React from "react";
import { Redirect, Route, withRouter } from "react-router-dom";
import IdleTimerContainer from "../../components/Common/IdleTimeout";
import routes from "../../helpers/routes.json";

const Authmiddleware = ({ component: Component, layout: Layout }) => (
  <Route
    render={(props) => {
      if (!localStorage.getItem("isAuth")) {
        return (
          <Redirect
            to={{ pathname: routes.login, state: { from: props.location } }}
          />
        );
      }

      return (
        <Layout>
          <Component {...props} />
          <IdleTimerContainer />
        </Layout>
      );
    }}
  />
);

export default withRouter(Authmiddleware);
