import React, { useEffect, useState } from "react";

// MetisMenu
import MetisMenu from "metismenujs";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

//i18n
import { withNamespaces } from "react-i18next";
import code_all_permissions from "../../helpers/code_all_permissions.json";
import routes from "../../helpers/routes.json";
const SidebarContent = (props) => {
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const [isViewUsers, setIsViewUsers] = useState(true);
  const [isViewRoles, setIsViewRoles] = useState(true);
  const [isViewAuditTrail, setIsViewAuditTrail] = useState(true);

  useEffect(() => {
    let viewUsers = permissions.find(
      (value) => value.code === code_all_permissions.view_users
    );
    let viewRoles = permissions.find(
      (value) => value.code === code_all_permissions.view_role
    );
    let viewAuditTrail = permissions.find(
      (value) => value.code === code_all_permissions.view_audit_trail
    );

    viewUsers ? setIsViewUsers(true) : setIsViewUsers(false);
    viewRoles ? setIsViewRoles(true) : setIsViewRoles(false);
    viewAuditTrail ? setIsViewAuditTrail(true) : setIsViewAuditTrail(false);

    var pathName = props.location.pathname;
    const initMenu = async () => {
      new MetisMenu("#side-menu");
      var matchingMenuItem = null;
      var ul = document.getElementById("side-menu");
      var items = ul.getElementsByTagName("a");
      for (var i = 0; i < items.length; ++i) {
        if (pathName === items[i].pathname) {
          matchingMenuItem = items[i];
          break;
        }
      }
      if (matchingMenuItem) {
        activateParentDropdown(matchingMenuItem);
      }
    };
    initMenu();
  }, [props.location.pathname]);

  const activateParentDropdown = async (item) => {
    await item.classList.add("active");
    const parent = item.parentElement;

    if (parent) {
      await parent.classList.add("mm-active");
      const parent2 = parent.parentElement;

      if (parent2) {
        await parent2.classList.add("mm-show");

        const parent3 = parent2.parentElement;

        if (parent3) {
          await parent3.classList.add("mm-active"); // li
          await parent3.childNodes[0].classList.add("mm-active"); //a
          const parent4 = parent3.parentElement;
          if (parent4) {
            await parent4.classList.add("mm-active");
          }
        }
      }
      return false;
    }
    return false;
  };

  return (
    <React.Fragment>
      <div id="sidebar-menu">
        <ul
          className="metismenu list-unstyled"
          id="side-menu"
          style={{ paddingLeft: "8px", paddingRight: "8px" }}
        >
          <li className="menu-title">{props.t("Menu")} </li>
          <li>
            <Link to={routes.dashboard} className="waves-effect">
              <i className="bx bxs-tachometer"></i>
              <span>{props.t("Dashboards")}</span>
            </Link>
          </li>
          <li>
            <Link to={routes.monitoring} className="waves-effect">
              <i className="bx bx-desktop"></i>
              <span>{props.t("Monitoring")}</span>
            </Link>
          </li>
          <li>
            <Link to="/#" className="has-arrow waves-effect">
              <i className="bx bxs-file-blank"></i>
              <span>{props.t("Report")}</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to={routes.report_all_transaction}>
                  {props.t("All Transaction")}
                </Link>
              </li>
              <li>
                <Link to={routes.report_ganti_kartu}>
                  {props.t("Ganti Kartu")}
                </Link>
              </li>
              <li>
                <Link to={routes.report_print_last_5_transaction}>
                  {props.t("Print Last 5 Transaction")}
                </Link>
              </li>
              <li>
                <Link to={routes.report_monthly_statement}>
                  {props.t("Print Monthly Statement")}
                </Link>
              </li>
              <li>
                <Link to={routes.report_print_passbook}>
                  {props.t("Print Passbook")}
                </Link>
              </li>
            </ul>
          </li>
          {isViewAuditTrail && (
            <li>
              <Link to={routes.audit_trail} className="waves-effect">
                <i className="bx bx-list-check"></i>
                <span>{props.t("Audit Trail")}</span>
              </Link>
            </li>
          )}
          <li>
            <Link to={routes.terminal} className="waves-effect">
              <i className="bx bxs-terminal"></i>
              <span>{props.t("Terminal")}</span>
            </Link>
          </li>
          <li>
            <Link to="/#" className="has-arrow waves-effect">
              <i className="bx bxs-data"></i>
              <span>{props.t("Master Data")}</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to={routes.card}>{props.t("Card")}</Link>
              </li>
              <li>
                <Link to={routes.master_terminal_configuration}>
                  {props.t("Terminal Config")}
                </Link>
              </li>
            </ul>
          </li>
          <li>
            <Link to="/#" className="has-arrow waves-effect">
              <i className="bx bxs-cog"></i>
              <span>{props.t("Settings")}</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              {isViewUsers && (
                <li>
                  <Link to={routes.users}>{props.t("Users")}</Link>
                </li>
              )}
              {isViewRoles && (
                <li>
                  <Link to={routes.role}>{props.t("Role")}</Link>
                </li>
              )}
            </ul>
          </li>
        </ul>
      </div>
    </React.Fragment>
  );
};

export default withRouter(withNamespaces()(SidebarContent));
