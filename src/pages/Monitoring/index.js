import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  FormGroup,
  CardTitle,
  Table,
  Badge,
} from "reactstrap";
import { AvForm } from "availity-reactstrap-validation";
import {
  readRegional,
  readTerminalByRegional,
  readKanwil,
  readTerminalByKanwil,
  readKanca,
  readTerminalByKanca,
} from "../../store/pages/properties/actions";
import { readListMonitoring } from "../../store/pages/monitoring/actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { parseFullDate } from "../../helpers/index";
import { Link } from "react-router-dom";
import Breadcrumb from "../../components/Common/Breadcrumb";
import Select from "react-select";
import ReactPaginate from "react-paginate";
import routes from "../../helpers/routes.json";
import "../../assets/css/pagination.css";

const Monitoring = (props) => {
  const list_regional = props.list_regional;
  const list_kanwil = props.list_kanwil;
  const list_terminal = props.list_terminal;
  const list_kanca = props.list_kanca;
  const list_monitoring = props.list_monitoring;
  const total_pages_monitoring = props.total_pages_monitoring;
  const active_pages_monitoring = props.active_pages_monitoring;

  const [isShowTable, setIsShowTable] = useState(false);
  const [showModalMonitoring, setShowModalMonitoring] = useState(false);
  const [showPagination, setShowPagination] = useState(false);

  const [data, setData] = useState(null);
  const [selectedRegional, setSelectedRegional] = useState(null);
  const [selectedKanwil, setSelectedKanwil] = useState(null);
  const [selectedKanca, setSelectedKanca] = useState(null);
  const [selectedTerminalId, setSelectedTerminalId] = useState(null);

  const handleRegional = (selectedRegional) => {
    if (selectedRegional && selectedRegional.length > 0) {
      let id = [];
      selectedRegional.map((value) => id.push(value.value));
      props.readTerminalByRegional({ id });
      props.readKanwil({ id });
    }
    setSelectedRegional(selectedRegional);
  };
  const handleKanwil = (selectedKanwil) => {
    if (selectedKanwil && selectedKanwil.length > 0) {
      let id = [];
      selectedKanwil.map((value) => id.push(value.value));
      props.readTerminalByKanwil({ id });
      props.readKanca({ id });
    }
    setSelectedKanwil(selectedKanwil);
  };
  const handleKanca = (selectedKanca) => {
    if (selectedKanca && selectedKanca.length > 0) {
      let id = [];
      selectedKanca.map((value) => id.push(value.value));
      props.readTerminalByKanca({ id });
    }
    setSelectedKanca(selectedKanca);
  };
  const handleTerminalId = (selectedTerminalId) => {
    setSelectedTerminalId(selectedTerminalId);
  };
  const handleSearchByAreaCode = async () => {
    let listTerminalId = [];
    if (selectedTerminalId) {
      await selectedTerminalId.map((value) => listTerminalId.push(value.value));
    } else {
      await list_terminal.map((value) => listTerminalId.push(value.value));
    }
    setData({ ...data, listTerminalId: listTerminalId });
    props.readListMonitoring({ ...data, listTerminalId: listTerminalId });
    setIsShowTable(true);
    setShowPagination(true);
  };
  const handlePageClick = (value) => {
    props.readListMonitoring({ ...data, pageNo: value.selected });
    setData({ ...data, pageNo: value.selected });
  };

  useEffect(() => {
    props.readRegional();
    props.readTerminalByRegional({ id: [0] });
    setData({
      sortBy: "terminalId",
      sortType: "asc",
      listTerminalId: [],
      pageNo: 0,
      pageSize: 10,
    });
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumb title={"Monitoring"} breadcrumbItem={"Monitoring"} />
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <AvForm className="needs-validation">
                    <Row>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">Regional</label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleRegional(...args);
                            }}
                            options={list_regional}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">
                            Kantor Wilayah
                          </label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleKanwil(...args);
                            }}
                            options={list_kanwil}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">Kantor Cabang</label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleKanca(...args);
                            }}
                            options={list_kanca}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">Terminal Id</label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleTerminalId(...args);
                            }}
                            options={list_terminal}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="d-flex justify-content-end">
                        <button
                          type="button"
                          className="btn btn-primary waves-effect waves-light"
                          onClick={() => {
                            handleSearchByAreaCode();
                          }}
                        >
                          <i className="bx bx-search-alt-2 font-size-16 align-middle mr-2"></i>{" "}
                          Search
                        </button>
                      </Col>
                    </Row>
                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {isShowTable && (
            <Row>
              <Col className="col-12">
                <Card>
                  <CardBody>
                    <CardTitle>Monitoring</CardTitle>
                    {showPagination && (
                      <Row className="align-items-end mb-3">
                        <Col md="2">
                          <div className="form-group mb-0">
                            <label>Show Data</label>
                            <div>
                              <select
                                className="form-control"
                                defaultValue={10}
                                onChange={(event) => (
                                  setData({
                                    ...data,
                                    pageSize: parseInt(event.target.value),
                                    pageNo: 0,
                                  }),
                                  props.readListMonitoring({
                                    ...data,
                                    pageSize: parseInt(event.target.value),
                                    pageNo: 0,
                                  })
                                )}
                              >
                                {" "}
                                <option value={10}>10</option>
                                <option value={25}>25</option>
                                <option value={50}>50</option>
                                <option value={100}>100</option>
                              </select>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    )}
                    <div className="table-responsive">
                      <Table className="table table-centered">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Terminal Id</th>
                            <th scope="col">IP Cam</th>
                            <th scope="col">Monitor</th>
                            <th scope="col">Ektp Reader</th>
                            <th scope="col">Signpad</th>
                            <th scope="col">Finger Print</th>
                            <th scope="col">Edc</th>
                            <th scope="col">Card Destroyer</th>
                            <th scope="col">Card Dispenser</th>
                            <th scope="col">PC</th>
                            <th scope="col">Laser Printer</th>
                            <th scope="col">Passbook Printer</th>
                            <th scope="col">Thermal Printer</th>
                            <th scope="col">Last Update</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {list_monitoring &&
                            list_monitoring.map((value, index) => {
                              return (
                                <tr key={index}>
                                  <th scope="row">
                                    <div>{index + 1}</div>
                                  </th>
                                  <td>{value.terminalId}</td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.ipCam === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.ipCam === "1" ? "On" : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.monitor === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.monitor === "1" ? "On" : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.ektpReader === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.ektpReader === "1" ? "On" : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.signpad === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.signpad === "1" ? "On" : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.fingerPrint === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.fingerPrint === "1" ? "On" : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.edc === "1" ? "success" : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.edc === "1" ? "On" : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.cardDestroyer === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.cardDestroyer === "1"
                                        ? "On"
                                        : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.cardDispenser === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.cardDispenser === "1"
                                        ? "On"
                                        : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.pc === "1" ? "success" : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.pc === "1" ? "On" : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.laserPrinter === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.laserPrinter === "1"
                                        ? "On"
                                        : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.passbookPrinter === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.passbookPrinter === "1"
                                        ? "On"
                                        : "Off"}
                                    </Badge>
                                  </td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.thermalPrinter === "1"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.thermalPrinter === "1"
                                        ? "On"
                                        : "Off"}
                                    </Badge>
                                  </td>
                                  <td style={{ minWidth: "max-content" }}>
                                    {parseFullDate(value.updateDate)}
                                  </td>
                                  <td>
                                    <Link
                                      to={{
                                        pathname: routes.detail_monitoring,
                                        detailValue: value.terminalId,
                                      }}
                                    >
                                      <button
                                        type="button"
                                        className="btn btn-primary waves-effect waves-light"
                                        style={{ minWidth: "max-content" }}
                                      >
                                        <i className="bx bx-show font-size-16 align-middle"></i>
                                      </button>
                                    </Link>
                                  </td>
                                </tr>
                              );
                            })}
                        </tbody>
                      </Table>
                      {list_monitoring && list_monitoring.length <= 0 && (
                        <div style={{ textAlign: "center" }}>No Data</div>
                      )}
                    </div>
                    {showPagination &&
                      list_monitoring &&
                      list_monitoring.length > 0 && (
                        <Row className="d-flex align-items-end">
                          <ReactPaginate
                            previousLabel={"previous"}
                            nextLabel={"next"}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={total_pages_monitoring}
                            marginPagesDisplayed={1}
                            pageRangeDisplayed={5}
                            onPageChange={handlePageClick}
                            forcePage={active_pages_monitoring}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"}
                          />
                        </Row>
                      )}
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}
        </Container>
      </div>
    </React.Fragment>
  );
};
const mapStatetoProps = (state) => {
  const {
    list_regional,
    list_kanwil,
    list_kanca,
    list_terminal,
    message_kanwil,
    message_jenis_transaksi,
    message_kanca,
    message_regional,
    message_terminal,
    response_code_jenis_transaksi,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
    response_code_terminal,
  } = state.Properties;
  const {
    list_monitoring,
    message_monitoring,
    total_pages_monitoring,
    active_pages_monitoring,
  } = state.Monitoring;
  return {
    list_regional,
    list_kanwil,
    list_kanca,
    list_terminal,
    list_monitoring,
    message_monitoring,
    message_kanwil,
    message_jenis_transaksi,
    message_kanca,
    message_regional,
    message_terminal,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
    response_code_terminal,
    total_pages_monitoring,
    active_pages_monitoring,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readRegional,
      readTerminalByRegional,
      readKanwil,
      readTerminalByKanwil,
      readKanca,
      readTerminalByKanca,
      readListMonitoring,
    },
    dispatch
  );
export default connect(mapStatetoProps, mapDispatchToProps)(Monitoring);
