import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Table,
  Badge,
} from "reactstrap";
import { readDetailMonitoring } from "../../store/pages/monitoring/actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { parseFullDate } from "../../helpers/index";
import { useHistory } from "react-router";
import Breadcrumb from "../../components/Common/Breadcrumb";
import ReactPaginate from "react-paginate";
import routes from "../../helpers/routes.json";
import "../../assets/css/pagination.css";

const Monitoring = (props) => {
  const list_detail_monitoring = props.list_detail_monitoring
  const total_pages_monitoring = props.total_pages_monitoring;
  const active_pages_monitoring = props.active_pages_monitoring;
  const detailValue = props.location.detailValue;
  const history = useHistory();

  const [data, setData] = useState(null);

  const handlePageClick = (value) => {
    props.readDetailMonitoring({ ...data, pageNo: value.selected });
    setData({ ...data, pageNo: value.selected });
  };

  useEffect(() => {
    if (detailValue) {
      props.readDetailMonitoring({
        pageNo: 0,
        pageSize: 10,
        terminal_id: detailValue,
      });
      setData({
        pageNo: 0,
        pageSize: 10,
        terminal_id: detailValue,
      });
    } else {
      history.push(routes.monitoring);
    }
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumb
            title={"Monitoring"}
            breadcrumbItem={"Detail Monitoring"}
          />

          <Row>
            <Col className="col-12">
              <Card>
                <CardBody>
                  <CardTitle>Detail Monitoring</CardTitle>
                  <Row className="align-items-end mb-3">
                    <Col md="2">
                      <div className="form-group mb-0">
                        <label>Show Data</label>
                        <div>
                          <select
                            className="form-control"
                            defaultValue={10}
                            onChange={(event) => (
                              setData({
                                ...data,
                                pageSize: parseInt(event.target.value),
                                pageNo: 0,
                              }),
                              props.readDetailMonitoring({
                                ...data,
                                pageSize: parseInt(event.target.value),
                                pageNo: 0,
                              })
                            )}
                          >
                            {" "}
                            <option value={10}>10</option>
                            <option value={25}>25</option>
                            <option value={50}>50</option>
                            <option value={100}>100</option>
                          </select>
                        </div>
                      </div>
                    </Col>
                  </Row>
                  <div className="table-responsive">
                    <Table className="table table-centered">
                      <thead>
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Terminal Id</th>
                          <th scope="col">Hardware</th>
                          <th scope="col">Status</th>
                          <th scope="col">Create Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        {list_detail_monitoring &&
                          list_detail_monitoring.map((value, index) => {
                            return (
                              <tr key={index}>
                                <th scope="row">
                                  <div>{index + 1}</div>
                                </th>
                                <td>{value.terminalId}</td>
                                <td>{value.hardware}</td>
                                <td>
                                  <Badge
                                    color={`${
                                      value.status === "1"
                                        ? "success"
                                        : "danger"
                                    }`}
                                    className="p-2"
                                    style={{ fontSize: "0.8125rem" }}
                                  >
                                    {value.status === "1" ? "On" : "Off"}
                                  </Badge>
                                </td>
                                <td style={{ minWidth: "max-content" }}>
                                  {parseFullDate(value.createdDate)}
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </Table>
                    {list_detail_monitoring &&
                      list_detail_monitoring.length <= 0 && (
                        <div style={{ textAlign: "center" }}>No Data</div>
                      )}
                  </div>
                  {list_detail_monitoring && list_detail_monitoring.length > 0 && (
                    <Row className="d-flex align-items-end">
                      <ReactPaginate
                        previousLabel={"previous"}
                        nextLabel={"next"}
                        breakLabel={"..."}
                        breakClassName={"break-me"}
                        pageCount={total_pages_monitoring}
                        marginPagesDisplayed={1}
                        pageRangeDisplayed={5}
                        onPageChange={handlePageClick}
                        forcePage={active_pages_monitoring}
                        containerClassName={"pagination"}
                        subContainerClassName={"pages pagination"}
                        activeClassName={"active"}
                      />
                    </Row>
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};
const mapStatetoProps = (state) => {
  const {
    list_detail_monitoring,
    message_monitoring,
    response_code_monitoring,
    total_pages_monitoring,
    active_pages_monitoring,
  } = state.Monitoring;
  return {
    list_detail_monitoring,
    message_monitoring,
    response_code_monitoring,
    total_pages_monitoring,
    active_pages_monitoring,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readDetailMonitoring,
    },
    dispatch
  );
export default connect(mapStatetoProps, mapDispatchToProps)(Monitoring);
