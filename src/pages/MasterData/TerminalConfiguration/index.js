import React, { useState, useEffect } from "react";
import { Container, Card, CardBody, Modal, Table } from "reactstrap";
import {
  readMasterTerminalConfiguration,
  deleteMasterTerminalConfiguration,
} from "../../../store/pages/masterData/terminalConfiguration/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { parseFullDate } from "../../../helpers/index";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import ReactPaginate from "react-paginate";
import general_constant from "../../../helpers/general_constant.json";
import routes from "../../../helpers/routes.json";
import "../../../assets/css/pagination.css";

const MasterTerminalConfiguration = (props) => {
  const list_master_terminal_configuration =
    props.list_master_terminal_configuration;
  const message = props.message_master_terminal_configuration;
  const response_code = props.response_master_code_terminal_configuration;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();

  const [modalDelete, setModalDelete] = useState(false);

  const [selectedData, setSelectedData] = useState(null);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);

  const removeBodyCss = () => {
    document.body.classList.add("no_padding");
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (response_code === general_constant.success_response_code) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setSelectedData(null);
              history.push(routes.master_terminal_configuration);
            }}
          >
            The card has successfully deleted!
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message}
          </SweetAlert>
        );
      }
    }
    return value;
  };

  useEffect(() => {
    props.readMasterTerminalConfiguration();
  }, []);
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs
            title={"Master Data"}
            breadcrumbItem={"Terminal Configuration"}
          />
          <Card>
            <CardBody>
              <div
                className="col-md-12 mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-end",
                  gridTemplateColumns: "1fr",
                  columnGap: "8px",
                }}
              >
                {" "}
                <Link to={routes.add_master_terminal_configuration}>
                  <button
                    type="button"
                    className="btn btn-primary waves-effect waves-light"
                  >
                    <i className="bx bx-edit-alt font-size-16 align-middle mr-2"></i>{" "}
                    New
                  </button>
                </Link>
              </div>

              <div className="table-responsive">
                <Table className="table table-centered table-striped">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Section</th>
                      <th scope="col">Key</th>
                      <th scope="col">Value</th>
                      <th scope="col">Create At</th>
                      <th scope="col">Last Update</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {list_master_terminal_configuration &&
                      list_master_terminal_configuration.map((value, index) => {
                        return (
                          <tr key={value.id}>
                            <th scope="row">
                              <div>{index + 1}</div>
                            </th>
                            <td>{value.configSection}</td>
                            <td>{value.configKey}</td>
                            <td>{value.configValue}</td>
                            <td>{parseFullDate(value.createdDate)}</td>
                            <td>{parseFullDate(value.updateDate)}</td>
                            <td>
                              <div
                                style={{
                                  display: "grid",
                                  rowGap: "8px",
                                  gridAutoFlow: "column",
                                  gridAutoColumns: "max-content",
                                  columnGap: "4px",
                                }}
                              >
                                <Link
                                  to={{
                                    pathname:
                                      routes.edit_master_terminal_configuration,
                                    editValue: value,
                                  }}
                                >
                                  <button
                                    type="button"
                                    className="btn btn-primary waves-effect waves-light"
                                    style={{ minWidth: "max-content" }}
                                  >
                                    <i className="bx bx-edit font-size-16 align-middle"></i>
                                  </button>
                                </Link>
                                <button
                                  type="button"
                                  className="btn btn-danger waves-effect waves-light"
                                  style={{ minWidth: "max-content" }}
                                  onClick={() => {
                                    setSelectedData(value);
                                    setModalDelete(!modalDelete);
                                  }}
                                >
                                  <i className="bx bx-trash font-size-16 align-middle"></i>
                                </button>
                              </div>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                {!list_master_terminal_configuration && (
                  <div style={{ textAlign: "center" }}>No Data</div>
                )}
              </div>
            </CardBody>
          </Card>

          {/* Modal Delete */}
          <Modal
            isOpen={modalDelete}
            toggle={() => {
              setModalDelete(!modalDelete);
              removeBodyCss();
              setSelectedData(null);
            }}
          >
            <div className="modal-header">
              <h5 className="modal-title mt-0" id="myModalLabel">
                Delete Ms. Terminal Config
              </h5>
              <button
                type="button"
                onClick={() => {
                  setModalDelete(false);
                  setSelectedData(null);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              Are you sure want to delete this config?
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => {
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                  setSelectedData(null);
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-danger waves-effect waves-light"
                onClick={() => {
                  setIsShowSweetAlert(true);
                  props.deleteMasterTerminalConfiguration(selectedData.id);
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                }}
              >
                Delete
              </button>
            </div>
          </Modal>
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const {
    list_master_terminal_configuration,
    message_master_terminal_configuration,
    response_master_code_terminal_configuration,
  } = state.MasterTerminalConfiguration;
  return {
    list_master_terminal_configuration,
    message_master_terminal_configuration,
    response_master_code_terminal_configuration,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readMasterTerminalConfiguration,
      deleteMasterTerminalConfiguration,
    },
    dispatch
  );

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(MasterTerminalConfiguration);
