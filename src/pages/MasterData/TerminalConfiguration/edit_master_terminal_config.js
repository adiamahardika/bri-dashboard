import React, { useState, useEffect } from "react";
import { Container, Card, CardBody, Row, Col } from "reactstrap";
import { updateMasterTerminalConfiguration } from "../../../store/pages/masterData/terminalConfiguration/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { useHistory } from "react-router";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import general_constant from "../../../helpers/general_constant.json";
import UnsavedChangesWarning from "../../../helpers/unsaved_changes_warning";
import routes from "../../../helpers/routes.json";

const EditMasterTerminalConfig = (props) => {
  const message = props.message_master_terminal_configuration;
  const response_code = props.response_master_code_terminal_configuration;
  const editValue = props.location.editValue;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();
  const [Prompt, setDirty, setPristine] = UnsavedChangesWarning();

  const [data, setData] = useState(null);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);

  const onChangeData = (event) => {
    setData({
      ...data,
      [event.target.name]: event.target.value,
    });
    setDirty();
  };

  const onSubmitCreate = async () => {
    props.updateMasterTerminalConfiguration({
      ...data,
      createdDate: new Date(),
    });
    setIsShowSweetAlert(true);
    setPristine();
  };
  const ButtonSubmitCreate = () => {
    if (
      data &&
      Object.keys(data).length >= 3 &&
      Object.values(data).every((value) => value !== "")
    ) {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          onClick={() => {
            onSubmitCreate();
          }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          disabled
          style={{ cursor: "default" }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    }
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (response_code === general_constant.success_response_code) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setData(null);
              history.push(routes.master_terminal_configuration);
            }}
          >
            Master Terminal Config has successfully edited!
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message}
          </SweetAlert>
        );
      }
    }
    return value;
  };

  useEffect(() => {
    if (editValue) {
      setData(editValue);
    } else {
      history.push(routes.master_terminal_configuration);
    }
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs
            title={"Master Data"}
            breadcrumbItem={"Edit Terminal Config"}
          />
          <Card>
            <CardBody>
              <div
                className="col-md-12 mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-end",
                  gridTemplateColumns: "1fr",
                  columnGap: "8px",
                }}
              >
                {" "}
                <ButtonSubmitCreate />
              </div>
              <AvForm>
                <Row className="justify-content-center">
                  <Col md={6}>
                    <AvField
                      name="configSection"
                      label="Section"
                      placeholder="ex: TERMINAL"
                      type="text"
                      errorMessage="Enter Section"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 16 },
                      }}
                      value={data && data.configSection}
                      onChange={onChangeData}
                    />
                    <AvField
                      name="configKey"
                      label="Key"
                      placeholder="ex: terminal.id"
                      type="text"
                      errorMessage="Enter Key"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 16 },
                      }}
                      value={data && data.configKey}
                      onChange={onChangeData}
                    />
                    <AvField
                      name="configValue"
                      label="Value"
                      placeholder="ex: t001"
                      type="text"
                      errorMessage="Enter Value"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 16 },
                      }}
                      value={data && data.configValue}
                      onChange={onChangeData}
                    />
                  </Col>
                </Row>
              </AvForm>
            </CardBody>
          </Card>
          {Prompt}
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const {
    message_master_terminal_configuration,
    response_master_code_terminal_configuration,
  } = state.MasterTerminalConfiguration;
  return {
    message_master_terminal_configuration,
    response_master_code_terminal_configuration,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateMasterTerminalConfiguration,
    },
    dispatch
  );

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(EditMasterTerminalConfig);
