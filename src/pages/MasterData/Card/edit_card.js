import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  Form,
} from "reactstrap";
import {
  updateCard,
  readNamaProduk,
} from "../../../store/pages/masterData/card/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { useHistory } from "react-router";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import general_constant from "../../../helpers/general_constant.json";
import UnsavedChangesWarning from "../../../helpers/unsaved_changes_warning";
import Dropzone from "react-dropzone";
import routes from "../../../helpers/routes.json";

const EditCard = (props) => {
  const message = props.message_all_card;
  const response_code = props.response_code_all_card;
  const list_nama_produk = props.list_nama_produk;
  const cardValue = props.location.cardValue;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();
  const [Prompt, setDirty, setPristine] = UnsavedChangesWarning();

  const [data, setData] = useState(null);
  const [imageUrl, setImageUrl] = useState(null);
  const [imageData, setImageData] = useState(null);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);
  const [validNumber, setValidNumber] = useState(null);
  const [filterMessage, setFilterMessage] = useState(null);
  const [filterCode, setFilterCode] = useState(null);

  const onInsertImage = async (files) => {
    const image = files[0];
    const type = image.type;
    const reader = new FileReader();
    if (type === "image/png" || type === "image/jpg" || type === "image/jpeg") {
      reader.onload = () => {
        if (reader.readyState === 2) {
          setImageUrl(reader.result);
          setImageData(image);
        }
      };
      reader.readAsDataURL(image);
    } else {
      setIsShowSweetAlert(true);
      setFilterCode(general_constant.error_response_code);
      response_code = general_constant.error_response_code;
      setFilterMessage("Only .jpg, .png, .jpeg can be upload!");
    }
  };
  const onChangeData = (event) => {
    setData({
      ...data,
      [event.target.name]: event.target.value,
    });
    setDirty();
  };
  const onValidateNumber = (event) => {
    let regex = /^[0-9]+$/;
    setData({
      ...data,
      [event.target.name]: event.target.value,
    });
    setDirty();
    if (regex.test(event.target.value) === true) {
      setValidNumber({
        ...validNumber,
        [event.target.name]: true,
      });
    } else {
      setValidNumber({
        ...validNumber,
        [event.target.name]: false,
      });
    }
  };
  const onSubmitCreate = async () => {
    let image = imageUrl.split(",");
    let name = imageData.type.split("/");
    props.updateCard({
      ...data,
      picture: Math.random().toString(36).substring(7) + "." + name[1],
      url_picture: ``,
      base64: image[1],
    });
    setIsShowSweetAlert(true);
    setPristine();
  };
  const ButtonSubmitCreate = () => {
    if (
      data &&
      Object.keys(data).length >= 6 &&
      Object.values(data).every((value) => value !== "")
    ) {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          onClick={() => {
            onSubmitCreate();
          }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          disabled
          style={{ cursor: "default" }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    }
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (response_code === general_constant.success_response_code) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setData(null);
              history.push(routes.card);
            }}
          >
            Card has successfully edited!
          </SweetAlert>
        );
      } else if (filterCode === general_constant.error_response_code) {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setFilterMessage(null);
              setFilterCode(null);
              response_code = general_constant.error_response_code;
            }}
          >
            {filterMessage}
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message}
          </SweetAlert>
        );
      }
    }
    return value;
  };

  useEffect(() => {
    if (cardValue) {
      props.readNamaProduk();
      setData(cardValue);
      setImageUrl(
        cardValue && "data:image/jpeg;base64," + cardValue.url_picture
      );
      setImageData({ name: cardValue && cardValue.picture });
      setDirty();
    } else {
      history.push(routes.card);
    }
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title={"Master Data"} breadcrumbItem={"Edit Card"} />
          <Card>
            <CardBody>
              <div
                className="col-md-12 mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-end",
                  gridTemplateColumns: "1fr",
                  columnGap: "8px",
                }}
              >
                {" "}
                <ButtonSubmitCreate />
              </div>
              <AvForm>
                <Row>
                  <Col md={6}>
                    <FormGroup className="select2-container">
                      <label className="control-label">Nama Produk</label>
                      <div>
                        <select
                          name="namaProduk"
                          className="form-control"
                          defaultValue={data && data.namaProduk}
                          onChange={(event) => (
                            setData({
                              ...data,
                              namaProduk: event.target.value,
                            }),
                            setDirty()
                          )}
                        >
                          {list_nama_produk &&
                            list_nama_produk.map((value) => (
                              <option
                                key={value.id}
                                value={value.nama}
                                selected={
                                  data && data.namaProduk === value.nama
                                }
                                onChange={(event) => (
                                  setData({
                                    ...data,
                                    namaProduk: event.target.value,
                                  }),
                                  setDirty()
                                )}
                              >
                                {value.nama}
                              </option>
                            ))}
                        </select>
                      </div>
                    </FormGroup>
                    <AvField
                      name="jenisKartu"
                      label="Jenis Kartu"
                      placeholder="ex: BRITAMA"
                      type="text"
                      errorMessage="Enter Name"
                      value={data && data.jenisKartu}
                      validate={{
                        required: { value: true },
                      }}
                      onChange={onChangeData}
                    />
                    <AvField
                      name="biayaPenggantiKartu"
                      label="Biaya Penggantian Kartu"
                      placeholder="ex: 120000"
                      type="text"
                      errorMessage="Only number can be allowed"
                      value={data && data.biayaPenggantiKartu}
                      validate={{
                        required: { value: true },
                        pattern: { value: "^[0-9]+$" },
                      }}
                      onChange={onValidateNumber}
                    />
                    <AvField
                      name="maxTarikTunai"
                      label="Max. Tarik Tunai"
                      placeholder="ex: 120000"
                      type="text"
                      errorMessage="Only number can be allowed"
                      value={data && data.maxTarikTunai}
                      validate={{
                        required: { value: true },
                        pattern: { value: "^[0-9]+$" },
                      }}
                      onChange={onValidateNumber}
                    />
                    <AvField
                      name="maxTransferAntarBank"
                      label="Max. Transfer Antar Bank"
                      placeholder="ex: 120000"
                      type="text"
                      errorMessage="Only number can be allowed"
                      value={data && data.maxTransferAntarBank}
                      validate={{
                        required: { value: true },
                        pattern: { value: "^[0-9]+$" },
                      }}
                      onChange={onValidateNumber}
                    />
                    <AvField
                      name="maxTransferSesama"
                      label="Max. Transfer Sesama"
                      placeholder="ex: 120000"
                      type="text"
                      errorMessage="Only number can be allowed"
                      value={data && data.maxTransferSesama}
                      validate={{
                        required: { value: true },
                        pattern: { value: "^[0-9]+$" },
                      }}
                      onChange={onValidateNumber}
                    />
                  </Col>
                  <Col
                    md={6}
                    style={{ display: "grid", alignSelf: "flex-start" }}
                  >
                    <Form>
                      <Dropzone
                        onDrop={(acceptedFiles) => {
                          onInsertImage(acceptedFiles);
                        }}
                      >
                        {({ getRootProps, getInputProps }) => (
                          <div
                            className="dropzone"
                            style={{
                              borderStyle: "dashed",
                              borderWidth: "2px",
                              borderColor: "#556ee6",
                              borderRadius: "10px",
                              position: "relative",
                              minHeight: "250px",
                              display: "grid",
                              alignItems: "center",
                            }}
                          >
                            <div
                              className="dz-message needsclick mt-2"
                              {...getRootProps()}
                            >
                              <input
                                {...getInputProps()}
                                accept=".jpg, .png, .jpeg"
                                type="file"
                              />
                              {imageUrl === null ? (
                                <>
                                  <div className="mb-2">
                                    <i className="display-4 text-muted bx bxs-cloud-upload"></i>
                                  </div>
                                  <h4>Drop files here or click to upload.</h4>
                                </>
                              ) : (
                                <img
                                  src={imageUrl}
                                  style={{
                                    width: "100%",
                                    borderRadius: "10px",
                                  }}
                                  alt="HomeCard"
                                />
                              )}
                            </div>
                          </div>
                        )}
                      </Dropzone>
                    </Form>
                  </Col>
                </Row>
              </AvForm>
            </CardBody>
          </Card>
          {Prompt}
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const {
    message_all_card,
    loading,
    response_code_all_card,
    list_nama_produk,
  } = state.Card;
  return {
    list_nama_produk,
    list_nama_produk,
    response_code_all_card,
    message_all_card,
    loading,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateCard,
      readNamaProduk,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(EditCard);
