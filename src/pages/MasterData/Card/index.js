import React, { useState, useEffect } from "react";
import { Container, Card, CardBody, Modal, Table, Col, Row } from "reactstrap";
import {
  readAllCard,
  deleteCard,
} from "../../../store/pages/masterData/card/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { parseFullDate, parseToRupiah } from "../../../helpers/index";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import ReactPaginate from "react-paginate";
import general_constant from "../../../helpers/general_constant.json";
import routes from "../../../helpers/routes.json";
import "../../../assets/css/pagination.css";

const CardManagement = (props) => {
  const list_all_card = props.list_all_card;
  const message = props.message_all_card;
  const response_code = props.response_code_all_card;
  const total_pages_card = props.total_pages_card;
  const active_page_card = props.active_page_card;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();

  const [modalDelete, setModalDelete] = useState(false);

  const [data, setData] = useState({
    size: 10,
    page_no: 0,
    sort_by: "jenisKartu",
    sort_type: "asc",
  });
  const [selectedData, setSelectedData] = useState(null);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);

  const removeBodyCss = () => {
    document.body.classList.add("no_padding");
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (response_code === general_constant.success_response_code) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setSelectedData(null);
              history.push(routes.card);
            }}
          >
            The card has successfully deleted!
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message}
          </SweetAlert>
        );
      }
    }
    return value;
  };
  const handlePageClick = (value) => {
    props.readAllCard({ ...data, page_no: value.selected });
    setData({ ...data, page_no: value.selected });
  };

  useEffect(() => {
    props.readAllCard(data);
  }, []);
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs
            title={"Master Data"}
            breadcrumbItem={"Card"}
          />
          <Card>
            <CardBody>
              <Row className="mb-3 d-flex align-items-end">
                <Col md="10">
                  <Row className="d-flex align-items-end">
                    <Col md="2">
                      <div className="form-group mb-0">
                        <label>Show Data</label>
                        <div>
                          <select
                            className="form-control"
                            defaultValue={10}
                            onChange={(event) => (
                              setData({
                                ...data,
                                size: parseInt(event.target.value),
                                page_no: 0,
                              }),
                              props.readAllCard({
                                ...data,
                                size: parseInt(event.target.value),
                                page_no: 0,
                              })
                            )}
                          >
                            <option value={10}>10</option>
                            <option value={25}>25</option>
                            <option value={50}>50</option>
                            <option value={100}>100</option>
                          </select>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col md="2" className="d-flex justify-content-end">
                  <Link to={routes.add_card}>
                    <button
                      type="button"
                      className="btn btn-primary waves-effect waves-light"
                    >
                      <i className="bx bx-edit-alt font-size-16 align-middle mr-2"></i>{" "}
                      New
                    </button>
                  </Link>
                </Col>
              </Row>

              <div className="table-responsive">
                <Table className="table table-centered table-striped">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Image</th>
                      <th scope="col">Jenis Kartu</th>
                      <th scope="col">Name Produk</th>
                      <th scope="col">Max. Tarik Tunai</th>
                      <th scope="col">Max. Transfer Sesama</th>
                      <th scope="col">Max. Transfer Antar Bank</th>
                      <th scope="col">Biaya Penggantian Kartu</th>
                      <th scope="col">Create At</th>
                      <th scope="col">Last Update</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {list_all_card &&
                      list_all_card.map((value, index) => {
                        return (
                          <tr key={value.id}>
                            <th scope="row">
                              <div>{index + 1}</div>
                            </th>
                            <td>
                              {" "}
                              <img
                                src={`data:image/jpeg;base64,${value.url_picture}`}
                                alt={value.picture}
                                style={{ maxHeight: "75px" }}
                              />
                            </td>
                            <td>{value.namaProduk}</td>
                            <td>{value.jenisKartu}</td>
                            <td>{parseToRupiah(value.maxTarikTunai)}</td>
                            <td>{parseToRupiah(value.maxTransferSesama)}</td>
                            <td>{parseToRupiah(value.maxTransferAntarBank)}</td>
                            <td>{parseToRupiah(value.biayaPenggantiKartu)}</td>
                            <td>{parseFullDate(value.createdDate)}</td>
                            <td>{parseFullDate(value.updateDate)}</td>
                            <td>
                              <div
                                style={{
                                  display: "grid",
                                  gridAutoFlow: "column",
                                  columnGap: "4px",
                                }}
                              >
                                <Link
                                  to={{
                                    pathname: routes.edit_card,
                                    cardValue: value,
                                  }}
                                >
                                  <button
                                    type="button"
                                    className="btn btn-primary waves-effect waves-light"
                                    style={{ minWidth: "max-content" }}
                                  >
                                    <i className="bx bx-edit font-size-16 align-middle"></i>
                                  </button>
                                </Link>
                                <button
                                  type="button"
                                  className="btn btn-danger waves-effect waves-light"
                                  style={{ minWidth: "max-content" }}
                                  onClick={() => {
                                    setSelectedData(value);
                                    setModalDelete(!modalDelete);
                                  }}
                                >
                                  <i className="bx bx-trash font-size-16 align-middle"></i>
                                </button>
                              </div>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                {list_all_card && list_all_card.length <= 0 && (
                  <div style={{ textAlign: "center" }}>No Data</div>
                )}
              </div>
              <Row className="d-flex align-items-end">
                <ReactPaginate
                  previousLabel={"previous"}
                  nextLabel={"next"}
                  breakLabel={"..."}
                  breakClassName={"break-me"}
                  pageCount={total_pages_card}
                  marginPagesDisplayed={1}
                  pageRangeDisplayed={5}
                  forcePage={active_page_card}
                  onPageChange={handlePageClick}
                  containerClassName={"pagination"}
                  subContainerClassName={"pages pagination"}
                  activeClassName={"active"}
                />
              </Row>
            </CardBody>
          </Card>

          {/* Modal Delete */}
          <Modal
            isOpen={modalDelete}
            toggle={() => {
              setModalDelete(!modalDelete);
              removeBodyCss();
              setSelectedData(null);
            }}
          >
            <div className="modal-header">
              <h5 className="modal-title mt-0" id="myModalLabel">
                Delete Card
              </h5>
              <button
                type="button"
                onClick={() => {
                  setModalDelete(false);
                  setSelectedData(null);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              Are you sure want to delete this card?
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => {
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                  setSelectedData(null);
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-danger waves-effect waves-light"
                onClick={() => {
                  setIsShowSweetAlert(true);
                  props.deleteCard({ ...data, id: selectedData.id });
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                }}
              >
                Delete
              </button>
            </div>
          </Modal>
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const {
    list_all_card,
    message_all_card,
    response_code_all_card,
    loading,
    total_pages_card,
    active_page_card,
  } = state.Card;
  return {
    list_all_card,
    response_code_all_card,
    message_all_card,
    total_pages_card,
    active_page_card,
    loading,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readAllCard,
      deleteCard,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(CardManagement);
