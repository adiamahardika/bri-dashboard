import React, { useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Media,
} from "reactstrap";
import { parseDate } from "../../helpers";
import { readDashboard } from "../../store/pages/dashboard/actions";
import BarChart from "../AllCharts/chartjs/barchart";
import Breadcrumbs from "../../components/Common/Breadcrumb";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import routes from "../../helpers/routes.json";

const Dashboard = (props) => {
  const total_transaksi_gk = props.total_transaksi_gk;
  const total_transaksi_5_mutasi = props.total_transaksi_5_mutasi;
  const total_transaksi_passbook = props.total_transaksi_passbook;
  const total_transaksi_statement = props.total_transaksi_statement;
  const history = useHistory();

  useEffect(() => {
    if (localStorage.getItem("accessToken")) {
      let year = new Date().getFullYear();
      let month = "" + (new Date().getMonth() + 1);
      let date = "" + new Date().getDate();
      if (month.length < 2) month = "0" + month;
      if (date.length < 2) date = "0" + date;
      let today = date + "-" + month + "-" + year;
      props.readDashboard(today);
    } else {
      history.push(routes.login);
    }
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumbs title={"Dashboard"} breadcrumbItem={"Dashboard"} />

          <Row>
            <Col>
              <Card>
                <CardBody
                  style={{
                    borderBottomWidth: "3px",
                    borderBottomStyle: "solid",
                    borderBottomColor: "#556ee6",
                  }}
                >
                  <h4 className="mb-0">
                    Summary,
                    {" " + parseDate(new Date())}
                  </h4>
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Row>
            <Col>
              <Row>
                {/* Transaction Render */}
                <Col md="3">
                  <Card className="mini-stats-wid">
                    <CardBody>
                      <Media>
                        <Media body>
                          <p className="text-muted font-weight-medium">
                            Ganti Kartu
                          </p>
                          <h4 className="mb-0">
                            {total_transaksi_gk &&
                              total_transaksi_gk.totalTransaksi}
                          </h4>
                        </Media>
                        <div className="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                          <span className="avatar-title">
                            <i className="bx bx-credit-card-alt font-size-24"></i>
                          </span>
                        </div>
                      </Media>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="3">
                  <Card className="mini-stats-wid">
                    <CardBody>
                      <Media>
                        <Media body>
                          <p className="text-muted font-weight-medium">
                            Print Passbook
                          </p>
                          <h4 className="mb-0">
                            {total_transaksi_passbook &&
                              total_transaksi_passbook.totalTransaksi}
                          </h4>
                        </Media>
                        <div className="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                          <span className="avatar-title">
                            <i className="bx bxs-book-content font-size-24"></i>
                          </span>
                        </div>
                      </Media>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="3">
                  <Card className="mini-stats-wid">
                    <CardBody>
                      <Media>
                        <Media body>
                          <p className="text-muted font-weight-medium">
                            Print Monthly Statement
                          </p>
                          <h4 className="mb-0">
                            {total_transaksi_statement &&
                              total_transaksi_statement.totalTransaksi}
                          </h4>
                        </Media>
                        <div className="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                          <span className="avatar-title">
                            <i className="bx bxs-calendar font-size-24"></i>
                          </span>
                        </div>
                      </Media>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="3">
                  <Card className="mini-stats-wid">
                    <CardBody>
                      <Media>
                        <Media body>
                          <p className="text-muted font-weight-medium">
                            Print Last 5 Transaction
                          </p>
                          <h4 className="mb-0">
                            {total_transaksi_5_mutasi &&
                              total_transaksi_5_mutasi.totalTransaksi}
                          </h4>
                        </Media>
                        <div className="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                          <span className="avatar-title">
                            <i className="mdi mdi-numeric-5-circle-outline font-size-24"></i>
                          </span>
                        </div>
                      </Media>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col sm={6} md={6}>
              <Card>
                <CardBody>
                  <CardTitle>Ganti Kartu</CardTitle>
                  <BarChart transaction={total_transaksi_gk} />
                </CardBody>
              </Card>
            </Col>
            <Col sm={6} md={6}>
              <Card>
                <CardBody>
                  <CardTitle>Print Passbook</CardTitle>
                  <BarChart transaction={total_transaksi_passbook} />
                </CardBody>
              </Card>
            </Col>

            <Col sm={6} md={6}>
              <Card>
                <CardBody>
                  <CardTitle>Print Monthly Statement</CardTitle>
                  <BarChart transaction={total_transaksi_statement} />
                </CardBody>
              </Card>
            </Col>
            <Col sm={6} md={6}>
              <Card>
                <CardBody>
                  <CardTitle>Print Last 5 Transaction</CardTitle>
                  <BarChart transaction={total_transaksi_5_mutasi} />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const {
    total_transaksi_gk,
    total_transaksi_5_mutasi,
    total_transaksi_passbook,
    total_transaksi_statement,
    response_code_dashboard,
    message_dashboard,
    loading,
  } = state.Dashboard;
  return {
    total_transaksi_gk,
    total_transaksi_5_mutasi,
    total_transaksi_passbook,
    total_transaksi_statement,
    response_code_dashboard,
    message_dashboard,
    loading,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readDashboard,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(Dashboard);
