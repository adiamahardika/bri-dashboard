import React, { useEffect, useState } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  FormGroup,
  CardTitle,
  Table,
  Modal,
} from "reactstrap";
import { AvForm } from "availity-reactstrap-validation";
import {
  readRegional,
  readTerminalByRegional,
  readKanwil,
  readTerminalByKanwil,
  readKanca,
  readTerminalByKanca,
  readJenisTransaksi,
} from "../../store/pages/properties/actions";
import {
  readListAuditTrail,
  readDetailAuditTrail,
  readSearchAuditTrail,
} from "../../store/pages/auditTrail/actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { parseFullDate } from "../../helpers/index";
import Breadcrumb from "../../components/Common/Breadcrumb";
import Select from "react-select";
import ReactPaginate from "react-paginate";
import "../../assets/css/pagination.css";

const AuditTrail = (props) => {
  const list_regional = props.list_regional;
  const list_kanwil = props.list_kanwil;
  const list_terminal = props.list_terminal;
  const list_kanca = props.list_kanca;
  const list_audit_trail = props.list_audit_trail;
  const list_detail_audit_trail = props.list_detail_audit_trail;
  const message_audit_trail = props.message_audit_trail;
  const total_pages_audit_trail = props.total_pages_audit_trail;
  const active_pages_audit_trail = props.active_pages_audit_trail;

  const [today, setToday] = useState(null);
  const [isShowTable, setIsShowTable] = useState(false);
  const [filterBy, setFilterBy] = useState(null);
  const [showModalAuditTrail, setShowModalAuditTrail] = useState(false);
  const [showPagination, setShowPagination] = useState(false);

  const [data, setData] = useState(null);
  const [searchTrxId, setSearchTrxId] = useState(null);
  const [selectedRegional, setSelectedRegional] = useState(null);
  const [selectedKanwil, setSelectedKanwil] = useState(null);
  const [selectedKanca, setSelectedKanca] = useState(null);
  const [selectedTerminalId, setSelectedTerminalId] = useState(null);

  const removeBodyCss = () => {
    document.body.classList.add("no_padding");
  };
  const handleRegional = (selectedRegional) => {
    if (selectedRegional && selectedRegional.length > 0) {
      let id = [];
      selectedRegional.map((value) => id.push(value.value));
      props.readTerminalByRegional({ id });
      props.readKanwil({ id });
    }
    setSelectedRegional(selectedRegional);
  };
  const handleKanwil = (selectedKanwil) => {
    if (selectedKanwil && selectedKanwil.length > 0) {
      let id = [];
      selectedKanwil.map((value) => id.push(value.value));
      props.readTerminalByKanwil({ id });
      props.readKanca({ id });
    }
    setSelectedKanwil(selectedKanwil);
  };
  const handleKanca = (selectedKanca) => {
    if (selectedKanca && selectedKanca.length > 0) {
      let id = [];
      selectedKanca.map((value) => id.push(value.value));
      props.readTerminalByKanca({ id });
    }
    setSelectedKanca(selectedKanca);
  };
  const handleTerminalId = (selectedTerminalId) => {
    setSelectedTerminalId(selectedTerminalId);
  };
  const handleSearchByAreaCode = async () => {
    let listTerminalId = [];
    if (selectedTerminalId) {
      await selectedTerminalId.map((value) => listTerminalId.push(value.value));
    } else {
      await list_terminal.map((value) => listTerminalId.push(value.value));
    }
    setData({ ...data, listTerminalId: listTerminalId });
    props.readListAuditTrail({ ...data, listTerminalId: listTerminalId });
    setIsShowTable(true);
    setShowPagination(true);
  };
  const handleSearchByTrxId = async () => {
    props.readSearchAuditTrail(searchTrxId);
    setIsShowTable(true);
    setShowPagination(false);
  };
  const handlePageClick = (value) => {
    props.readListAuditTrail({ ...data, pageNo: value.selected });
    setData({ ...data, pageNo: value.selected });
  };

  useEffect(() => {
    let year = new Date().getFullYear();
    let month = "" + (new Date().getMonth() + 1);
    let date = "" + new Date().getDate();
    if (month.length < 2) month = "0" + month;
    if (date.length < 2) date = "0" + date;
    let today = year + "-" + month + "-" + date;
    props.readRegional();
    props.readJenisTransaksi();
    props.readTerminalByRegional({ id: [0] });
    setToday(today);
    setData({
      start: today,
      end: today,
      sortBy: "tgl_transaksi",
      sortType: "asc",
      status: "0",
      listTerminalId: [],
      pageNo: 0,
      pageSize: 10,
    });
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          {/* Render Breadcrumb */}
          <Breadcrumb title={"Audit Trail"} breadcrumbItem={"Audit Trail"} />
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <AvForm className="needs-validation">
                    <Row>
                      <Col md="4">
                        <div className="form-group">
                          <label>Fiter by</label>
                          <div>
                            <select
                              className="form-control"
                              defaultValue=""
                              onChange={(event) =>
                                setFilterBy(event.target.value)
                              }
                            >
                              <option value="" disabled>
                                Select
                              </option>
                              <option value="area">Area</option>
                              <option value="transaction_id">
                                Transaction Id
                              </option>
                            </select>
                          </div>
                        </div>
                      </Col>
                    </Row>
                    {filterBy && filterBy === "area" && (
                      <>
                        <Row>
                          <Col md="4">
                            <div className="form-group">
                              <label htmlFor="example-datetime-local-input">
                                Start Date
                              </label>
                              <input
                                className="form-control"
                                type="date"
                                id="example-date-input"
                                max={today}
                                defaultValue={today}
                                onChange={(event) =>
                                  setData({
                                    ...data,
                                    start: event.target.value,
                                  })
                                }
                              />
                            </div>
                          </Col>
                          <Col md="4">
                            <div className="form-group">
                              <label htmlFor="example-datetime-local-input">
                                End Date
                              </label>
                              <input
                                className="form-control"
                                type="date"
                                id="example-date-input"
                                max={today}
                                min={data && data.start}
                                defaultValue={today}
                                onChange={(event) =>
                                  setData({ ...data, end: event.target.value })
                                }
                              />
                            </div>
                          </Col>
                          <Col md="4">
                            <div className="form-group">
                              <label>Status Transaksi</label>
                              <div>
                                <select
                                  className="form-control"
                                  defaultValue="0"
                                  onChange={(event) =>
                                    setData({
                                      ...data,
                                      status: event.target.value,
                                    })
                                  }
                                >
                                  <option value="0">All</option>
                                  <option value="SUCCESS">Success</option>
                                  <option value="ERROR">Error</option>
                                </select>
                              </div>
                            </div>
                          </Col>
                        </Row>
                        <Row>
                          <Col md="3">
                            <FormGroup className="select2-container">
                              <label className="control-label">Regional</label>
                              <Select
                                isMulti={true}
                                placeholder="All"
                                onChange={(...args) => {
                                  handleRegional(...args);
                                }}
                                options={list_regional}
                              />
                            </FormGroup>
                          </Col>
                          <Col md="3">
                            <FormGroup className="select2-container">
                              <label className="control-label">
                                Kantor Wilayah
                              </label>
                              <Select
                                isMulti={true}
                                placeholder="All"
                                onChange={(...args) => {
                                  handleKanwil(...args);
                                }}
                                options={list_kanwil}
                              />
                            </FormGroup>
                          </Col>
                          <Col md="3">
                            <FormGroup className="select2-container">
                              <label className="control-label">
                                Kantor Cabang
                              </label>
                              <Select
                                isMulti={true}
                                placeholder="All"
                                onChange={(...args) => {
                                  handleKanca(...args);
                                }}
                                options={list_kanca}
                              />
                            </FormGroup>
                          </Col>
                          <Col md="3">
                            <FormGroup className="select2-container">
                              <label className="control-label">
                                Terminal Id
                              </label>
                              <Select
                                isMulti={true}
                                placeholder="All"
                                onChange={(...args) => {
                                  handleTerminalId(...args);
                                }}
                                options={list_terminal}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col className="d-flex justify-content-end">
                            <button
                              type="button"
                              className="btn btn-primary waves-effect waves-light"
                              onClick={() => {
                                handleSearchByAreaCode();
                              }}
                            >
                              <i className="bx bx-search-alt-2 font-size-16 align-middle mr-2"></i>{" "}
                              Search
                            </button>
                          </Col>
                        </Row>
                      </>
                    )}
                    {filterBy && filterBy === "transaction_id" && (
                      <Row className="align-items-end">
                        <Col sm="4">
                          <div className="form-group mb-0">
                            <label
                              htmlFor="example-search-input"
                              className="col-form-label"
                            >
                              Transaction Id
                            </label>
                            <div>
                              <input
                                className="form-control"
                                type="search"
                                placeholder="Search Transaction Id"
                                onChange={(event) =>
                                  setSearchTrxId(event.target.value)
                                }
                              />
                            </div>
                          </div>
                        </Col>
                        <Col className="d-flex" sm="2">
                          <button
                            type="button"
                            className="btn btn-primary waves-effect waves-light"
                            onClick={() => {
                              handleSearchByTrxId();
                            }}
                          >
                            <i className="bx bx-search-alt-2 font-size-16 align-middle mr-2"></i>{" "}
                            Search
                          </button>
                        </Col>
                      </Row>
                    )}
                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {isShowTable && (
            <Row>
              <Col className="col-12">
                <Card>
                  <CardBody>
                    <CardTitle>Audit Trail</CardTitle>
                    {showPagination && (
                      <Row className="align-items-end mb-3">
                        <Col md="2">
                          <div className="form-group mb-0">
                            <label>Show Data</label>
                            <div>
                              <select
                                className="form-control"
                                defaultValue={10}
                                onChange={(event) => (
                                  setData({
                                    ...data,
                                    pageSize: parseInt(event.target.value),
                                    pageNo: 0,
                                  }),
                                  props.readListAuditTrail({
                                    ...data,
                                    pageSize: parseInt(event.target.value),
                                    pageNo: 0,
                                  })
                                )}
                              >
                                {" "}
                                <option value={10}>10</option>
                                <option value={25}>25</option>
                                <option value={50}>50</option>
                                <option value={100}>100</option>
                              </select>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    )}
                    <div className="table-responsive">
                      <Table className="table table-centered">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Transaction Id</th>
                            <th scope="col">Terminal Id</th>
                            <th scope="col">Step</th>
                            <th scope="col">Action</th>
                            <th scope="col">Result</th>
                            <th scope="col">Data</th>
                            <th scope="col">Tgl Transaksi</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {list_audit_trail &&
                            list_audit_trail.map((value, index) => {
                              return (
                                <tr key={index}>
                                  <th scope="row">
                                    <div>{index + 1}</div>
                                  </th>
                                  <td>{value.trxId}</td>
                                  <td>{value.createdBy}</td>
                                  <td>{value.step}</td>
                                  <td>{value.action}</td>
                                  <td>{value.result}</td>
                                  <td>{value.data}</td>
                                  <td style={{ minWidth: "max-content" }}>
                                    {parseFullDate(value.createdDate)}
                                  </td>
                                  <td>
                                    <button
                                      type="button"
                                      className="btn btn-primary waves-effect waves-light"
                                      style={{ minWidth: "max-content" }}
                                      onClick={() => (
                                        props.readDetailAuditTrail(value.trxId),
                                        setShowModalAuditTrail(
                                          !showModalAuditTrail
                                        )
                                      )}
                                    >
                                      <i className="bx bx-show font-size-16 align-middle"></i>
                                    </button>
                                  </td>
                                </tr>
                              );
                            })}
                        </tbody>
                      </Table>
                      {list_audit_trail && list_audit_trail.length <= 0 && (
                        <div style={{ textAlign: "center" }}>No Data</div>
                      )}
                    </div>
                    {showPagination &&
                      list_audit_trail &&
                      list_audit_trail.length > 0 && (
                        <Row className="d-flex align-items-end">
                          <ReactPaginate
                            previousLabel={"previous"}
                            nextLabel={"next"}
                            breakLabel={"..."}
                            breakClassName={"break-me"}
                            pageCount={total_pages_audit_trail}
                            marginPagesDisplayed={1}
                            pageRangeDisplayed={5}
                            onPageChange={handlePageClick}
                            forcePage={active_pages_audit_trail}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"}
                          />
                        </Row>
                      )}
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}

          {/* Modal Audit Trail */}
          <Modal
            isOpen={showModalAuditTrail}
            toggle={() => {
              setShowModalAuditTrail(!showModalAuditTrail);
              removeBodyCss();
            }}
            scrollable={true}
            size="xl"
          >
            <div className="modal-header">
              <h5 className="modal-title mt-0" id="myModalLabel">
                Detail Audit Trail
              </h5>
              <button
                type="button"
                onClick={() => {
                  setShowModalAuditTrail(false);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="table-responsive">
                <Table className="table table-centered">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Transaction Id</th>
                      <th scope="col">Terminal Id</th>
                      <th scope="col">Step</th>
                      <th scope="col">Action</th>
                      <th scope="col">Result</th>
                      <th scope="col">Data</th>
                      <th scope="col">Tgl Transaksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    {list_detail_audit_trail &&
                      list_detail_audit_trail.map((value, index) => {
                        return (
                          <tr key={index}>
                            <th scope="row">
                              <div>{index + 1}</div>
                            </th>
                            <td>{value.trxId}</td>
                            <td>{value.createdBy}</td>
                            <td>{value.step}</td>
                            <td>{value.action}</td>
                            <td>{value.result}</td>
                            <td>{value.data}</td>
                            <td style={{ minWidth: "max-content" }}>
                              {parseFullDate(value.createdDate)}
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                {list_detail_audit_trail &&
                  list_detail_audit_trail.length <= 0 && (
                    <div style={{ textAlign: "center" }}>No Data</div>
                  )}
                {!list_detail_audit_trail && (
                  <div style={{ textAlign: "center" }}>
                    {message_audit_trail}
                  </div>
                )}
              </div>
            </div>
          </Modal>
        </Container>
      </div>
    </React.Fragment>
  );
};
const mapStatetoProps = (state) => {
  const {
    list_jenis_transaksi,
    list_regional,
    list_kanwil,
    list_kanca,
    list_terminal,
    message_kanwil,
    message_jenis_transaksi,
    message_kanca,
    message_regional,
    message_terminal,
    response_code_jenis_transaksi,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
    response_code_terminal,
  } = state.Properties;
  const {
    list_audit_trail,
    list_detail_audit_trail,
    message_audit_trail,
    total_pages_audit_trail,
    active_pages_audit_trail,
  } = state.AuditTrail;
  return {
    list_jenis_transaksi,
    list_regional,
    list_kanwil,
    list_kanca,
    list_terminal,
    list_audit_trail,
    list_detail_audit_trail,
    message_audit_trail,
    message_kanwil,
    message_jenis_transaksi,
    message_kanca,
    message_regional,
    message_terminal,
    response_code_jenis_transaksi,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
    response_code_terminal,
    total_pages_audit_trail,
    active_pages_audit_trail,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readRegional,
      readTerminalByRegional,
      readKanwil,
      readTerminalByKanwil,
      readKanca,
      readTerminalByKanca,
      readListAuditTrail,
      readJenisTransaksi,
      readDetailAuditTrail,
      readSearchAuditTrail,
    },
    dispatch
  );
export default connect(mapStatetoProps, mapDispatchToProps)(AuditTrail);
