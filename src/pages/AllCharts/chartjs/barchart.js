import React from "react";
import { Bar } from "react-chartjs-2";
import { parseDate } from "../../../helpers/index";
const BarChart = (props) => {
  const data = {
    labels: [parseDate(new Date())],
    datasets: [
      {
        label: "Success",
        backgroundColor: "#34c38f",
        borderColor: "#34c38f",
        borderWidth: 1,
        hoverBackgroundColor: "#34c38f",
        hoverBorderColor: "#34c38f",
        data: props.transaction && [props.transaction.totalSukses],
      },
      {
        label: "Failed",
        backgroundColor: "#f46a6a",
        borderColor: "#f46a6a",
        borderWidth: 1,
        hoverBackgroundColor: "#f46a6a",
        hoverBorderColor: "#f46a6a",
        data: props.transaction && [props.transaction.totalGagal],
      },
    ],
  };

  const option = {
    scales: {
      xAxes: [
        {
          barPercentage: 1,
        },
      ],
      yAxes: [
        {
          ticks: {
            max: props.transaction && props.transaction.totalTransaksi,
            min: 0,
          },
        },
      ],
    },
  };

  return <Bar width={474} height={300} data={data} options={option} />;
};

export default BarChart;
