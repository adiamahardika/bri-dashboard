import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  CardTitle,
  Button,
} from "reactstrap";
import { readAllCard } from "../../../store/pages/masterData/card/actions";
import {
  readRegional,
  readKanwil,
  readKanca,
} from "../../../store/pages/properties/actions";
import { createTerminalCard } from "../../../store/pages/profilingMachines/terminalCard/actions";
import { createTerminal } from "../../../store/pages/profilingMachines/terminal/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { useHistory } from "react-router";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import general_constant from "../../../helpers/general_constant.json";
import UnsavedChangesWarning from "../../../helpers/unsaved_changes_warning";
import Switch from "react-switch";
import routes from "../../../helpers/routes.json";

const AddTerminal = (props) => {
  const message_terminal_card = props.message_terminal_card;
  const message_all_terminal = props.message_all_terminal;
  const response_code_terminal_card = props.response_code_terminal_card;
  const response_code_all_terminal = props.response_code_all_terminal;
  const list_all_regional = props.list_all_regional;
  const list_all_kanwil = props.list_all_kanwil;
  const list_all_kanca = props.list_all_kanca;
  const list_all_card = props.list_all_card;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();
  const [Prompt, setDirty, setPristine] = UnsavedChangesWarning();

  const [terminalCardData, setTerminalCardData] = useState(null);
  const [terminalProfileData, setTerminalProfileData] = useState(null);

  const [validEmail, setValidEmail] = useState(false);
  const [validIpAddress, setValidIpAddress] = useState(false);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);
  const [showJenisKartu, setShowJenisKartu] = useState(false);
  const [isActive, setIsActive] = useState(true);
  const [validPassword, setValidPassword] = useState(false);

  const onChangeTerminalProfileData = (event) => {
    setTerminalProfileData({
      ...terminalProfileData,
      [event.target.name]: event.target.value,
    });
    setDirty();
  };
  const onValidateEmail = (email) => {
    let regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    setTerminalProfileData({
      ...terminalProfileData,
      email: email,
    });
    setDirty();
    if (regex.test(email) === true) {
      setValidEmail(true);
    } else {
      setValidEmail(false);
    }
  };
  const onvalidateIpAddress = (ip) => {
    let regex =
      /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    setTerminalProfileData({
      ...terminalProfileData,
      ip: ip,
    });
    setDirty();
    if (regex.test(ip) === true) {
      setValidIpAddress(true);
    } else {
      setValidIpAddress(false);
    }
  };
  const onValidatePassword = (value) => {
    setTerminalProfileData({
      ...terminalProfileData,
      password: value,
    });
    setDirty();
    if (value.length >= 8 && value.length <= 12) {
      setValidPassword(true);
    } else {
      setValidPassword(false);
    }
  };
  const onSubmitCreate = async () => {
    let data = [];
    await terminalCardData.map((value) => {
      data.push({ ...value, terminalId: terminalProfileData.terminalId });
    });

    props.createTerminalCard({ listCard: data });
    props.createTerminal({ ...terminalProfileData, updateDate: new Date() });
    setIsShowSweetAlert(true);
    setPristine();
  };
  const handleIsActive = () => {
    setTerminalProfileData({
      ...terminalProfileData,
      status: !isActive === true ? "On" : "Off",
    });
    setIsActive(!isActive);
    setDirty();
  };
  const handleTerminalCard = (value, index) => {
    let data = [...terminalCardData];

    data[index] = {
      idMsCard: parseInt(value),
    };

    setTerminalCardData(data);
    setDirty();
  };
  const handleAddJenisKartu = () => {
    let data = showJenisKartu.length + 1;
    let data2 = [];
    let newArray = [...showJenisKartu];

    if (terminalCardData && terminalCardData.length > 0) {
      data2 = [...terminalCardData];
    }

    newArray.push(data);
    data2.push({
      idMsCard: list_all_card[0].id,
    });

    setShowJenisKartu(newArray);
    setTerminalCardData(data2);
    setDirty();
  };
  const handleRemoveJenisKartu = (index) => {
    let newArray = [...showJenisKartu];
    let data2 = [...terminalCardData];

    newArray.splice(index, 1);
    data2.splice(index, 1);

    setShowJenisKartu(newArray);
    setTerminalCardData(data2);
    setShowJenisKartu(newArray);
    setDirty();
  };

  const ButtonSubmitCreate = () => {
    if (
      terminalCardData &&
      terminalCardData.length >= 1 &&
      terminalProfileData &&
      Object.keys(terminalProfileData).length >= 17 &&
      Object.values(terminalProfileData).every((value) => value !== "") &&
      validEmail &&
      validIpAddress &&
      validPassword
    ) {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          onClick={() => {
            onSubmitCreate();
          }}
        >
          <i className="bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          disabled
          style={{ cursor: "default" }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    }
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (
        response_code_terminal_card ===
          general_constant.success_response_code &&
        response_code_all_terminal === general_constant.success_response_code
      ) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setTerminalCardData(null);
              history.push(routes.terminal);
            }}
          >
            Terminal has successfully created!
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {response_code_terminal_card ===
            general_constant.error_response_code
              ? message_terminal_card
              : message_all_terminal}
          </SweetAlert>
        );
      }
    }
    return value;
  };
  const Offsymbol = (props) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 12,
          color: "#fff",
          paddingRight: 2,
        }}
      >
        {" "}
        Off
      </div>
    );
  };

  const OnSymbol = (props) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 12,
          color: "#fff",
          paddingRight: 2,
        }}
      >
        {" "}
        On
      </div>
    );
  };

  useEffect(() => {
    props.readAllCard({
      size: 1000,
      page_no: 0,
      sort_by: "jenisKartu",
      sort_type: "asc",
    });
    props.readRegional();
    setTerminalProfileData({
      status: "On",
    });
    setShowJenisKartu([]);
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title={"Terminal"} breadcrumbItem={"Add Terminal"} />
          <Card>
            <CardBody>
              <div
                className="mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-start",
                  alignItems: "center",
                  gridTemplateColumns: "1fr repeat(2, max-content) ",
                  columnGap: "2rem",
                }}
              >
                <CardTitle>Add Terminal Profile</CardTitle>{" "}
                <FormGroup className="select2-container mb-0 align-content-center">
                  <label className="control-label mr-2">Status :</label>
                  <Switch
                    uncheckedIcon={<Offsymbol />}
                    checkedIcon={<OnSymbol />}
                    onColor="#626ed4"
                    checked={isActive}
                    onChange={() => handleIsActive()}
                  />
                </FormGroup>{" "}
                <ButtonSubmitCreate />
              </div>
              <AvForm>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="terminalId"
                      label="Terminal Id"
                      type="text"
                      placeholder="ex: t001"
                      errorMessage="Enter Terminal Id"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 10 },
                      }}
                      onChange={(event) => (
                        setTerminalProfileData({
                          ...terminalProfileData,
                          terminalId: event.target.value,
                        }),
                        setDirty()
                      )}
                    />
                  </Col>
                  <Col md={6}>
                    <AvField
                      name="terminalIdBri"
                      label="Terminal Id BRI"
                      type="text"
                      errorMessage="Enter Terminal Id BRI"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="email"
                      label="Email"
                      type="email"
                      placeholder="ex: user@mail.com"
                      errorMessage="Enter Valid Email"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={(event) => onValidateEmail(event.target.value)}
                    />
                  </Col>
                  <Col md={6}>
                    <AvField
                      name="password"
                      label="Password"
                      type="password"
                      errorMessage="Password must be 8 - 12 character"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 12 },
                        minLength: { value: 8 },
                      }}
                      onChange={(event) =>
                        onValidatePassword(event.target.value)
                      }
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <AvField
                      name="sw_version"
                      label="Software Version"
                      type="text"
                      errorMessage="Enter SW Version"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={4}>
                    <AvField
                      name="ip"
                      label="IP Address"
                      type="text"
                      placeholder="ex: 10.10.10.10"
                      errorMessage="Enter Valid IP Address"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                        pattern: {
                          value:
                            "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$",
                        },
                      }}
                      onChange={(event) =>
                        onvalidateIpAddress(event.target.value)
                      }
                    />
                  </Col>
                  <Col md={4}>
                    <AvField
                      name="nomorRangka"
                      label="Nomor Rangka"
                      type="text"
                      errorMessage="Enter Nomor Rangka"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="tellerId"
                      label="Teller Id"
                      type="text"
                      errorMessage="Enter Teller Id"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={6}>
                    {" "}
                    <AvField
                      name="tellerIdButab"
                      label="Teller Id Butab"
                      type="text"
                      errorMessage="Enter Teller Id Butab"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="spv"
                      label="Supervisor"
                      type="text"
                      errorMessage="Enter Supervisor"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={6}>
                    <AvField
                      name="kodeSpv"
                      label="Kode Supervisor"
                      type="text"
                      errorMessage="Enter Kode Supervisor"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <FormGroup className="select2-container">
                      <label className="control-label">Regional</label>
                      <div>
                        <select
                          name="regional"
                          className="form-control"
                          defaultValue="-"
                          onChange={(event) => (
                            setTerminalProfileData({
                              ...terminalProfileData,
                              regional: { id: parseInt(event.target.value) },
                            }),
                            props.readKanwil({
                              id: [parseInt(event.target.value)],
                            }),
                            setDirty()
                          )}
                        >
                          <option value="-" disabled>
                            Select Regional
                          </option>
                          {list_all_regional &&
                            list_all_regional.map((value) => (
                              <option
                                key={value.id}
                                value={value.id}
                                onChange={(event) => (
                                  setTerminalProfileData({
                                    ...terminalProfileData,
                                    regional: {
                                      id: parseInt(event.target.value),
                                    },
                                  }),
                                  props.readKanwil({
                                    id: [parseInt(event.target.value)],
                                  }),
                                  setDirty()
                                )}
                              >
                                {value.name}
                              </option>
                            ))}
                        </select>
                      </div>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup className="select2-container">
                      <label className="control-label">Kantor Wilayah</label>
                      <div>
                        <select
                          name="kanwil"
                          className="form-control"
                          defaultValue="-"
                          onChange={(event) => (
                            setTerminalProfileData({
                              ...terminalProfileData,
                              kanwil: { id: parseInt(event.target.value) },
                            }),
                            props.readKanca({
                              id: [parseInt(event.target.value)],
                            }),
                            setDirty()
                          )}
                        >
                          <option value="-" disabled>
                            Select Kantor Wilayah
                          </option>
                          {list_all_kanwil &&
                            list_all_kanwil.map((value) => (
                              <option
                                key={value.id}
                                value={value.id}
                                onChange={(event) => (
                                  setTerminalProfileData({
                                    ...terminalProfileData,
                                    kanwil: {
                                      id: parseInt(event.target.value),
                                    },
                                  }),
                                  props.readKanca({
                                    id: [parseInt(event.target.value)],
                                  }),
                                  setDirty()
                                )}
                              >
                                {value.name}
                              </option>
                            ))}
                        </select>
                      </div>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup className="select2-container">
                      <label className="control-label">Kantor Cabang</label>
                      <div>
                        <select
                          name="kanca"
                          className="form-control"
                          defaultValue="-"
                          onChange={(event) => (
                            setTerminalProfileData({
                              ...terminalProfileData,
                              kanca: { id: parseInt(event.target.value) },
                            }),
                            setDirty()
                          )}
                        >
                          <option value="-" disabled>
                            Select Kantor Cabang
                          </option>
                          {list_all_kanca &&
                            list_all_kanca.map((value) => (
                              <option
                                key={value.id}
                                value={value.id}
                                onChange={(event) => (
                                  setTerminalProfileData({
                                    ...terminalProfileData,
                                    kanca: { id: parseInt(event.target.value) },
                                  }),
                                  setDirty()
                                )}
                              >
                                {value.name}
                              </option>
                            ))}
                        </select>
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="unitKerja"
                      label="Unit Kerja"
                      type="text"
                      errorMessage="Enter Unit Kerja"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={6}>
                    <AvField
                      name="alamatUnitKerja"
                      label="Alamat Unit Kerja"
                      type="textarea"
                      errorMessage="Enter Alamat Unit Kerja"
                      validate={{
                        required: { value: true },
                      }}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
              </AvForm>
            </CardBody>
            <CardBody>
              <div
                className="mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-end",
                  alignItems: "center",
                  gridTemplateColumns: "max-content",
                  columnGap: "8px",
                }}
              >
                <CardTitle>Terminal Card</CardTitle>
              </div>
              <AvForm>
                {showJenisKartu &&
                  showJenisKartu.map((value, index) => (
                    <Row key={index}>
                      <Col md={4}>
                        <FormGroup className="select2-container">
                          <label className="control-label">
                            Jenis Kartu {index + 1}
                          </label>
                          <div>
                            <select
                              className="form-control"
                              defaultValue={
                                terminalCardData &&
                                terminalCardData[index].idMsCard
                              }
                              onChange={(event) =>
                                handleTerminalCard(event.target.value, index)
                              }
                            >
                              {list_all_card &&
                                list_all_card.map(
                                  (valueAllCard, indexAllCard) => (
                                    <option
                                      key={indexAllCard}
                                      value={valueAllCard.id}
                                      selected={
                                        terminalCardData &&
                                        terminalCardData[index].idMsCard ===
                                          valueAllCard.id
                                      }
                                      onChange={(event) =>
                                        handleTerminalCard(
                                          event.target.value,
                                          index
                                        )
                                      }
                                    >
                                      {valueAllCard.jenisKartu}
                                    </option>
                                  )
                                )}
                            </select>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col
                        md={1}
                        style={{ display: "grid", alignItems: "flex-end" }}
                        className="m-0"
                      >
                        <Button
                          color="light"
                          className="btn btn-light waves-effect mb-3"
                          onClick={() => {
                            handleRemoveJenisKartu(index);
                          }}
                        >
                          <i className="bx bxs-x-circle font-size-16 align-middle"></i>
                        </Button>
                      </Col>
                    </Row>
                  ))}
                {showJenisKartu && showJenisKartu.length < 8 && (
                  <Row>
                    <Col
                      md={4}
                      style={{ display: "grid", justifyItems: "flex-end" }}
                    >
                      <button
                        type="button"
                        color="link"
                        className="btn btn-link waves-effect"
                        onClick={() => {
                          handleAddJenisKartu();
                        }}
                      >
                        + Tambah Jenis kartu
                      </button>
                    </Col>
                  </Row>
                )}
              </AvForm>
            </CardBody>
          </Card>
          {Prompt}
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const { list_all_card, message_all_card, response_code_all_card, loading } =
    state.Card;
  const { response_code_terminal_card, message_terminal_card } =
    state.TerminalCard;
  const { response_code_all_terminal, message_all_terminal } = state.Terminal;
  const {
    list_all_regional,
    list_all_kanwil,
    list_all_kanca,
    message_kanwil,
    message_kanca,
    message_regional,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
  } = state.Properties;
  return {
    list_all_card,
    list_all_regional,
    list_all_kanwil,
    list_all_kanca,
    message_all_card,
    message_terminal_card,
    message_kanwil,
    message_kanca,
    message_regional,
    message_all_terminal,
    response_code_terminal_card,
    response_code_all_terminal,
    response_code_all_card,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
    loading,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readAllCard,
      createTerminalCard,
      createTerminal,
      readRegional,
      readKanwil,
      readKanca,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(AddTerminal);
