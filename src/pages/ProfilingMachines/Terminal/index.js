import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  CardBody,
  FormGroup,
  Modal,
  Table,
  Col,
  Row,
  Badge,
  Button,
} from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import {
  readAllTerminal,
  deleteTerminal,
  changePasswordTerminal,
} from "../../../store/pages/profilingMachines/terminal/actions";
import { readDetailTerminalCard } from "../../../store/pages/profilingMachines/terminalCard/actions";
import {
  readRegional,
  readTerminalByRegional,
  readKanwil,
  readTerminalByKanwil,
  readKanca,
  readTerminalByKanca,
  readJenisTransaksi,
} from "../../../store/pages/properties/actions";
import { readMasterTerminalConfiguration } from "../../../store/pages/masterData/terminalConfiguration/actions";
import { readDetailTerminalConfiguration } from "../../../store/pages/profilingMachines/terminalConfiguration/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { parseFullDate, parseToRupiah } from "../../../helpers/index";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import ReactPaginate from "react-paginate";
import general_constant from "../../../helpers/general_constant.json";
import Select from "react-select";
import UnsavedChangesWarning from "../../../helpers/unsaved_changes_warning";
import routes from "../../../helpers/routes.json";
import "../../../assets/css/pagination.css";

const Terminal = (props) => {
  const list_all_terminal = props.list_all_terminal;
  const list_detail_terminal_card = props.list_detail_terminal_card;
  const list_regional = props.list_regional;
  const list_kanwil = props.list_kanwil;
  const list_terminal = props.list_terminal;
  const list_kanca = props.list_kanca;
  const list_detail_terminal_configuration =
    props.list_detail_terminal_configuration;
  const list_master_terminal_configuration =
    props.list_master_terminal_configuration;
  const message = props.message_all_terminal;
  const response_code = props.response_code_all_terminal;
  const response_code_terminal_card = props.response_code_terminal_card;
  const total_pages_terminal = props.total_pages_terminal;
  const active_page_terminal = props.active_page_terminal;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();
  const [Prompt, setDirty, setPristine] = UnsavedChangesWarning();

  const [modalDelete, setModalDelete] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const [modalTerminalCard, setModalTerminalCard] = useState(false);
  const [modalTerminalConfiguration, setModalTerminalConfiguration] =
    useState(false);

  const [data, setData] = useState(null);
  const [selectedData, setSelectedData] = useState(null);
  const [selectedRegional, setSelectedRegional] = useState(null);
  const [selectedKanwil, setSelectedKanwil] = useState(null);
  const [selectedKanca, setSelectedKanca] = useState(null);
  const [selectedTerminalId, setSelectedTerminalId] = useState(null);
  const [changePasswordData, setChangePasswordData] = useState(null);
  const [confirmNewPassword, setConfirmNewPassword] = useState(null);

  const [showPagination, setShowPagination] = useState(false);
  const [isShowTable, setIsShowTable] = useState(false);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);
  const [modalChangePassword, setModalChangePassword] = useState(false);
  const [isShowOldPassword, setIsShowOldPassword] = useState(false);
  const [isShowNewPassword, setIsShowNewPassword] = useState(false);
  const [isShowConfirmPassword, setIsShowConfirmPassword] = useState(false);
  const [sweetAlertText, setSweeAlertText] = useState(null);
  const [validOldPassword, setValidOldPassword] = useState(false);
  const [validNewPassword, setValidNewPassword] = useState(false);

  const removeBodyCss = () => {
    document.body.classList.add("no_padding");
  };
  const handleRegional = (selectedRegional) => {
    if (selectedRegional && selectedRegional.length > 0) {
      let id = [];
      selectedRegional.map((value) => id.push(value.value));
      props.readTerminalByRegional({ id });
      props.readKanwil({ id });
    }
    setSelectedRegional(selectedRegional);
  };
  const handleKanwil = (selectedKanwil) => {
    if (selectedKanwil && selectedKanwil.length > 0) {
      let id = [];
      selectedKanwil.map((value) => id.push(value.value));
      props.readTerminalByKanwil({ id });
      props.readKanca({ id });
    }
    setSelectedKanwil(selectedKanwil);
  };
  const handleKanca = (selectedKanca) => {
    if (selectedKanca && selectedKanca.length > 0) {
      let id = [];
      selectedKanca.map((value) => id.push(value.value));
      props.readTerminalByKanca({ id });
    }
    setSelectedKanca(selectedKanca);
  };
  const handleTerminalId = (selectedTerminalId) => {
    setSelectedTerminalId(selectedTerminalId);
  };
  const handleSearch = async () => {
    let listTerminalId = [];
    if (selectedTerminalId) {
      await selectedTerminalId.map((value) => listTerminalId.push(value.value));
    } else {
      await list_terminal.map((value) => listTerminalId.push(value.value));
    }
    setData({ ...data, listTerminalId: listTerminalId });
    props.readAllTerminal({ ...data, listTerminalId: listTerminalId });
    setIsShowTable(true);
    setShowPagination(true);
  };
  const handlePageClick = (value) => {
    props.readAllTerminal({ ...data, pageNo: value.selected });
    setData({ ...data, pageNo: value.selected });
  };

  const onValidateNewPassword = (value) => {
    setChangePasswordData({
      ...changePasswordData,
      newPassword: value,
    });
    setDirty();
    if (value.length >= 8 && value.length <= 12) {
      setValidNewPassword(true);
    } else {
      setValidNewPassword(false);
    }
  };
  const onValidateOldPassword = (value) => {
    setChangePasswordData({
      ...changePasswordData,
      oldPassword: value,
    });
    setDirty();
    if (value.length >= 8 && value.length <= 12) {
      setValidOldPassword(true);
    } else {
      setValidOldPassword(false);
    }
  };

  const ShowChangePassword = () => {
    setChangePasswordData({
      terminalId: selectedData && selectedData.terminalId,
    });
    setModalChangePassword(!modalChangePassword);
  };
  const onSubmitChangePassword = async () => {
    await props.changePasswordTerminal({
      ...changePasswordData,
    });
    setSweeAlertText("Password has successfully changed!");
    setIsShowSweetAlert(true);
    setPristine();
  };
  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (response_code === general_constant.success_response_code) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setSelectedData(null);
              setModalDetail(false);
              setModalChangePassword(false);
              history.push(routes.terminal);
            }}
          >
            {sweetAlertText}
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message}
          </SweetAlert>
        );
      }
    }
    return value;
  };
  const ButtonSubmitChangePassword = () => {
    if (
      changePasswordData &&
      changePasswordData.newPassword &&
      changePasswordData.oldPassword &&
      changePasswordData.newPassword === confirmNewPassword &&
      confirmNewPassword &&
      validNewPassword &&
      validOldPassword &&
      Object.keys(changePasswordData).length >= 3 &&
      Object.values(changePasswordData).every((value) => value !== "")
    ) {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          onClick={() => {
            onSubmitChangePassword();
          }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save Password
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          disabled
          style={{ cursor: "default" }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save Password
        </button>
      );
    }
  };

  useEffect(() => {
    props.readRegional();
    props.readTerminalByRegional({ id: [0] });
    props.readMasterTerminalConfiguration();
    setData({
      listTerminalId: [],
      pageNo: 0,
      pageSize: 10,
      sortBy: "terminalId",
      sortType: "asc",
    });
  }, []);
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs
            title={"Profiling Machine"}
            breadcrumbItem={"Terminal"}
          />
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <AvForm className="needs-validation">
                    <Row>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">Regional</label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleRegional(...args);
                            }}
                            options={list_regional}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">
                            Kantor Wilayah
                          </label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleKanwil(...args);
                            }}
                            options={list_kanwil}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">Kantor Cabang</label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleKanca(...args);
                            }}
                            options={list_kanca}
                          />
                        </FormGroup>
                      </Col>
                      <Col md="3">
                        <FormGroup className="select2-container">
                          <label className="control-label">Terminal Id</label>
                          <Select
                            isMulti={true}
                            placeholder="All"
                            onChange={(...args) => {
                              handleTerminalId(...args);
                            }}
                            options={list_terminal}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="d-flex justify-content-end">
                        <button
                          type="button"
                          className="btn btn-primary waves-effect waves-light"
                          onClick={() => {
                            handleSearch();
                          }}
                        >
                          <i className="bx bx-search-alt-2 font-size-16 align-middle mr-2"></i>{" "}
                          Search
                        </button>
                      </Col>
                    </Row>
                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {isShowTable && (
            <Row>
              <Col className="col-12">
                <Card>
                  <CardBody>
                    <Row className="mb-3 d-flex align-items-end">
                      <Col md="10">
                        <Row className="d-flex align-items-end">
                          <Col md="2">
                            <div className="form-group mb-0">
                              <label>Show Data</label>
                              <div>
                                <select
                                  className="form-control"
                                  defaultValue={10}
                                  onChange={(event) => (
                                    setData({
                                      ...data,
                                      pageSize: parseInt(event.target.value),
                                      pageNo: 0,
                                    }),
                                    props.readAllTerminal({
                                      ...data,
                                      pageSize: parseInt(event.target.value),
                                      pageNo: 0,
                                    })
                                  )}
                                >
                                  <option value={1}>1</option>
                                  <option value={10}>10</option>
                                  <option value={25}>25</option>
                                  <option value={50}>50</option>
                                  <option value={100}>100</option>
                                </select>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Col md="2" className="d-flex justify-content-end">
                        <Link to={routes.add_terminal}>
                          <button
                            type="button"
                            className="btn btn-primary waves-effect waves-light"
                          >
                            <i className="bx bx-edit-alt font-size-16 align-middle mr-2"></i>{" "}
                            New
                          </button>
                        </Link>
                      </Col>
                    </Row>

                    <div className="table-responsive">
                      <Table className="table table-centered table-striped">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Terminal Id</th>
                            <th scope="col">Email</th>
                            <th scope="col">Nomor Rangka</th>
                            <th scope="col">Ip</th>
                            <th scope="col">Unit Kerja</th>
                            <th scope="col">Status</th>
                            <th scope="col">Last Update</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {list_all_terminal &&
                            list_all_terminal.map((value, index) => {
                              return (
                                <tr key={value.id}>
                                  <th scope="row">
                                    <div>{index + 1}</div>
                                  </th>
                                  <td>{value.terminalId}</td>
                                  <td>{value.email}</td>
                                  <td>{value.nomorRangka}</td>
                                  <td>{value.ip}</td>
                                  <td>{value.unitKerja}</td>
                                  <td>
                                    <Badge
                                      color={`${
                                        value.status === "On"
                                          ? "success"
                                          : "danger"
                                      }`}
                                      className="p-2"
                                      style={{ fontSize: "0.8125rem" }}
                                    >
                                      {value.status}
                                    </Badge>
                                  </td>
                                  <td>{parseFullDate(value.updateDate)}</td>
                                  <td>
                                    <div
                                      style={{
                                        display: "grid",
                                        gridAutoFlow: "column",
                                        columnGap: "4px",
                                      }}
                                    >
                                      <button
                                        type="button"
                                        className="btn btn-info waves-effect waves-light"
                                        style={{ minWidth: "max-content" }}
                                        onClick={() => {
                                          setSelectedData(value);
                                          setModalDetail(!modalDetail);
                                        }}
                                      >
                                        <i className="bx bx-show-alt font-size-16 align-middle"></i>
                                      </button>
                                      <button
                                        type="button"
                                        className="btn btn-info waves-effect waves-light"
                                        style={{ minWidth: "max-content" }}
                                        onClick={() => {
                                          setSelectedData(value);
                                          props.readDetailTerminalCard(
                                            value.terminalId
                                          );
                                          setModalTerminalCard(
                                            !modalTerminalCard
                                          );
                                        }}
                                      >
                                        <i className="bx bx-credit-card-alt font-size-16 align-middle"></i>
                                      </button>
                                      <button
                                        type="button"
                                        className="btn btn-info waves-effect waves-light"
                                        style={{ minWidth: "max-content" }}
                                        onClick={() => {
                                          setSelectedData(value);
                                          props.readDetailTerminalConfiguration(
                                            value.terminalId
                                          );
                                          setModalTerminalConfiguration(
                                            !modalTerminalConfiguration
                                          );
                                        }}
                                      >
                                        <i className="bx bx-wrench font-size-16 align-middle"></i>
                                      </button>
                                    </div>
                                  </td>
                                </tr>
                              );
                            })}
                        </tbody>
                      </Table>
                      {!list_all_terminal ||
                        (list_all_terminal.length <= 0 && (
                          <div style={{ textAlign: "center" }}>No Data</div>
                        ))}
                    </div>
                    {showPagination && (
                      <Row className="d-flex align-items-end">
                        <ReactPaginate
                          previousLabel={"previous"}
                          nextLabel={"next"}
                          breakLabel={"..."}
                          breakClassName={"break-me"}
                          pageCount={total_pages_terminal}
                          marginPagesDisplayed={1}
                          pageRangeDisplayed={5}
                          forcePage={active_page_terminal}
                          onPageChange={handlePageClick}
                          containerClassName={"pagination"}
                          subContainerClassName={"pages pagination"}
                          activeClassName={"active"}
                        />
                      </Row>
                    )}
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}

          {/* Modal Detail */}
          <Modal
            isOpen={modalDetail}
            toggle={() => {
              setModalDetail(!modalDetail);
              removeBodyCss();
              setSelectedData(null);
            }}
            size="xl"
            scrollable={true}
          >
            <div
              className="modal-header"
              style={{
                display: "grid",
                gridTemplateColumns: "1fr repeat(2, max-content)",
                alignItems: "center",
              }}
            >
              <h5 className="modal-title mt-0" id="myModalLabel">
                Detail Terminal {selectedData && selectedData.terminalId}
              </h5>
              <Link
                to={{
                  pathname: routes.edit_terminal,
                  editValue: selectedData,
                }}
              >
                <button
                  type="button"
                  className="btn btn-primary waves-effect waves-light"
                >
                  <i className="bx bx-edit font-size-16 align-middle mr-2"></i>{" "}
                  Edit
                </button>
              </Link>
              <button
                type="button"
                onClick={() => {
                  setModalDetail(false);
                  setSelectedData(null);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="table-responsive">
                <Table className="table table-centered">
                  <tbody>
                    <tr>
                      <th>Terminal Id</th>
                      <td>{selectedData ? selectedData.terminalId : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Terminal Id BRI</th>
                      <td>{selectedData ? selectedData.terminalIdBri : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Status</th>
                      <td>
                        <Badge
                          color={`${
                            selectedData && selectedData.status === "On"
                              ? "success"
                              : "danger"
                          }`}
                          className="p-2"
                          style={{ fontSize: "0.8125rem" }}
                        >
                          {selectedData ? selectedData.status : "-"}
                        </Badge>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td>{selectedData ? selectedData.email : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Password</th>
                      <td>********</td>
                      <td
                        style={{
                          display: "flex",
                          justifyContent: "flex-end",
                        }}
                      >
                        <Button
                          color="light"
                          className="waves-effect waves-light"
                          onClick={ShowChangePassword}
                        >
                          Change Password
                        </Button>
                      </td>
                    </tr>
                    <tr>
                      <th>Nomor Rangka</th>
                      <td>{selectedData ? selectedData.nomorRangka : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>IP</th>
                      <td>{selectedData ? selectedData.ip : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>SW Version</th>
                      <td>{selectedData ? selectedData.sw_version : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>SPV</th>
                      <td>{selectedData ? selectedData.spv : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Kode SPV</th>
                      <td>{selectedData ? selectedData.kodeSpv : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Teller Id</th>
                      <td>{selectedData ? selectedData.tellerId : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Teller Id Butab</th>
                      <td>{selectedData ? selectedData.tellerIdButab : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Regional</th>
                      <td>{selectedData ? selectedData.regional.name : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Kantor Wilayah</th>
                      <td>{selectedData ? selectedData.kanwil.name : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Kantor Cabang</th>
                      <td>{selectedData ? selectedData.kanca.name : "-"}</td>
                      <td></td>
                    </tr>{" "}
                    <tr>
                      <th>Unit Kerja</th>
                      <td>{selectedData ? selectedData.unitKerja : "-"}</td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Alamat Unit Kerja</th>
                      <td>
                        {selectedData ? selectedData.alamatUnitKerja : "-"}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Created Date</th>
                      <td>
                        {parseFullDate(
                          selectedData ? selectedData.createdDate : "-"
                        )}
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <th>Update Date</th>
                      <td>
                        {parseFullDate(
                          selectedData ? selectedData.updateDate : "-"
                        )}
                      </td>
                      <td></td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-danger waves-effect waves-light"
                style={{ minWidth: "max-content" }}
                onClick={() => {
                  setModalDelete(!modalDelete);
                }}
              >
                <i className="bx bx-trash font-size-16 align-middle"></i>
              </button>
              <button
                type="button"
                onClick={() => {
                  setModalDetail(!modalDetail);
                  removeBodyCss();
                  setSelectedData(null);
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </Modal>

          {/* Modal Change Password */}
          <Modal isOpen={modalChangePassword}>
            <div className="modal-header">
              <h5 className="modal-title mt-0" id="myModalLabel">
                Change Terminal Password
              </h5>
              <button
                type="button"
                onClick={() => {
                  setModalChangePassword(!modalChangePassword);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div
              className="modal-body"
              style={{ paddingLeft: "80px", paddingRight: "80px" }}
            >
              <div className="d-flex justify-content-center">
                <AvForm>
                  <div className="form-group mb-0" style={{ display: "grid" }}>
                    <label
                      htmlFor="example-datetime-local-input"
                      className="col-form-label"
                    >
                      Current Password
                    </label>
                    <div
                      style={{
                        display: "grid",
                        gridTemplateColumns: "1fr 48px",
                        alignItems: "flex-start",
                        columnGap: "8px",
                      }}
                    >
                      <AvField
                        name="oldPassword"
                        type={isShowOldPassword ? "text" : "password"}
                        errorMessage="Type your valid current password"
                        validate={{
                          required: { value: true },
                          maxLength: { value: 12 },
                          minLength: { value: 8 },
                          pattern: { value: "^[A-Za-z0-9]+$" },
                        }}
                        value={
                          changePasswordData && changePasswordData.oldPassword
                        }
                        onChange={(event) =>
                          onValidateOldPassword(event.target.value)
                        }
                      />
                      <Button
                        color="light"
                        outline
                        onClick={() => setIsShowOldPassword(!isShowOldPassword)}
                      >
                        {isShowOldPassword ? (
                          <i className="bx bx-hide font-size-16 align-middle"></i>
                        ) : (
                          <i className="bx bx-show font-size-16 align-middle"></i>
                        )}
                      </Button>
                    </div>
                  </div>
                  <div className="form-group mb-0" style={{ display: "grid" }}>
                    <label
                      htmlFor="example-datetime-local-input"
                      className="col-form-label"
                    >
                      New Password
                    </label>
                    <div
                      style={{
                        display: "grid",
                        gridTemplateColumns: "1fr 48px",
                        alignItems: "flex-start",
                        columnGap: "8px",
                      }}
                    >
                      <AvField
                        name="newPassword"
                        type={isShowNewPassword ? "text" : "password"}
                        errorMessage="New password must be 8 - 12 character and cannot contain special character"
                        validate={{
                          required: { value: true },
                          maxLength: { value: 12 },
                          minLength: { value: 8 },
                          pattern: { value: "^[A-Za-z0-9]+$" },
                        }}
                        value={
                          changePasswordData && changePasswordData.newPassword
                        }
                        onChange={(event) =>
                          onValidateNewPassword(event.target.value)
                        }
                      />
                      <Button
                        color="light"
                        outline
                        onClick={() => setIsShowNewPassword(!isShowNewPassword)}
                      >
                        {isShowNewPassword ? (
                          <i className="bx bx-hide font-size-16 align-middle"></i>
                        ) : (
                          <i className="bx bx-show font-size-16 align-middle"></i>
                        )}
                      </Button>
                    </div>
                  </div>
                  <div className="form-group mb-0" style={{ display: "grid" }}>
                    <label
                      htmlFor="example-datetime-local-input"
                      className="col-form-label"
                    >
                      Re-Type New Password
                    </label>
                    <div
                      style={{
                        display: "grid",
                        gridTemplateColumns: "1fr 48px",
                        alignItems: "flex-start",
                        columnGap: "8px",
                      }}
                    >
                      <AvField
                        name="confirmNewPassword"
                        type={isShowConfirmPassword ? "text" : "password"}
                        errorMessage="New Password do not match"
                        validate={{
                          required: { value: true },
                          maxLength: { value: 12 },
                          pattern: { value: "^[A-Za-z0-9]+$" },
                          match: { value: "newPassword" },
                        }}
                        onChange={(event) =>
                          setConfirmNewPassword(event.target.value)
                        }
                        value={confirmNewPassword}
                      />
                      <Button
                        color="light"
                        outline
                        onClick={() =>
                          setIsShowConfirmPassword(!isShowConfirmPassword)
                        }
                      >
                        {isShowConfirmPassword ? (
                          <i className="bx bx-hide font-size-16 align-middle"></i>
                        ) : (
                          <i className="bx bx-show font-size-16 align-middle"></i>
                        )}
                      </Button>
                    </div>
                  </div>
                </AvForm>
              </div>
            </div>
            <div className="modal-footer">
              <ButtonSubmitChangePassword />
            </div>
          </Modal>

          {/* Modal Terminal Card */}
          <Modal
            isOpen={modalTerminalCard}
            toggle={() => {
              setModalTerminalCard(!modalTerminalCard);
              removeBodyCss();
              setSelectedData(null);
            }}
            size="xl"
            scrollable={true}
          >
            <div
              className="modal-header"
              className="modal-header"
              style={{
                display: "grid",
                gridTemplateColumns: "1fr repeat(2, max-content)",
                alignItems: "center",
              }}
            >
              <h5 className="modal-title mt-0" id="myModalLabel">
                Terminal Card {selectedData && selectedData.terminalId}
              </h5>
              <Link
                to={{
                  pathname: routes.edit_terminal_card,
                  editValue: {
                    terminalId: selectedData && selectedData.terminalId,
                    listData: list_detail_terminal_card,
                  },
                }}
              >
                <button
                  type="button"
                  className="btn btn-primary waves-effect waves-light"
                >
                  <i className="bx bx-edit font-size-16 align-middle mr-2"></i>{" "}
                  Edit
                </button>
              </Link>
              <button
                type="button"
                onClick={() => {
                  setModalTerminalCard(false);
                  setSelectedData(null);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="table-responsive">
                <Table className="table table-centered table-striped">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Jenis Kartu</th>
                      <th scope="col">Name Produk</th>
                      <th scope="col">Max. Tarik Tunai</th>
                      <th scope="col">Max. Transfer Sesama</th>
                      <th scope="col">Max. Transfer Antar Bank</th>
                      <th scope="col">Biaya Penggantian Kartu</th>
                    </tr>
                  </thead>
                  <tbody>
                    {response_code_terminal_card ===
                      general_constant.success_response_code &&
                      list_detail_terminal_card &&
                      list_detail_terminal_card.map((value, index) => {
                        return (
                          <tr key={value.id}>
                            <th scope="row">
                              <div>{index + 1}</div>
                            </th>
                            <td>{value.jenisKartu}</td>
                            <td>{value.namaProduk}</td>
                            <td>{parseToRupiah(value.maxTarikTunai)}</td>
                            <td>{parseToRupiah(value.maxTransferSesama)}</td>
                            <td>{parseToRupiah(value.maxTransferAntarBank)}</td>
                            <td>{parseToRupiah(value.biayaPenggantiKartu)}</td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                {response_code_terminal_card !==
                  general_constant.success_response_code && (
                  <div style={{ textAlign: "center" }}>No Data</div>
                )}
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => {
                  setModalTerminalCard(!modalTerminalCard);
                  removeBodyCss();
                  setSelectedData(null);
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </Modal>

          {/* Modal Terminal Configuration */}
          <Modal
            isOpen={modalTerminalConfiguration}
            toggle={() => {
              setModalTerminalConfiguration(!modalTerminalConfiguration);
              removeBodyCss();
              setSelectedData(null);
            }}
            size="lg"
            scrollable={true}
          >
            <div
              className="modal-header"
              style={{
                display: "grid",
                gridTemplateColumns: "1fr repeat(2, max-content)",
                alignItems: "center",
              }}
            >
              <h5 className="modal-title mt-0" id="myModalLabel">
                Terminal Configuration {selectedData && selectedData.terminalId}
              </h5>
              <Link
                to={{
                  pathname: routes.edit_terminal_configuration,
                  editValue: {
                    terminalId: selectedData && selectedData.terminalId,
                    listData: list_detail_terminal_configuration,
                    listMsTerminalConfig: list_master_terminal_configuration,
                  },
                }}
              >
                <button
                  type="button"
                  className="btn btn-primary waves-effect waves-light"
                >
                  <i className="bx bx-edit font-size-16 align-middle mr-2"></i>{" "}
                  Edit
                </button>
              </Link>
              <button
                type="button"
                onClick={() => {
                  setModalTerminalConfiguration(false);
                  setSelectedData(null);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="table-responsive">
                <Table className="table table-centered table-striped">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Section</th>
                      <th scope="col">Name</th>
                      <th scope="col">Value</th>
                      <th scope="col">Is Active</th>
                      <th scope="col">Created By</th>
                      <th scope="col">Created At</th>
                    </tr>
                  </thead>
                  <tbody>
                    {list_detail_terminal_configuration &&
                      list_detail_terminal_configuration.map((value, index) => {
                        return (
                          <tr key={value.id}>
                            <th scope="row">
                              <div>{index + 1}</div>
                            </th>
                            <td>{value.configSection}</td>
                            <td>{value.configName}</td>
                            <td>{value.configValue}</td>
                            <td>
                              <Badge
                                color={`${
                                  value.isAktif === "On" ? "success" : "danger"
                                }`}
                                className="p-2"
                                style={{ fontSize: "0.8125rem" }}
                              >
                                {value.isAktif}
                              </Badge>
                            </td>
                            <td>{value.createdBy}</td>
                            <td>{parseFullDate(value.createdAt)}</td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                {!list_detail_terminal_configuration && (
                  <div style={{ textAlign: "center" }}>No Data</div>
                )}
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => {
                  setModalTerminalConfiguration(!modalTerminalConfiguration);
                  removeBodyCss();
                  setSelectedData(null);
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </Modal>

          {/* Modal Delete */}
          <Modal
            isOpen={modalDelete}
            toggle={() => {
              setModalDelete(!modalDelete);
              removeBodyCss();
            }}
          >
            <div className="modal-header">
              <h5 className="modal-title mt-0" id="myModalLabel">
                Delete Terminal
              </h5>
              <button
                type="button"
                onClick={() => {
                  setModalDelete(false);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              Are you sure want to delete terminal{" "}
              <b>{selectedData && selectedData.terminalId}</b> ?
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => {
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-danger waves-effect waves-light"
                onClick={() => {
                  setIsShowSweetAlert(true);
                  setSweeAlertText("The terminal has successfully deleted!");
                  props.deleteTerminal({
                    terminalId: selectedData.terminalId,
                    data: data,
                  });
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                }}
              >
                Delete
              </button>
            </div>
          </Modal>
          {Prompt}
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const {
    list_all_terminal,
    message_all_terminal,
    response_code_all_terminal,
    loading,
    total_pages_terminal,
    active_page_terminal,
  } = state.Terminal;
  const {
    list_regional,
    list_kanwil,
    list_kanca,
    list_terminal,
    message_kanwil,
    message_kanca,
    message_regional,
    message_terminal,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
    response_code_terminal,
  } = state.Properties;
  const { list_detail_terminal_card, response_code_terminal_card } =
    state.TerminalCard;
  const { list_detail_terminal_configuration } = state.TerminalConfiguration;
  const {
    list_master_terminal_configuration,
    message_master_terminal_configuration,
    response_master_code_terminal_configuration,
  } = state.MasterTerminalConfiguration;
  return {
    list_all_terminal,
    list_detail_terminal_card,
    list_regional,
    list_kanwil,
    list_kanca,
    list_terminal,
    list_detail_terminal_configuration,
    list_master_terminal_configuration,
    message_kanwil,
    message_kanca,
    message_regional,
    message_terminal,
    message_all_terminal,
    message_master_terminal_configuration,
    response_master_code_terminal_configuration,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
    response_code_terminal,
    response_code_all_terminal,
    response_code_terminal_card,
    total_pages_terminal,
    active_page_terminal,
    loading,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readAllTerminal,
      readDetailTerminalCard,
      readRegional,
      readTerminalByRegional,
      readKanwil,
      readTerminalByKanwil,
      readKanca,
      readTerminalByKanca,
      readJenisTransaksi,
      deleteTerminal,
      readDetailTerminalConfiguration,
      readMasterTerminalConfiguration,
      changePasswordTerminal,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(Terminal);
