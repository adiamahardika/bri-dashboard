import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  CardTitle,
} from "reactstrap";
import {
  readRegional,
  readKanwil,
  readKanca,
} from "../../../store/pages/properties/actions";
import { updateTerminal } from "../../../store/pages/profilingMachines/terminal/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { useHistory } from "react-router";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import general_constant from "../../../helpers/general_constant.json";
import UnsavedChangesWarning from "../../../helpers/unsaved_changes_warning";
import Switch from "react-switch";
import routes from "../../../helpers/routes.json";

const EditTerminal = (props) => {
  const message_all_terminal = props.message_all_terminal;
  const response_code_all_terminal = props.response_code_all_terminal;
  const list_all_regional = props.list_all_regional;
  const list_all_kanwil = props.list_all_kanwil;
  const list_all_kanca = props.list_all_kanca;
  const editValue = props.location.editValue;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();
  const [Prompt, setDirty, setPristine] = UnsavedChangesWarning();

  const [terminalProfileData, setTerminalProfileData] = useState(null);
  const [validEmail, setValidEmail] = useState(false);
  const [validIpAddress, setValidIpAddress] = useState(false);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);
  const [isActive, setIsActive] = useState(true);

  const onChangeTerminalProfileData = (event) => {
    setTerminalProfileData({
      ...terminalProfileData,
      [event.target.name]: event.target.value,
    });
    setDirty();
  };
  const onValidateEmail = (email) => {
    let regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    setTerminalProfileData({
      ...terminalProfileData,
      email: email,
    });
    setDirty();
    if (regex.test(email) === true) {
      setValidEmail(true);
    } else {
      setValidEmail(false);
    }
  };
  const onvalidateIpAddress = (ip) => {
    let regex =
      /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    setTerminalProfileData({
      ...terminalProfileData,
      ip: ip,
    });
    setDirty();
    if (regex.test(ip) === true) {
      setValidIpAddress(true);
    } else {
      setValidIpAddress(false);
    }
  };
  const handleIsActive = () => {
    setTerminalProfileData({
      ...terminalProfileData,
      status: !isActive === true ? "On" : "Off",
    });
    setIsActive(!isActive);
    setDirty();
  };
  const onSubmitCreate = async () => {
    props.updateTerminal({
      ...terminalProfileData,
      updateDate: new Date(),
      password: "",
    });
    setIsShowSweetAlert(true);
    setPristine();
  };

  const ButtonSubmitCreate = () => {
    if (
      terminalProfileData &&
      Object.keys(terminalProfileData).length >= 17 &&
      Object.values(terminalProfileData).every((value) => value !== "") &&
      validEmail &&
      validIpAddress
    ) {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          onClick={() => {
            onSubmitCreate();
          }}
        >
          <i className="bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          disabled
          style={{ cursor: "default" }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    }
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (
        response_code_all_terminal === general_constant.success_response_code
      ) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              history.push(routes.terminal);
            }}
          >
            Terminal has successfully updated!
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message_all_terminal}
          </SweetAlert>
        );
      }
    }
    return value;
  };
  const Offsymbol = (props) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 12,
          color: "#fff",
          paddingRight: 2,
        }}
      >
        {" "}
        Off
      </div>
    );
  };

  const OnSymbol = (props) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 12,
          color: "#fff",
          paddingRight: 2,
        }}
      >
        {" "}
        On
      </div>
    );
  };

  useEffect(() => {
    if (editValue) {
      setTerminalProfileData(editValue);
      setIsActive(editValue && editValue.status === "On" ? true : false);
      props.readRegional();
      props.readKanwil({
        id: [editValue && editValue.regional.id],
      });
      props.readKanca({
        id: [editValue && editValue.kanwil.id],
      });
      setValidEmail(!validEmail);
      setValidIpAddress(!validIpAddress);
    } else {
      history.push(routes.terminal);
    }
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title={"Terminal"} breadcrumbItem={"Edit Terminal"} />
          <Card>
            <CardBody>
              <div
                className="mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-start",
                  alignItems: "center",
                  gridTemplateColumns: "1fr repeat(2, max-content) ",
                  columnGap: "2rem",
                }}
              >
                <CardTitle>Edit Terminal Profile</CardTitle>
                <FormGroup className="select2-container mb-0 align-content-center">
                  <label className="control-label mr-2">Status :</label>
                  <Switch
                    uncheckedIcon={<Offsymbol />}
                    checkedIcon={<OnSymbol />}
                    onColor="#626ed4"
                    checked={isActive}
                    onChange={() => handleIsActive()}
                  />
                </FormGroup>
                <ButtonSubmitCreate />
              </div>
              <AvForm>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="terminalId"
                      label="Terminal Id"
                      style={{ backgroundColor: "#ced4da" }}
                      type="text"
                      value={
                        terminalProfileData && terminalProfileData.terminalId
                      }
                      disabled
                    />
                  </Col>
                  <Col md={6}>
                    <AvField
                      name="terminalIdBri"
                      label="Terminal Id BRI"
                      type="text"
                      errorMessage="Enter Terminal Id BRI"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={
                        terminalProfileData && terminalProfileData.terminalIdBri
                      }
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={12}>
                    <AvField
                      name="email"
                      label="Email"
                      type="email"
                      placeholder="ex: user@mail.com"
                      errorMessage="Enter Valid Email"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={terminalProfileData && terminalProfileData.email}
                      onChange={(event) => onValidateEmail(event.target.value)}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <AvField
                      name="sw_version"
                      label="Software Version"
                      type="text"
                      errorMessage="Enter SW Version"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={
                        terminalProfileData && terminalProfileData.sw_version
                      }
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={4}>
                    <AvField
                      name="ip"
                      label="IP Address"
                      type="text"
                      placeholder="ex: 10.10.10.10"
                      errorMessage="Enter Valid IP Address"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                        pattern: {
                          value:
                            "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$",
                        },
                      }}
                      value={terminalProfileData && terminalProfileData.ip}
                      onChange={(event) =>
                        onvalidateIpAddress(event.target.value)
                      }
                    />
                  </Col>
                  <Col md={4}>
                    <AvField
                      name="nomorRangka"
                      label="Nomor Rangka"
                      type="text"
                      errorMessage="Enter Nomor Rangka"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={
                        terminalProfileData && terminalProfileData.nomorRangka
                      }
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="tellerId"
                      label="Teller Id"
                      type="text"
                      errorMessage="Enter Teller Id"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={
                        terminalProfileData && terminalProfileData.tellerId
                      }
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={6}>
                    {" "}
                    <AvField
                      name="tellerIdButab"
                      label="Teller Id Butab"
                      type="text"
                      errorMessage="Enter Teller Id Butab"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={
                        terminalProfileData && terminalProfileData.tellerIdButab
                      }
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="spv"
                      label="Supervisor"
                      type="text"
                      errorMessage="Enter Supervisor"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={terminalProfileData && terminalProfileData.spv}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={6}>
                    <AvField
                      name="kodeSpv"
                      label="Kode Supervisor"
                      type="text"
                      errorMessage="Enter Kode Supervisor"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={terminalProfileData && terminalProfileData.kodeSpv}
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <FormGroup className="select2-container">
                      <label className="control-label">Regional</label>
                      <div>
                        <select
                          name="regional"
                          className="form-control"
                          onChange={(event) => (
                            setTerminalProfileData({
                              ...terminalProfileData,
                              regional: { id: parseInt(event.target.value) },
                            }),
                            props.readKanwil({
                              id: [parseInt(event.target.value)],
                            }),
                            setDirty()
                          )}
                        >
                          {list_all_regional &&
                            list_all_regional.map((value) => (
                              <option
                                key={value.id}
                                value={value.id}
                                onChange={(event) => (
                                  setTerminalProfileData({
                                    ...terminalProfileData,
                                    regional: {
                                      id: parseInt(event.target.value),
                                    },
                                  }),
                                  props.readKanwil({
                                    id: [parseInt(event.target.value)],
                                  }),
                                  setDirty()
                                )}
                                selected={
                                  terminalProfileData &&
                                  terminalProfileData.regional.id === value.id
                                }
                              >
                                {value.name}
                              </option>
                            ))}
                        </select>
                      </div>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup className="select2-container">
                      <label className="control-label">Kantor Wilayah</label>
                      <div>
                        <select
                          name="kanwil"
                          className="form-control"
                          onChange={(event) => (
                            setTerminalProfileData({
                              ...terminalProfileData,
                              kanwil: { id: parseInt(event.target.value) },
                            }),
                            props.readKanca({
                              id: [parseInt(event.target.value)],
                            }),
                            setDirty()
                          )}
                        >
                          {" "}
                          {list_all_kanwil &&
                            list_all_kanwil.map((value) => (
                              <option
                                key={value.id}
                                value={value.id}
                                onChange={(event) => (
                                  setTerminalProfileData({
                                    ...terminalProfileData,
                                    kanwil: {
                                      id: parseInt(event.target.value),
                                    },
                                  }),
                                  props.readKanca({
                                    id: [parseInt(event.target.value)],
                                  }),
                                  setDirty()
                                )}
                                selected={
                                  terminalProfileData &&
                                  terminalProfileData.kanwil.id === value.id
                                }
                              >
                                {value.name}
                              </option>
                            ))}
                        </select>
                      </div>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup className="select2-container">
                      <label className="control-label">Kantor Cabang</label>
                      <div>
                        <select
                          name="kanca"
                          className="form-control"
                          onChange={(event) => (
                            setTerminalProfileData({
                              ...terminalProfileData,
                              kanca: { id: parseInt(event.target.value) },
                            }),
                            setDirty()
                          )}
                        >
                          {" "}
                          {list_all_kanca &&
                            list_all_kanca.map((value) => (
                              <option
                                key={value.id}
                                value={value.id}
                                onChange={(event) => (
                                  setTerminalProfileData({
                                    ...terminalProfileData,
                                    kanca: { id: parseInt(event.target.value) },
                                  }),
                                  setDirty()
                                )}
                                selected={
                                  terminalProfileData &&
                                  terminalProfileData.kanca.id === value.id
                                }
                              >
                                {value.name}
                              </option>
                            ))}
                        </select>
                      </div>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md={6}>
                    <AvField
                      name="unitKerja"
                      label="Unit Kerja"
                      type="text"
                      errorMessage="Enter Unit Kerja"
                      validate={{
                        required: { value: true },
                        maxLength: { value: 50 },
                      }}
                      value={
                        terminalProfileData && terminalProfileData.unitKerja
                      }
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                  <Col md={6}>
                    <AvField
                      name="alamatUnitKerja"
                      label="Alamat Unit Kerja"
                      type="textarea"
                      errorMessage="Enter Alamat Unit Kerja"
                      validate={{
                        required: { value: true },
                      }}
                      value={
                        terminalProfileData &&
                        terminalProfileData.alamatUnitKerja
                      }
                      onChange={onChangeTerminalProfileData}
                    />
                  </Col>
                </Row>
              </AvForm>
            </CardBody>
          </Card>
          {Prompt}
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const { response_code_all_terminal, message_all_terminal } = state.Terminal;
  const {
    list_all_regional,
    list_all_kanwil,
    list_all_kanca,
    message_kanwil,
    message_kanca,
    message_regional,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
  } = state.Properties;
  return {
    list_all_regional,
    list_all_kanwil,
    list_all_kanca,
    message_kanwil,
    message_kanca,
    message_regional,
    message_all_terminal,
    response_code_all_terminal,
    response_code_kanwil,
    response_code_kanca,
    response_code_regional,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateTerminal,
      readRegional,
      readKanwil,
      readKanca,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(EditTerminal);
