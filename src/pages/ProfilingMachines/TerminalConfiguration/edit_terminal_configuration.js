import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  CardTitle,
  Button,
} from "reactstrap";
import { createTerminalConfiguration } from "../../../store/pages/profilingMachines/terminalConfiguration/actions";
import {
  readMsTerminalConfigSection,
  readMasterTerminalConfiguration,
} from "../../../store/pages/masterData/terminalConfiguration/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { useHistory } from "react-router";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import general_constant from "../../../helpers/general_constant.json";
import UnsavedChangesWarning from "../../../helpers/unsaved_changes_warning";
import Switch from "react-switch";
import routes from "../../../helpers/routes.json";

const EditTerminalConfiguration = (props) => {
  const message_terminal_configuration = props.message_terminal_configuration;
  const response_code_terminal_configuration =
    props.response_code_terminal_configuration;
  const editValue = props.location.editValue;
  const list_ms_terminal_config_section = props.list_ms_terminal_config_section;
  const list_master_terminal_configuration =
    props.list_master_terminal_configuration;
  const username = localStorage.getItem("username");
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();

  const [Prompt, setDirty, setPristine] = UnsavedChangesWarning();

  const [terminalConfigData, setTerminalConfigData] = useState(
    list_master_terminal_configuration
  );
  const [isActive, setIsActive] = useState(null);

  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);
  const [showTerminalConfig, setShowTerminalConfig] = useState(false);

  const onSubmitCreate = async () => {
    props.createTerminalConfiguration({
      listTerminalConfig: terminalConfigData,
    });
    setIsShowSweetAlert(true);
    setPristine();
  };
  const handleOnchange = (event, index) => {
    let data = [...terminalConfigData];

    data[index] = {
      ...data[index],
      [event.target.name]: event.target.value,
    };

    setTerminalConfigData(data);
    setDirty();
  };
  const handleSection = (value, index) => {
    let data = [...terminalConfigData];

    data[index] = {
      ...data[index],
      configSection: value,
    };

    setTerminalConfigData(data);
    setDirty();
  };
  const handleIsActive = (event, index) => {
    let data = [...terminalConfigData];
    let isActiveData = [...isActive];

    data[index] = {
      ...data[index],
      isAktif: !isActiveData[index] === true ? "On" : "Off",
    };
    isActiveData[index] = !isActiveData[index];

    setIsActive(isActiveData);
    setTerminalConfigData(data);
    setDirty();
  };
  const handleAddTerminalConfiguration = () => {
    let data = showTerminalConfig.length + 1;
    let data2 = [];
    let data3 = [];
    let newArray = [...showTerminalConfig];

    if (terminalConfigData && terminalConfigData.length > 0) {
      data2 = [...terminalConfigData];
    }
    if (isActive && isActive.length > 0) {
      data3 = [...isActive];
    }

    newArray.push(data);
    data2.push({
      configSection:
        list_ms_terminal_config_section &&
        list_ms_terminal_config_section[0].configSection,
      configValue: "",
      configName: "",
      isAktif: "On",
      createdBy: username,
      terminalId: editValue && editValue.terminalId,
    });
    data3.push(true);

    setShowTerminalConfig(newArray);
    setTerminalConfigData(data2);
    setIsActive(data3);
    setDirty();
  };
  const handleRemoveTerminalConfiguration = (index) => {
    let newArray = [...showTerminalConfig];
    let data2 = [...terminalConfigData];
    let data3 = [...isActive];

    newArray.splice(index, 1);
    data2.splice(index, 1);
    data3.splice(index, 1);

    setIsActive(data3);
    setShowTerminalConfig(newArray);
    setTerminalConfigData(data2);
    setShowTerminalConfig(newArray);
    setDirty();
  };
  const ButtonSubmitCreate = () => {
    if (
      terminalConfigData &&
      terminalConfigData.length >= 1 &&
      terminalConfigData.every((value) => value.configName !== "") &&
      terminalConfigData.every((value) => value.configValue !== "")
    ) {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          onClick={() => {
            onSubmitCreate();
          }}
        >
          <i className="bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          disabled
          style={{ cursor: "default" }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    }
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (
        response_code_terminal_configuration ===
        general_constant.success_response_code
      ) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setTerminalConfigData(null);
              history.push(routes.terminal);
            }}
          >
            Terminal Configuration has successfully edited!
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message_terminal_configuration}
          </SweetAlert>
        );
      }
    }
    return value;
  };
  const Offsymbol = (props) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 12,
          color: "#fff",
          paddingRight: 2,
        }}
      >
        {" "}
        Off
      </div>
    );
  };

  const OnSymbol = (props) => {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 12,
          color: "#fff",
          paddingRight: 2,
        }}
      >
        {" "}
        On
      </div>
    );
  };

  useEffect(() => {
    let listData = editValue && editValue.listData;
    let listMsTerminalConfig = editValue && editValue.listMsTerminalConfig;
    let terminalId = editValue && editValue.terminalId;
    let activeArray = [];
    let dataArray = [];
    if (listData) {
      editValue.listData.map((value) => {
        activeArray.push(value.isAktif === "On" ? true : false);
      });
      setShowTerminalConfig(listData);
      setTerminalConfigData(listData);
      setIsActive(activeArray);
    } else if (terminalId) {
      if (listMsTerminalConfig) {
        listMsTerminalConfig.map((value) => {
          dataArray.push({
            configSection: value.configSection,
            configValue: value.configValue,
            configName: value.configKey,
            isAktif: "On",
            createdBy: username,
            terminalId: terminalId,
          });
        });
        listMsTerminalConfig.map(() => {
          activeArray.push(true);
        });
        setShowTerminalConfig(listMsTerminalConfig);
        setTerminalConfigData(dataArray);
        setIsActive(activeArray);
      } else {
        setShowTerminalConfig([]);
        setIsActive([]);
      }
    } else {
      history.push(routes.terminal);
    }
    props.readMsTerminalConfigSection();
    props.readMasterTerminalConfiguration();
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs
            title={"Terminal"}
            breadcrumbItem={"Edit Terminal Configuration"}
          />
          <Card>
            <CardBody>
              <div
                className="mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-end",
                  alignItems: "center",
                  gridTemplateColumns: "max-content 1fr",
                  columnGap: "8px",
                }}
              >
                <CardTitle>Terminal Configuration</CardTitle>{" "}
                <ButtonSubmitCreate />
              </div>
              <AvForm>
                {showTerminalConfig &&
                  showTerminalConfig.map((value, index) => (
                    <Row key={index}>
                      <Col md={3}>
                        <FormGroup className="select2-container">
                          <label className="control-label">Section</label>
                          <div>
                            <select
                              className="form-control"
                              name="configSection"
                              onChange={(event) =>
                                handleSection(event.target.value, index)
                              }
                            >
                              {list_ms_terminal_config_section &&
                                list_ms_terminal_config_section.map(
                                  (listValue, listIndex) => (
                                    <option
                                      onChange={(event) =>
                                        handleSection(event.target.value, index)
                                      }
                                      key={listIndex}
                                      name="configSection"
                                      value={listValue.configSection}
                                      selected={
                                        terminalConfigData &&
                                        terminalConfigData[index]
                                          .configSection ===
                                          listValue.configSection
                                      }
                                    >
                                      {listValue.configSection}
                                    </option>
                                  )
                                )}
                            </select>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col md={3}>
                        <AvField
                          name="configName"
                          label="Name"
                          type="text"
                          value={
                            terminalConfigData &&
                            terminalConfigData[index].configName
                          }
                          validate={{
                            maxLength: { value: 25 },
                          }}
                          onChange={(event) => handleOnchange(event, index)}
                        />
                      </Col>
                      <Col md={3}>
                        <AvField
                          name="configValue"
                          label="Value"
                          type="text"
                          value={
                            terminalConfigData &&
                            terminalConfigData[index].configValue
                          }
                          validate={{
                            maxLength: { value: 25 },
                          }}
                          onChange={(event) => handleOnchange(event, index)}
                        />
                      </Col>
                      <Col md={1}>
                        <FormGroup className="select2-container">
                          <label className="control-label">On / Off</label>
                          <Switch
                            uncheckedIcon={<Offsymbol />}
                            checkedIcon={<OnSymbol />}
                            onColor="#626ed4"
                            checked={isActive[index]}
                            onChange={(event) => handleIsActive(event, index)}
                          />
                        </FormGroup>
                      </Col>
                      <Col
                        md={1}
                        style={{ display: "grid", alignSelf: "flex-end" }}
                        className="m-0"
                      >
                        <FormGroup className="select2-container">
                          <label className="control-label"></label>
                          <Button
                            color="light"
                            className="btn btn-light waves-effect"
                            onClick={() => {
                              handleRemoveTerminalConfiguration(index);
                            }}
                          >
                            <i className="bx bxs-x-circle font-size-16 align-middle"></i>
                          </Button>
                        </FormGroup>
                      </Col>
                    </Row>
                  ))}
                {showTerminalConfig && showTerminalConfig.length < 8 && (
                  <Row>
                    <Col md={12}>
                      <button
                        type="button"
                        color="link"
                        className="btn btn-link waves-effect"
                        onClick={() => {
                          handleAddTerminalConfiguration();
                        }}
                      >
                        + Tambah Terminal Configuration
                      </button>
                    </Col>
                  </Row>
                )}
              </AvForm>
            </CardBody>
          </Card>
          {Prompt}
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const {
    message_terminal_configuration,
    response_code_terminal_configuration,
  } = state.TerminalConfiguration;
  const {
    list_ms_terminal_config_section,
    message_master_terminal_configuration,
    response_master_code_terminal_configuration,
    list_master_terminal_configuration,
  } = state.MasterTerminalConfiguration;
  return {
    list_ms_terminal_config_section,
    list_master_terminal_configuration,
    message_master_terminal_configuration,
    message_terminal_configuration,
    response_master_code_terminal_configuration,
    response_code_terminal_configuration,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      createTerminalConfiguration,
      readMsTerminalConfigSection,
      readMasterTerminalConfiguration,
    },
    dispatch
  );

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(EditTerminalConfiguration);
