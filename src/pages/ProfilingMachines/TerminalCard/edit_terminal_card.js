import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  CardBody,
  FormGroup,
  Row,
  Col,
  CardTitle,
  Button,
} from "reactstrap";
import { readAllCard } from "../../../store/pages/masterData/card/actions";
import { createTerminalCard } from "../../../store/pages/profilingMachines/terminalCard/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { useHistory } from "react-router";
import Breadcrumbs from "../../../components/Common/Breadcrumb";
import code_all_permissions from "../../../helpers/code_all_permissions.json";
import SweetAlert from "react-bootstrap-sweetalert";
import general_constant from "../../../helpers/general_constant.json";
import UnsavedChangesWarning from "../../../helpers/unsaved_changes_warning";
import routes from "../../../helpers/routes.json";

const EditTerminalCard = (props) => {
  const message_terminal_card = props.message_terminal_card;
  const response_code_terminal_card = props.response_code_terminal_card;
  const list_all_card = props.list_all_card;
  const editValue = props.location.editValue;
  const permissions = JSON.parse(localStorage.getItem("permission"));
  const history = useHistory();
  const [Prompt, setDirty, setPristine] = UnsavedChangesWarning();

  const [terminalCardData, setTerminalCardData] = useState(null);
  const [isShowSweetAlert, setIsShowSweetAlert] = useState(false);

  const [showJenisKartu, setShowJenisKartu] = useState(false);
  const [terminalId, setTerminalId] = useState(true);

  const onSubmitCreate = async () => {
    let data = [];
    await terminalCardData.map((value) => {
      data.push({ idMsCard: value.id, terminalId: terminalId });
    });
    props.createTerminalCard({ listCard: data, terminalId: terminalId });
    setIsShowSweetAlert(true);
    setPristine();
  };
  const handleTerminalCard = (value, index) => {
    let data = [...terminalCardData];

    data[index] = {
      id: parseInt(value),
    };

    setTerminalCardData(data);
    setDirty();
  };
  const handleAddJenisKartu = () => {
    let data = showJenisKartu.length + 1;
    let data2 = [];
    let newArray = [...showJenisKartu];

    if (terminalCardData && terminalCardData.length > 0) {
      data2 = [...terminalCardData];
    }

    newArray.push(data);
    data2.push({
      id: list_all_card[0].id,
    });

    setShowJenisKartu(newArray);
    setTerminalCardData(data2);
    setDirty();
  };
  const handleRemoveJenisKartu = (index) => {
    let newArray = [...showJenisKartu];
    let data2 = [...terminalCardData];

    newArray.splice(index, 1);
    data2.splice(index, 1);

    setShowJenisKartu(newArray);
    setTerminalCardData(data2);
    setShowJenisKartu(newArray);
    setDirty();
  };

  const ButtonSubmitCreate = () => {
    if (terminalCardData && terminalCardData.length >= 1) {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          onClick={() => {
            onSubmitCreate();
          }}
        >
          <i className="bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    } else {
      return (
        <button
          type="button"
          className="btn btn-primary waves-effect waves-light"
          disabled
          style={{ cursor: "default" }}
        >
          <i className="bx bx bx-save font-size-16 align-middle mr-2"></i>
          Save
        </button>
      );
    }
  };

  const ShowSweetAlert = () => {
    let value = null;
    if (isShowSweetAlert) {
      if (
        response_code_terminal_card === general_constant.success_response_code
      ) {
        value = (
          <SweetAlert
            title={general_constant.success_message}
            success
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
              setTerminalCardData(null);
              history.push(routes.terminal);
            }}
          >
            Terminal card has successfully edited!
          </SweetAlert>
        );
      } else {
        value = (
          <SweetAlert
            title={general_constant.error_message}
            error
            confirmBtnBsStyle="success"
            onConfirm={() => {
              setIsShowSweetAlert(false);
            }}
          >
            {message_terminal_card}
          </SweetAlert>
        );
      }
    }
    return value;
  };

  useEffect(() => {
    let listData = editValue && editValue.listData;
    let terminalId = editValue && editValue.terminalId;
    if (editValue) {
      props.readAllCard({
        size: 1000,
        page_no: 0,
        sort_by: "jenisKartu",
        sort_type: "asc",
      });
      if (listData) {
        setShowJenisKartu(listData);
        setTerminalCardData(listData);
      } else {
        setShowJenisKartu([]);
        setTerminalCardData([]);
      }
      setTerminalId(terminalId);
    } else {
      history.push(routes.terminal);
    }
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs
            title={"Terminal"}
            breadcrumbItem={"Edit Terminal Card"}
          />
          <Card>
            <CardBody>
              <div
                className="mb-3"
                style={{
                  display: "grid",
                  justifyItems: "flex-start",
                  alignItems: "center",
                  gridTemplateColumns: "1fr max-content",
                  columnGap: "2rem",
                }}
              >
                <CardTitle>Edit Terminal Card</CardTitle> <ButtonSubmitCreate />
              </div>
              <AvForm>
                {showJenisKartu &&
                  showJenisKartu.map((value, index) => (
                    <Row key={index}>
                      <Col md={4}>
                        <FormGroup className="select2-container">
                          <label className="control-label">
                            Jenis Kartu {index + 1}
                          </label>
                          <div>
                            <select
                              className="form-control"
                              defaultValue={
                                terminalCardData && terminalCardData[index].id
                              }
                              onChange={(event) =>
                                handleTerminalCard(event.target.value, index)
                              }
                            >
                              {list_all_card &&
                                list_all_card.map(
                                  (valueAllCard, indexAllCard) => (
                                    <option
                                      key={index}
                                      value={valueAllCard.id}
                                      selected={
                                        terminalCardData[index] &&
                                        terminalCardData[index].id ===
                                          valueAllCard.id
                                      }
                                      onChange={(event) =>
                                        handleTerminalCard(
                                          event.target.value,
                                          index
                                        )
                                      }
                                    >
                                      {valueAllCard.jenisKartu}
                                    </option>
                                  )
                                )}
                            </select>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col
                        md={1}
                        style={{ display: "grid", alignItems: "flex-end" }}
                        className="m-0"
                      >
                        <Button
                          color="light"
                          className="btn btn-light waves-effect mb-3"
                          onClick={() => {
                            handleRemoveJenisKartu(index);
                          }}
                        >
                          <i className="bx bxs-x-circle font-size-16 align-middle"></i>
                        </Button>
                      </Col>
                    </Row>
                  ))}
                {showJenisKartu && showJenisKartu.length < 8 && (
                  <Row>
                    <Col
                      md={4}
                      style={{ display: "grid", justifyItems: "flex-end" }}
                    >
                      <button
                        type="button"
                        color="link"
                        className="btn btn-link waves-effect"
                        onClick={() => {
                          handleAddJenisKartu();
                        }}
                      >
                        + Tambah Jenis kartu
                      </button>
                    </Col>
                  </Row>
                )}
              </AvForm>
            </CardBody>
          </Card>
          {Prompt}
          <ShowSweetAlert />
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const { list_all_card, message_all_card, response_code_all_card, loading } =
    state.Card;
  const { response_code_terminal_card, message_terminal_card } =
    state.TerminalCard;
  return {
    list_all_card,
    message_all_card,
    message_terminal_card,
    response_code_terminal_card,
    response_code_all_card,
    loading,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readAllCard,
      createTerminalCard,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(EditTerminalCard);
